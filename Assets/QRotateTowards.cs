﻿using UnityEngine;
using System.Collections;
using VacuumBreather;
public class QRotateTowards : MonoBehaviour
{
    public Transform player;
    private Vector3 target;
    private readonly PidQuaternionController _pidController = new PidQuaternionController(8.0f, 0.0f, 0.05f);
    private Rigidbody rb;
    public float Kp, Ki, Kd;
    public float maxDist = 1;
    public float maxAcc = 8;
    public float stunned = 0;


    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
            target = transform.position;
    }
    void Update()
    {
        if (!(stunned >= 0))
        {
            //lookTowards(player.position);
            float dist = Vector3.Distance(target, transform.position);
            if (dist > maxDist)
            {
                rb.AddForce((target - transform.position).normalized * Mathf.Min(maxAcc, 5 * dist / maxDist),ForceMode.Acceleration);
                
            }
            else {
                rb.velocity = Vector3.zero;
            }
        }
        else
        {
            stunned -= Time.deltaTime;
        }
    }
    private void lookTowards(Vector3 target)
    {
        Quaternion DesiredOrientation = Quaternion.identity;
            DesiredOrientation = Quaternion.LookRotation(target - transform.position);


        this._pidController.Kp = this.Kp;
        this._pidController.Ki = this.Ki;
        this._pidController.Kd = this.Kd;

        // The PID controller takes the current orientation of an object, its desired orientation and the current angular velocity
        // and returns the required angular acceleration to rotate towards the desired orientation.
        Vector3 requiredAngularAcceleration = this._pidController.ComputeRequiredAngularAcceleration(
            transform.rotation,
            DesiredOrientation,
            rb.angularVelocity,
            Time.fixedDeltaTime);
        rb.AddTorque(requiredAngularAcceleration, ForceMode.Acceleration);
    }
}