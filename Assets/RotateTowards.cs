﻿using UnityEngine;
using System.Collections;
public class RotateTowards : MonoBehaviour
{
    // The target marker.
    public Transform target;
    // Angular speed in radians per sec.
    public float speed;
    public float limit;
    void Start()
    {
        if(!target)
        target = GameObject.Find("Player").transform;
    }

    void Update()
    {
        Vector3 targetDir = target.position - transform.position;
        // The step size is equal to speed times frame time.
        float step = speed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        //Debug.DrawRay(transform.position, newDir, Color.red,1);
        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);
    }
}