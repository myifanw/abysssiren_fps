﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveTowards : MonoBehaviour {
    public GameObject target;
    public float force;
    private Rigidbody rb;
    public float rot_force = 0.1f;
    public bool attracting = true;
    // Use this for initialization
    void Start () {
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (attracting)
        {
            if (target)
            {
                Vector3 dist = target.transform.position - transform.position;
                float factor = 1;
                if (dist.magnitude > 4f)
                {
                    factor = .2f;
                    if (dist.magnitude > 6)
                    {
                        factor = .6f;
                    }
                    if (dist.magnitude > 10)
                    {
                        factor = 1;
                    }
                    rb.AddForce(Vector3.Normalize(dist) * force * factor, ForceMode.VelocityChange);

                    //get the angle between transform.forward and target delta
                    Vector3 targetDelta = target.transform.position - transform.position;
                    float angleDiff = Vector3.Angle(transform.forward, targetDelta);


                    // get its cross product, which is the axis of rotation to
                    // get from one vector to the other
                    Vector3 cross = Vector3.Cross(transform.forward, targetDelta);

                    // apply torque along that axis according to the magnitude of the angle.
                    rb.AddTorque(cross * angleDiff * force);
                }
            }
        }
    }
}
