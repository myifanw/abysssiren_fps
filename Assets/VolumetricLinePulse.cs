﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumetricLinePulse : MonoBehaviour {
    public float pulse_rise_length, pulse_fall_length;
    private float curr_pulse = 0;
    public float line_w_init, line_w_final;
    private float curr_w;
    public float saber_f_init, saber_f_final;
    private float curr_saber;
    private VolumetricLines.VolumetricLineBehavior v_line;
	// Use this for initialization
	void Start () {
        v_line = this.GetComponent<VolumetricLines.VolumetricLineBehavior>();
	}
	
	// Update is called once per frame
	void Update () {
        if(curr_pulse <= pulse_rise_length)
        {
            v_line.LineWidth = Mathf.Lerp(line_w_init, line_w_final, curr_pulse/pulse_rise_length);
            v_line.LightSaberFactor = Mathf.Lerp(saber_f_init, saber_f_final, curr_pulse);
        }
        else if (curr_pulse <= pulse_fall_length + pulse_rise_length)
        {
            v_line.LineWidth = Mathf.Lerp(line_w_final, line_w_init, (curr_pulse - pulse_rise_length)/(pulse_fall_length + pulse_rise_length));
            v_line.LightSaberFactor = Mathf.Lerp(saber_f_final, saber_f_init, (curr_pulse - pulse_rise_length) / (pulse_fall_length + pulse_rise_length));
        }
        else
        {
            curr_pulse = 0;
            this.gameObject.SetActive(false);
        }
        curr_pulse += Time.deltaTime;
	}
}
