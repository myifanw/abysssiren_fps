﻿Shader "Mobile/ToonShaderTransparentMobileeEditable" {
	Properties{
		_Color("Main Color", Color) = (0.5,0.5,0.5,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		 _Ramp ("Ramp", 2D) = "white" {}
		 _Ramp2("Ramp Damaged", 2D) = "white" {}
		_Alpha("Alpha", range(0.0, 1.0)) = 1.0
	}

		SubShader{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		//LOD 250

		CGPROGRAM
		#pragma surface surf Ramp alpha:fade



		uniform float _Alpha;
		sampler2D_half _Ramp;

	half4 LightingRamp(SurfaceOutput s, half3 lightDir, half atten) {
		half NdotL = dot(s.Normal, lightDir);
		half diff = NdotL * 0.5 + 0.5;
		half3 ramp = tex2D(_Ramp, float2(diff,diff)).rgb;
		half4 c;
		c.rgb = s.Albedo * _LightColor0.rgb * ramp * atten;
		c.a = _Alpha;
		return c;
	}

	sampler2D_half _MainTex;
	struct Input {
		half2 uv_MainTex;
	};

	half4 _Color;
	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
	}
	ENDCG
	}

		FallBack "Mobile/Diffuse"
}
