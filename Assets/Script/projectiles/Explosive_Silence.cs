﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosive_Silence : MonoBehaviour {
    public int max_amount = 3; //amount of storeable blanks
    public float Recharge_Rate = 10; //how long it takes to charge up one "blank"
    public float activation_delay = .35f; //how long your silence must be
    private float activation_countdown; 
    public bool active = false;
    private ProjectileShooter shooter;
    public GameObject explosion;
	// Use this for initialization
	void Start () {
        shooter = this.GetComponent<ProjectileShooter>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if(shooter.firing)
        {
            //shooter fired, reset everything
            activation_countdown = activation_delay;
            active = false;
        }
        else
        {
            activation_countdown -= Time.fixedDeltaTime;
            if(activation_countdown <= 0)
            {
                active = true;
            }
        }
	}
    public void explode ()
    {
        //turn on the explosion
        explosion.SetActive(true);
    }
}
