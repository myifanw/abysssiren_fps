﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLoadOutList : MonoBehaviour {
	public static ProjectileLoadOutList projectileLoadOutList;
	public List<LoadOut> LoadOutList;

	void Awake(){
		//set singleton
		if (projectileLoadOutList == null) {
			projectileLoadOutList = this;
			DontDestroyOnLoad (this.gameObject);
			for (int i = 0; i < LoadOutList.Count; i++) {
				LoadOutList [i].removeAnyDanglingSlot ();
				LoadOutList [i].refreshEffectiveWeaponAmt ();
			}
		} else {
			Destroy (this.gameObject);
		}
	}

	public void setLoadoutWeapon(int loadoutIdx, int weaponSlotIdx, Enum_ProjectileType pType){
		ProjectileLoadOutList.projectileLoadOutList.LoadOutList [loadoutIdx].setWeaponToSlot (weaponSlotIdx, pType);
		SaveGame.Save ();
	}
	public static saveGameData SaveData(saveGameData sgd){
		sgd.LoadOutList = new List<LoadOut>(projectileLoadOutList.LoadOutList);
		return sgd;
	}
	public static void LoadData(saveGameData sgd){
		projectileLoadOutList.LoadOutList = new List<LoadOut>(sgd.LoadOutList);
	}

}
