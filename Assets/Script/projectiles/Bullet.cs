﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Bullet : MonoBehaviour
{
    public int power;
    public int bounces = 1;
	public float stunDuration;
    public float stun_scale_max = 0.5f;
    public float validStunTravelDistance = 9999f;
    private int currBounces;
    public float scrollSpeedy = 0.5F; //for the animation of the texture
    public float scrollSpeedx = 5;
    public Renderer rend;

	public bool willChaseTarget;
	public Transform chaseTarget;
	public wayPointMoverPhy chaserMover;

    public GameObject collision_spark;
    private int spark_pool_amount = 10;
    public float spark_size = .1f;
    public float big_spark_size = .3f;
    public bool big_spark = false; //indicator for really large impact sparks
    public static Queue<GameObject> bullet1_spark_queue;

    //materials
    public Material mat;
    public Material power_mat;

    //death hitstop info
    public float hitstop = 1;
    private float hitstop_time;
    public GameObject hitstopping_segment;
    private Vector3 hitstop_last_velocity;
    private bool hitstopped = false;
    private GameObject hitstop_spark; //the spark that needs to freeze on impact as well to sell the hit
    public float timeToLive = 0f;
    public AudioClip hitSound;
    public AudioClip PowerhitSound;
    private bool powershot;
    private List<Collider> AffectedObjects;

    void Start()
    {
        AffectedObjects = new List<Collider>();
        currBounces = bounces;
        rend = GetComponent<Renderer>();
        if(bullet1_spark_queue == null)
        {
            //Debug.Log("create sparks");
            bullet1_spark_queue = new Queue<GameObject>();
            for (int i = 0; i < spark_pool_amount; i++)
            {
                GameObject spark = Instantiate(collision_spark, new Vector3(999, -999, 9999), new Quaternion(0, 0, 0, 0)) as GameObject;
                DontDestroyOnLoad(spark);
                bullet1_spark_queue.Enqueue(spark);
                //Debug.Log("creating each spark");
            }
        }
        if(!mat)
        {
            mat = rend.material;
        }
    }

    void Update()
    {
        if (hitstopped && hitstopping_segment)
        {
            //big spark for power feelz
            if (hitstop_spark && hitstop_spark.GetComponent<ParticleSystem>().time > .04f)
            {
                hitstop_spark.GetComponent<ParticleSystem>().Pause();
            }
            if (!hitstopping_segment.GetComponent<FishSegment>().hitstopped)
            {
                //Debug.Log(this.name + " unhitstop by " + hitstopping_segment.name);
                /*
                gameObject.GetComponent<Collider>().enabled = true;
                //check the limb you're stuck on if they're done being stunned
                //countdown must be over, resume normal actions
                this.GetComponent<Rigidbody>().isKinematic = false;
                this.GetComponent<Rigidbody>().velocity = hitstop_last_velocity;
                */
                hitstopped = false;
                hitstop_spark.GetComponent<ParticleSystem>().Play();
            }
        }
        else //do literally everything else. Hitstop is basically super stun
        {
            //double check to bust out of hitstop
            if (hitstopped)
            {
                hitstopped = false;
                hitstop_spark.GetComponent<ParticleSystem>().Play();
            }
            float offy = Time.time * scrollSpeedy;
            float offx = Time.time * scrollSpeedx;
            rend.material.mainTextureOffset = new Vector2(offx, offy);

            //chase target
            if (willChaseTarget)
            {
                if (Vector3.Distance(chaseTarget.position, new Vector3(0, 0, 0)) <= Vector3.Distance(transform.position, new Vector3(0, 0, 0)))
                {
                    willChaseTarget = false;
                    chaserMover.enabled = false;
                }
                chaserMover.resetWayPoint(chaseTarget.position);
            }
            timeToLive = Mathf.Max(0f, timeToLive - Time.deltaTime);
            if (timeToLive == 0)
            {
                HitboxOff();
            }
        }
    }
    void OnCollisionEnter(Collision col)
    {
        //Debug.Log(this.gameObject.name + " collides with " + col.gameObject.name);
        if (col.gameObject.tag == "Projectile")
        {
            Physics.IgnoreCollision(col.collider, this.GetComponent<Collider>());
		} else if (col.gameObject.tag == "Player")
		{
			Physics.IgnoreCollision(col.collider, this.GetComponent<Collider>());
		}
        else
        {
			if (col.gameObject.tag.Equals("Enemy"))
            {
                Physics.IgnoreCollision(col.collider, this.GetComponent<Collider>());
                
                AffectedObjects.Add(col.collider);
                if (hitSound)
                {
                    if (powershot && PowerhitSound) {
                        //Debug.Log(col.gameObject.name);
                        AudioManager.manager.playSFXOneShot(PowerhitSound, 0, PowerhitSound.length, 1 + UnityEngine.Random.Range(-0.05f, 0.2f), .8f);
                    }
                    AudioManager.manager.playSFXOneShot(hitSound, 0, hitSound.length, 1 + UnityEngine.Random.Range(-0.05f, 0.2f), .8f);
                }
                //Debug.Log("bullet hit " + col.gameObject.name);
                FishSegment fish_seg = col.gameObject.GetComponent<FishSegment>();
                fish_seg.processBulletContact (this, validStunTravelDistance > Vector3.Magnitude(gameObject.transform.position - PlayerCharacter.player.transform.position));
                //give a bounce back for hitting a dead body part
                if(fish_seg.health <= 0)
                {
                    currBounces++;
                }
                //also disable the homing, so the bullet doesn't get too many bonuses
                if (willChaseTarget)
                    chaserMover.enabled = false;
			}

            currBounces -= 1;
            //cut the power in half every hit
            power = power / 2;
            if (currBounces <= 0)
            {
                HitboxOff();
            }
            //Debug.Log(currBounces);

            var orthogonalVector = col.contacts[0].point - transform.position;
            var collisionAngle = Vector3.Angle(orthogonalVector, gameObject.GetComponent<Rigidbody>().velocity);

            if(collision_spark)
            {
                GameObject curr_spark = bullet1_spark_queue.Dequeue();
                curr_spark.transform.position = col.contacts[0].point;
                curr_spark.transform.rotation = new Quaternion(0, 0, 0, 0);
                curr_spark.transform.Rotate(orthogonalVector);
                curr_spark.transform.localScale = new Vector3(spark_size, spark_size, spark_size);
                var main = curr_spark.GetComponent<ParticleSystem>().main;
                if (big_spark)
                {
                    main.startSizeMultiplier = 6;
                    curr_spark.transform.localScale = new Vector3(.12f, .12f, .12f);

                    //Debug.Log(curr_spark.transform.localScale.ToString());
                    main.maxParticles = 40;
                }
                else
                {
                    main.startSizeMultiplier = 1;
                    main.maxParticles = 20;
                    curr_spark.transform.localScale = new Vector3(.1f, .1f, .1f);
                }
                curr_spark.GetComponent<ParticleSystem>().Play();
                hitstop_spark = curr_spark;
                bullet1_spark_queue.Enqueue(curr_spark);
            }
        }
    }

    void onTriggerEnter(Collider col)
    {
        
    }
    public void Powershot()
    {
        powershot = true;
        if(power_mat)
        {
            rend.material = power_mat;
        }
    }
    public void HitboxOff()
    {
        if (hitstopped)
        {
            hitstopped = false;
            hitstop_spark.GetComponent<ParticleSystem>().Play();
        }
        gameObject.GetComponent<Collider>().enabled = false;
        this.gameObject.SetActive(false);
    } 

	public void Reset(ProjectileInfo pInfo)
    {
        this.transform.localScale = pInfo.projectile.transform.localScale;
        this.gameObject.SetActive(true);
        gameObject.GetComponent<Collider>().enabled = true;
        currBounces = bounces;

		this.power = pInfo.power;
		this.stunDuration = pInfo.stunDuration;
		this.validStunTravelDistance = pInfo.validStunTravelDistance;

		willChaseTarget = (pInfo.lockOnChase && playerLockOnTriggerZone.lockedOnSegment != null);
		if (chaserMover != null) {
			chaserMover.enabled = willChaseTarget;
		}
		if (willChaseTarget) {
			chaseTarget = playerLockOnTriggerZone.lockedOnSegment.transform;
			chaserMover.resetWayPoint (chaseTarget.position);
		}
		timeToLive = pInfo.timeToLive;
        rend.material = mat;
        //maybe useless but reset hitstop stuff
        hitstopped = false;
        big_spark = false;
        for (int I = 0; I < AffectedObjects.Count; I++)
        {
            if(AffectedObjects[I])
                Physics.IgnoreCollision(AffectedObjects[I], this.GetComponent<Collider>(), false);
        }
        AffectedObjects.Clear();
    }

	public void setPower(int pow)
	{
		this.power = pow;
	}

    public void Hitstop(float stop_time, GameObject segment)
    {
        /*
        hitstop_last_velocity = this.gameObject.GetComponent<Rigidbody>().velocity;
        this.GetComponent<Rigidbody>().isKinematic = true;

        gameObject.GetComponent<Collider>().enabled = false;
        */
        currBounces = 0;
        hitstop_time = stop_time;
        hitstopped = true;
        hitstopping_segment = segment;
    }

    public void setStunDuration(float duration)
	{
		this.stunDuration = stunDuration;
	}
}