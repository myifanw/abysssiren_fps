﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBaseInfoDictionary : MonoBehaviour {
	public ProjectileInfo[] projectileInfosArray;

	public ProjectileInfo getProjectileInfo(Enum_ProjectileType pType){
		for (int i = 0; i < projectileInfosArray.Length; i++) {
			if (projectileInfosArray [i].projectileType.Equals (pType))
				return projectileInfosArray [i];
		}
		return null;
	}
}
