﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ProjectileInfo {
	public Enum_ProjectileType projectileType;
	public string name;
	public Sprite icon;
	public Rigidbody projectile;
	public int pooledAmount = 10;
	public float timeToLive = 5f;
	public int fireRate_CD = 1;
	public int power = 25;
    public float normalCost = .1f;
    public float superCost = 1f;
	public float bulletSpeed = 100;
	public Vector3 origin_PositionOffset = new Vector3(0f,-0.3f,0.12f);
	public int bulletPerShot = 1;
	public float minSpreadDegree = 0f;
	public float maxSpreadDegree = 10f;
	public float spreadIncreasePerShot = 3f;
	public float bulletMass = 1f;
	public float stunDuration = 0.5f;
	public float validStunTravelDistance = 9999f;
	public bool lockOnChase = false;
	public bool alternateShootFromSide = false;
	public Vector2 ShootFromSideAngle = Vector2.zero;

    //shockwave info
    public int shockwaves = 1;
    public float shockwave_speed = 50;
    public float shockwave_power_multiplier = 1;
    public float shockwave_base_duration = 4;
    public float shockwave_max_size = 4;

}
