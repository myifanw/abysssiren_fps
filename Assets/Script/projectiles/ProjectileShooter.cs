﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour {

	public static ProjectileShooter projectileShoorter;
    public bool firing = false;
	public ProjectileBaseInfoDictionary baseInfoDict;
	protected Dictionary<Enum_ProjectileType ,Queue<Rigidbody>> bulletsPool_Dict;
	public Dictionary<Enum_ProjectileType, ProjectileInfo> projectileInfosDict;

	public Enum_ProjectileType currentProjectileType;
	public float CD_CountDown;
	protected float spreadRadius;
	//public static float shootVolumeThreshold = 0.1f;
//    [Range(0, 1f)] //Check settingVariable script for threshold value
//    public float low_threshold = .1f;
//    [Range(0, 1f)]
//    public float mid_threshold = .2f;
//    [Range(0, 1f)]
//    public float high_threshold = .5f;
    //Input Buffering vars
    public int bufferWindow = 1; //number of frames before the cooldown finishes in which a valid fire input (average volume) can be buffered. 
	public float buffered_Volume = 0f;
	private bool volBufferRecorded = false;

    //Volume averaging vars
    [Range(0, 1f)]
    public float VolAverageing_WindowSize = .06f; //number of frames in the past to consider in calculating the average volume
    private float VolAveraging_WindowFrames;
    private float curr_Average_Window_buffer; //this is a counter for the variable above.whenever you shoot, it should only fire X frames later, after ascertaining the highest volume.
	private Queue<float> volAveraging_HistoryQueue;

	//loadout information
	public int curLoadOutIndex = 0;

	public delegate void CurLoadOutChange();
	public static event CurLoadOutChange OnCurLoadOutChange;

	public int alternateFireSignSwitch = 1;

	//time stuff
	private float curr_time_scale = 0;
	private float curr_time_slow_time = 0;
	// Use this for initialization
	void Start () {
		//shootVolumeThreshold = settingVariables.shooter_lowThreshold;
		if (projectileShoorter != this) {
			projectileShoorter = this;
		}

        curr_Average_Window_buffer = VolAverageing_WindowSize;
        VolAveraging_WindowFrames = VolAverageing_WindowSize / Time.fixedDeltaTime;
        bulletsPool_Dict = new Dictionary<Enum_ProjectileType ,Queue<Rigidbody>>();
		projectileInfosDict = new Dictionary<Enum_ProjectileType, ProjectileInfo> ();

		//instantiate volume averaging vars
		volAveraging_HistoryQueue = new Queue<float>();

		//instantiating buffering logic vars
		buffered_Volume = 0f;
		volBufferRecorded = false;

		//instantiate bullet pool
		GameObject bulletHolder = new GameObject("bulletPool");
		foreach (ProjectileInfo pi in baseInfoDict.projectileInfosArray) {
			Queue<Rigidbody> temp_q = new Queue<Rigidbody>();

			for (int i = 0; i < pi.pooledAmount; i++)
			{
				Rigidbody instantiatedProjectile = Instantiate(pi.projectile, new Vector3(999,999,9999), new Quaternion(0,0,0,0)) as Rigidbody;
                instantiatedProjectile.GetComponent<Collider>().enabled = false;
                instantiatedProjectile.transform.localScale = new Vector3(0,0,0);
				instantiatedProjectile.transform.SetParent (bulletHolder.transform);
				temp_q.Enqueue(instantiatedProjectile);
			}
			bulletsPool_Dict.Add (pi.projectileType, temp_q);
			projectileInfosDict.Add (pi.projectileType, pi);
		}
		//start scene with the first loadout
		setCurLoadOutIdx(0);

		setCurProjectile(settingVariables.getShooter_minShootThreshold()+0.01f);
	}

	//get the index for section of volume bar for the current load out
	public int curVolumeToBarIndex(float volume){
        //int WeaponPerLoadOut = ProjectileLoadOutList.projectileLoadOutList.LoadOutList [curLoadOutIndex].projectileTypeList.Count;
        int WeaponPerLoadOut = ProjectileLoadOutList.projectileLoadOutList.LoadOutList [curLoadOutIndex].effectiveWeaponAmount;
        //int WeaponPerLoadout = curLoadOutIndex;
        /*
        float increment = 1f/WeaponPerLoadOut;
		return  Math.Min((int)((volume-shootVolumeThreshold) / increment), WeaponPerLoadOut-1);
        */

		/*
		if (volume < settingVariables.shooter_midThreshold)
        {
            return 0;
        }
		else if (volume < settingVariables.shooter_highThreshold && WeaponPerLoadOut > 1)
        {
            return 1;
        }
		else if (volume >= settingVariables.shooter_highThreshold && WeaponPerLoadOut > 2)
        {
            return 2;
        }
        else
        {
            return 0;
        }
		*/

		//return normal bullet if within normal range (minshoot <-> powershoot), return power shot if over powershotthreshold
		if (volume < settingVariables.getShooter_powerShotThreshold())
		{
			return 0;
		} else
		{
			//return 1;
			return 0;
		}
    }

	//set current projectile type base on voluem and load out
	public void setCurProjectile(float volume){
		//get current loadout
		LoadOut lo = ProjectileLoadOutList.projectileLoadOutList.LoadOutList [curLoadOutIndex];
		//record previous projectile type to check if spread needs to be reset
		Enum_ProjectileType oldProjectileType = currentProjectileType;
		//change current projectile type

		currentProjectileType = lo.projectileTypeList[curVolumeToBarIndex(volume)];
		//reset spread if changing weapon
		if (!currentProjectileType.Equals (oldProjectileType)) {
			spreadRadius = getCurProjectileInfo ().minSpreadDegree;
		}
	}

	public ProjectileInfo getCurProjectileInfo(){
        
		return (projectileInfosDict [currentProjectileType]);
	}

	public Rigidbody dequeueCurrentBulletFromPool(){
		return bulletsPool_Dict[currentProjectileType].Dequeue();
	}
	public void enqueueCurrentBulletFromPool(Rigidbody rb){
		bulletsPool_Dict[currentProjectileType].Enqueue(rb);
	}

	private void recordVolAveragingHistoryQueue(float vol){
		volAveraging_HistoryQueue.Enqueue (vol);
		while (volAveraging_HistoryQueue.Count > VolAveraging_WindowFrames) {
			float temp = volAveraging_HistoryQueue.Dequeue ();
        }
	}

	//return the average volume value from the volume averaging logic
	//return -1 if not enough frames has been recorded
	private float getAveragedVolume(){
		if (volAveraging_HistoryQueue.Count < VolAveraging_WindowFrames) {
			return -1f;
		} else {
			//calculate the average volume
			//dequeue the history queue, calculate and enqueue them again
			float result = 0f;
			for (int i = 0; i < volAveraging_HistoryQueue.Count; i++) {
				float temp = volAveraging_HistoryQueue.Dequeue ();
                //result = Math.Max (temp, result); //currently taking the max, may need to change later
                result += temp; //trying averaging
                volAveraging_HistoryQueue.Enqueue (temp);
			}
            result /= volAveraging_HistoryQueue.Count;

            return result;
		}
	}

	// Update is called once per frame
	void LateUpdate () {
		//record current volume into queue for calculating average. The average is constantly calculated. Even if the player is not generating voice input
		recordVolAveragingHistoryQueue (MicTestFinishedBuild_Input.volumeVal);
        //if (MicTestFinishedBuild_Input.volumeVal > shootVolumeThreshold)
          //  Debug.Log(MicTestFinishedBuild_Input.volumeVal);x
        //Check whether calculated average volume is above threshold.
		if (getAveragedVolume () > settingVariables.getShooter_minShootThreshold() && TimescaleManager.manager.getCurTimeScale() != 0) {
			//check if firerate CD is Compelte. if CD_CountDown is at or below 0, if so bullets are available to be shot.
			if (CD_CountDown <= 0) {
                firing = true;
                if (curr_Average_Window_buffer <= 0) // Also check if the buffer window for mic input time is complete.
                {
                    //set the volume that is to be used for projectile generation
                    float finalVolumeForProjectile = getAveragedVolume();

                    curr_Average_Window_buffer = VolAverageing_WindowSize; //reset this buffer
                                                                           //use buffered volume if any registered in buffer
                                                                           //if (volBufferRecorded) finalVolumeForProjectile = buffered_Volume;
                                                                           //Find the projectile type base on volume and loadout
                    setCurProjectile(finalVolumeForProjectile);

                    //reset CD countdown 
                    CD_CountDown = getCurProjectileInfo().fireRate_CD;

                    //Debug.Log(finalVolumeForProjectile + " curr buffered volume **********************************");
                    //shoot the bullets
					bool powershot = false;
					if (PlayerCharacter.player.superMeter >= getCurProjectileInfo ().superCost && finalVolumeForProjectile > settingVariables.getShooter_powerShotThreshold()) {
						powershot = true;
                        shoot(powershot);
                    }
                    else if(PlayerCharacter.player.superMeter >= getCurProjectileInfo().normalCost)
                    {
                        //this is a check if you can even fire at all given normal cost
                        shoot(powershot);
                    }
                    else
                    {
                        //can't fire.
                        misfire(); 
                    }

                }
                else
                {
                    //count down the window buffer too, probably...
                    curr_Average_Window_buffer -= Time.fixedDeltaTime;
                    //Debug.Log(curr_Average_Window_buffer + "what's up with the time");
                }
            } else {
				//Player tried to fire but Fire rate CD not complete, can still record it via buffer if we are within the buffer window
				if (bufferWindow <= CD_CountDown) {
					//if we are within the last few frames of the CD then we can record the input
					buffered_Volume = getAveragedVolume ();
					volBufferRecorded = true;
                    //Michael: Unsure if any of this is needed, and it actively doesn't work for some reason. 
				}
			}
		} else {
			//not firing so reset spread
			spreadRadius = getCurProjectileInfo().minSpreadDegree;
            firing = false;
		}

		//time slow decay
		if (curr_time_scale != 1) {
			curr_time_slow_time -= Time.deltaTime;

			if (curr_time_slow_time <= 0) {
				TimescaleManager.manager.setCurTimeScale (1);
				curr_time_scale = 1;
			}
			else if (curr_time_slow_time <= 0.5f) {
				TimescaleManager.manager.setCurTimeScale (Mathf.Lerp(curr_time_scale, 1, curr_time_slow_time));
			}
		}

		CD_CountDown = CD_CountDown - TimescaleManager.manager.getRealTimeDeltaTime() * 60;
    }

	private void shoot(bool powershot){
		if (powershot) {
			PlayerCharacter.player.deltaSuperMeter (getCurProjectileInfo().superCost * -1f);
		}
        else
        {
            PlayerCharacter.player.deltaSuperMeter(getCurProjectileInfo().normalCost * -1f);
        }
		switch (getCurProjectileInfo ().projectileType) {
		case Enum_ProjectileType.SingleShot:
			if (powershot) {
				timeSlow (.3f, .08f);
				shootBullets (powershot, 2, 10000, 10, 2, 2, Color.black);//black is the non-option. Jank, I know...
			}
			else
				shootBullets (powershot);
			break;
		case Enum_ProjectileType.MachineGun:
            if (powershot)
            {
                timeSlow(.15f, 1.25f);
            }
            else
            {
                shootBullets(false, 1, 0, 0, 0, 1, new Color(.8F, 0.2F, .8F));
            }
			break;
		case Enum_ProjectileType.ShotGun:
			if (powershot) {
				shootBullets (powershot, 3, 50, 1, 0, 2, new Color(1F, 0.5F, 0.5F));
				timeSlow (.3f, .08f);
			}
			else
                shootBullets(false, 1, 0, 0, 0, 1, new Color(0.6F, 0.3F, 0.6F));
            break;
		}
	}

	public void setCurLoadOutIdx(int idx){
		curLoadOutIndex = idx;
		//notify listener that cur loadout index changed
		if (OnCurLoadOutChange != null) {
			OnCurLoadOutChange ();
		}
	}

	public void shootBullets()
	{
		shootBullets (false, 1, 0, 0, 0, 1, Color.white);
	}
	public void shootBullets(bool powershot)
	{
		shootBullets (powershot, 1, 0, 0, 0, 1, Color.white);
	}
	public void shootBullets(bool powershot, float bullet_scale, float additional_mass, int additional_bounces, int add_rings, float power_multiplier, Color c){
        //shoot bullets
        Vector2 spreadVector_degree = new Vector2();
        for (int i = 0; i < getCurProjectileInfo ().bulletPerShot; i++) {
			//generate origin and angle value of shooting
			//origin offset by each bullet's offset position
			Vector3 origin = transform.position + transform.forward * getCurProjectileInfo ().origin_PositionOffset.z + transform.right * getCurProjectileInfo ().origin_PositionOffset.x + transform.up * getCurProjectileInfo ().origin_PositionOffset.y;
			//rotation offset by spread of each weapon
			Quaternion rotation = transform.rotation;
			Vector3 rot_degree = rotation.eulerAngles;
			spreadVector_degree = UnityEngine.Random.insideUnitCircle * spreadRadius;

			//increase spread for next consecutive fire, if no immediate bullet after, the else statement will reset spread
			spreadRadius = Math.Min(spreadRadius + getCurProjectileInfo().spreadIncreasePerShot, getCurProjectileInfo().maxSpreadDegree);

			//dequeue bullet from queue
			Rigidbody instantiatedProjectile = dequeueCurrentBulletFromPool ();
            if (powershot)
            {
                //instantiatedProjectile.GetComponent<Collider>().isTrigger = true;
            }
            else
            {
                //instantiatedProjectile.GetComponent<Collider>().isTrigger = false;
            }
            //reset velocity
            instantiatedProjectile.velocity = Vector3.zero;
			instantiatedProjectile.angularVelocity = Vector3.zero;

			//set origin and rotation
			instantiatedProjectile.transform.position = origin;
			instantiatedProjectile.transform.rotation = rotation;
			//set spread
			instantiatedProjectile.transform.Rotate (new Vector3(spreadVector_degree.x, spreadVector_degree.y, 0f));
            //mass
			instantiatedProjectile.mass = getCurProjectileInfo ().bulletMass * bullet_scale + additional_mass;
            //bounces
            instantiatedProjectile.GetComponent<Bullet>().bounces += additional_bounces;
            

			//manually rotate if is alternate fire from side (such as homing shot that is shot at a 60 degree angle)
			if (getCurProjectileInfo ().alternateShootFromSide) {
				instantiatedProjectile.transform.Rotate (getCurProjectileInfo ().ShootFromSideAngle*alternateFireSignSwitch);
				alternateFireSignSwitch = alternateFireSignSwitch*-1;
			}


			//set bullet script specific values such as damage
			Bullet mybullet = instantiatedProjectile.GetComponent<Bullet> ();
			mybullet.Reset (getCurProjectileInfo ());

			if (powershot) {
                //change color 
                instantiatedProjectile.GetComponent<Bullet>().Powershot();
				instantiatedProjectile.transform.localScale = instantiatedProjectile.transform.localScale * bullet_scale;
                instantiatedProjectile.GetComponent<Bullet>().setPower(Mathf.RoundToInt(instantiatedProjectile.GetComponent<Bullet>().power * power_multiplier));

            }

			//shooting code
			instantiatedProjectile.AddForce (instantiatedProjectile.transform.forward * getCurProjectileInfo ().bulletSpeed);
			instantiatedProjectile.velocity = instantiatedProjectile.transform.forward * getCurProjectileInfo ().bulletSpeed;
			enqueueCurrentBulletFromPool (instantiatedProjectile); //recycle the bullet to the end of the queue
		}

        //Set size based on volume, for testing purpose only
        //instantiatedProjectile.transform.localScale = new Vector3 (.5f, .5f, .5f) * finalVolumeForProjectile;
        float finalVolumeForProjectile = getAveragedVolume();
        //fire the shockwaves
        if (gameObject.GetComponent<SoundRipple>())
            gameObject.GetComponent<SoundRipple>().fire(finalVolumeForProjectile, 5);
        if (gameObject.GetComponent<WhiteSoundRipple>())
        {
            gameObject.GetComponent<WhiteSoundRipple>().fire(getCurProjectileInfo().shockwave_power_multiplier * bullet_scale, getCurProjectileInfo().shockwave_speed * Mathf.Pow(bullet_scale, 1.5f), finalVolumeForProjectile,
                getCurProjectileInfo().shockwaves + add_rings, getCurProjectileInfo().shockwave_base_duration, getCurProjectileInfo().shockwave_max_size, spreadVector_degree);
            if(!c.Equals(Color.black))
                gameObject.GetComponent<WhiteSoundRipple>().Color(c);
        }
    }

    public void misfire()
    {
        if (gameObject.GetComponent<WhiteSoundRipple>())
        {
            gameObject.GetComponent<WhiteSoundRipple>().fire(.3f, 25f, .5f, 2, 3f, .5f, new Vector2() );
            gameObject.GetComponent<WhiteSoundRipple>().Color(Color.red);
        }
    }
    public void timeSlow(float scale, float duration) {
		TimescaleManager.manager.setCurTimeScale (scale);
		curr_time_scale = scale;
		curr_time_slow_time = duration;
	}
}
