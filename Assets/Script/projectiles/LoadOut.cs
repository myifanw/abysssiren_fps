﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class LoadOut
{
	public int effectiveWeaponAmount = 0;
	public List<Enum_ProjectileType> projectileTypeList;

	public void setWeaponToSlot(int idx, Enum_ProjectileType pType){
		projectileTypeList [idx] = pType;
		removeAnyDanglingSlot ();
		refreshEffectiveWeaponAmt ();
	}

	public void refreshEffectiveWeaponAmt(){
		effectiveWeaponAmount = 0;
		for (int i = 0; i < projectileTypeList.Count; i++) {
			if (!projectileTypeList [i].Equals (Enum_ProjectileType.None)) {
				effectiveWeaponAmount++;
			} 
		}
	}

	//if a lower number slot has weapon type NONE then this slot become none by default
	public void removeAnyDanglingSlot(){
		bool foundNone = false;
		for (int i = 0; i < projectileTypeList.Count; i++) {
			if (foundNone) {
				projectileTypeList [i] = Enum_ProjectileType.None;
			} else {
				if(projectileTypeList [i].Equals(Enum_ProjectileType.None)){
					foundNone=true;
				}
			}
		}
	}
}