﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Headtilt_shield : MonoBehaviour {
	public float setAngle_absVal;
	public float resetAngle_absVal;
	public AudioClip switchSound;
	private bool shielded = false;
    public GameObject Shield;
	// Use this for initialization
	void Start () {
		shielded = false;
	}
	
	// Update is called once per frame
	void Update () {

		float angle = UnwrapAngle (gameObject.transform.rotation.eulerAngles.x);
		if (angle < resetAngle_absVal) {
			//head is more or less upright
			shielded = false;
		} 
		if (angle > setAngle_absVal) {
			if (!shielded) {
                Shield.SetActive(true);
				SoundUtil.playClip (gameObject, switchSound);
			}
		}
	}
	private static float UnwrapAngle(float angle)
	{
		float temp = angle;
		if(temp >=360)
			temp = temp%360;

		if(temp >=180)
			temp = temp - 360;

		return temp;
	}
}
