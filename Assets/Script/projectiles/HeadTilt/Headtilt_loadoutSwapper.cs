﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Headtilt_loadoutSwapper : MonoBehaviour {
	public float setAngle_absVal;
	public float resetAngle_absVal;
	public AudioClip switchSound;
	private bool setYet = false;
	// Use this for initialization
	void Start () {
		setYet = false;
	}
	
	// Update is called once per frame
	void Update () {

		float angle = UnwrapAngle (gameObject.transform.rotation.eulerAngles.z);
		if (Mathf.Abs (angle) < resetAngle_absVal) {
			//head is more or less upright
			setYet = false;
		} 
		if (Mathf.Abs (angle) > setAngle_absVal) {
			if (!setYet) {
				setYet = true;
				int newLoadOutIdx = ProjectileShooter.projectileShoorter.curLoadOutIndex;
				if (angle < 0) {
					//tilting right is up
					newLoadOutIdx++;
				} else {
					//tilting left is down
					newLoadOutIdx--;
				}
				if (newLoadOutIdx < 0) {
					newLoadOutIdx = ProjectileLoadOutList.projectileLoadOutList.LoadOutList.Count - 1;
				}
                

                if (newLoadOutIdx >= ProjectileLoadOutList.projectileLoadOutList.LoadOutList.Count){
					newLoadOutIdx = 0;
                }
                ProjectileShooter.projectileShoorter.setCurLoadOutIdx(newLoadOutIdx);
				SoundUtil.playClip (gameObject, switchSound);
			}
		}
	}
	private static float UnwrapAngle(float angle)
	{
		float temp = angle;
		if(temp >=360)
			temp = temp%360;

		if(temp >=180)
			temp = temp - 360;

		return temp;
	}
}
