﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundwaveRipple : Bullet 
{
    private float s;
    public float _startSize = 1;
    public float _maxSize = 10;
    private float _baseDuration = .25f;
    public float _duration = 2;
    public float zOffset = .5f;
    public float distortion = 64;

    private float _elapsedTime = 0f;
    private float _normalizedTime;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_normalizedTime < 1)
        {
            _elapsedTime = _elapsedTime + Time.deltaTime;
            _normalizedTime = _elapsedTime / _duration;
            s = Mathf.Lerp(_startSize, _maxSize, _normalizedTime);
            gameObject.transform.localScale = new Vector3(s, s, s);
            gameObject.GetComponent<Renderer>().material.SetFloat("_BumpAmt", ((1 - _normalizedTime) * distortion));
        }
        else
        {
            this.gameObject.SetActive(false);

            _elapsedTime = 0f;
            _normalizedTime = 0f;
        }
    }

    public new void Reset()
    {
        this.gameObject.SetActive(true);
        _elapsedTime = 0f;
        _normalizedTime = 0f;
    }
}
