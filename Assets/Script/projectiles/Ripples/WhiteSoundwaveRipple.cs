﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteSoundwaveRipple : Bullet 
{
    private float s;
    public float _startSize = 1;
    public float _maxSize = 10;
    public float _duration = 2;
    public float zOffset = .5f;
    public float distortion = 20;

    private float _elapsedTime = 0f;
    private float _normalizedTime;

    private SpriteRenderer sprite;
    private Color c;
    // Use this for initialization
    void Start () {
        sprite = gameObject.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (_normalizedTime < 1)
        {
            _elapsedTime = _elapsedTime + Time.deltaTime;
            _normalizedTime = _elapsedTime / _duration;
            s = Mathf.Lerp(_startSize, _maxSize + distortion, _normalizedTime);
            gameObject.transform.localScale = new Vector3(s, s, s);

            s = Mathf.Lerp(distortion, distortion/2, .1f + _normalizedTime * 10f );
            if(s <= distortion / 2)
            {
                s = Mathf.Lerp(distortion / 2, 0, .1f + _normalizedTime * 4f );
            }
            c = sprite.color;
            sprite.color = new Color(c.r, c.g, c.b, s);
            gameObject.GetComponent<SpriteRenderer>().color = sprite.color;
        }
        else
        {
            this.gameObject.SetActive(false);

            _elapsedTime = 0f;
            _normalizedTime = 0f;
        }
    }

    public new void Reset()
    {
        this.gameObject.SetActive(true);
        _elapsedTime = 0f;
        _normalizedTime = 0f;
    }
}
