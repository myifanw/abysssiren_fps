﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class SoundRipple : MonoBehaviour
{

    public Rigidbody projectile;
    protected Queue<Rigidbody> bullets;
    public int pooledAmount = 10;
    public int fireRate = 1;
    public int power = 25;
    protected int currFire;
    public float bulletSpeed = 100;

    public static float shootVolumeTreshold = 0.2f;
    //buffer so you can buffer shots
    public int bufferFrames = 1;
    protected int currBuff;

    protected bool firing = false;
    public float originy = 0.3f;
    public bool fired;
    public float currPower;
    private float currSpeed;
    public float distortion_multiplier = 30;
    // Use this for initialization
    void Start()
    {
        currFire = 0;
        currBuff = 0;
        bullets = new Queue<Rigidbody>();
        for (int i = 0; i < pooledAmount; i++)
        {
            Rigidbody instantiatedProjectile = Instantiate(projectile, new Vector3(999, 999, 9999), new Quaternion(0, 0, 0, 0)) as Rigidbody;
            bullets.Enqueue(instantiatedProjectile);
        }
    }
    void LateUpdate()
    {
        //SoundRipple is only triggered by soundblast.
        if (firing)
        {
            Rigidbody instantiatedWave = bullets.Dequeue();
            //reset shit
            instantiatedWave.velocity = Vector3.zero;
            instantiatedWave.angularVelocity = Vector3.zero;
            Vector3 origin = transform.position + transform.forward * 0.12f;
            Quaternion rot = transform.rotation;


            instantiatedWave.transform.position = origin + transform.forward;
            instantiatedWave.transform.rotation = rot;
            instantiatedWave.velocity = transform.forward * bulletSpeed/10;
            instantiatedWave.transform.Rotate(-90, 0, 0, Space.Self);

            instantiatedWave.GetComponent<SoundwaveRipple>().distortion = distortion_multiplier * power/100;
            
            //instantiatedWave.GetComponent<SoundwaveRipple>().distortion = 10 * Mathf.Pow(MicTestFinishedBuild_Input.volumeVal, 1.5f);
            instantiatedWave.GetComponent<SoundwaveRipple>().Reset();
            bullets.Enqueue(instantiatedWave);
            //_heatwave.gameObject.transform.localScale = new Vector3(s, s, s);
        }
        firing = false;
    }

    public void fire(float power, float speed)
    {
        //Debug.Log("display!" + currPower);
        firing = true;
        currPower = power;
        currSpeed = speed;
    }
    

    public void Reset()
    {
        //duration = _baseDuration;
    }
}