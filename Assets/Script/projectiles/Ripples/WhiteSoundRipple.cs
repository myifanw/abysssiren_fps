﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class WhiteSoundRipple : MonoBehaviour
{

    public Rigidbody projectile;
    protected Queue<Rigidbody> bullets;
    public int pooledAmount = 10;
    public int fireRate = 1;
    public int power = 25;
    protected int currFire;
    public float bulletSpeed = 100;
    public float bulletPower = 1;
    public float duration = 4;
    public int rippleCount = 3;
    public float max_size = 4;
    public static float shootVolumeTreshold = 0.2f;
    //buffer so you can buffer shots
    public int bufferFrames = 1;
    protected int currBuff;

    protected bool firing = false;
    public float originy = 0.3f;
    public bool fired;
    private Color color;
    private float currSpeed;
    private float currVolume;
    private Vector2 spread;
    // Use this for initialization
    void Start()
    {
        currFire = 0;
        currBuff = 0;
        bullets = new Queue<Rigidbody>();
        for (int i = 0; i < pooledAmount; i++)
        {
            Rigidbody instantiatedProjectile = Instantiate(projectile, new Vector3(999, 999, 9999), new Quaternion(0, 0, 0, 0)) as Rigidbody;
            bullets.Enqueue(instantiatedProjectile);
        }
    }
    void LateUpdate()
    {
        //SoundRipple is only triggered by soundblast.
        if (firing)
        {
            int currRip = rippleCount;
            while (currRip > 0)
            {
                Rigidbody instantiatedWave = bullets.Dequeue();
                //reset shit
                instantiatedWave.velocity = Vector3.zero;
                instantiatedWave.angularVelocity = Vector3.zero;
                Vector3 origin = transform.position + transform.forward * 0.12f + transform.up * originy;
                Quaternion rot = transform.rotation;

                instantiatedWave.GetComponent<SpriteRenderer>().color = color;
                instantiatedWave.transform.position = origin + transform.forward;
                instantiatedWave.transform.rotation = rot;
                instantiatedWave.transform.Rotate(new Vector3(spread.x, spread.y, 0f));
                instantiatedWave.velocity = transform.forward * (bulletSpeed * (rippleCount/(float)currRip));
                instantiatedWave.GetComponent<WhiteSoundwaveRipple>().distortion = (.2f + (bulletPower * (float)currVolume) / currRip) * 9/10;
                instantiatedWave.GetComponent<WhiteSoundwaveRipple>()._duration = (duration * (float)rippleCount / (currRip+2f))/2;
   
                instantiatedWave.GetComponent<WhiteSoundwaveRipple>()._maxSize = max_size/4 * Mathf.Pow(currRip, 2f) / rippleCount;
                instantiatedWave.GetComponent<WhiteSoundwaveRipple>().Reset();
                bullets.Enqueue(instantiatedWave);
                //_heatwave.gameObject.transform.localScale = new Vector3(s, s, s);
                currRip--;
            }
        }
        firing = false;
    }

    public void fire(float power, float speed, float volume, int ripples, float duration, float max_size, Vector2 spread)
    {
        //Debug.Log("display!" + currPower);
        firing = true;
        bulletPower = power;
        bulletSpeed = speed;
        currVolume = volume;
        rippleCount = ripples;
        this.duration = duration;
        this.max_size = max_size;
    }
    
    public void Color(Color c)
    {
        c.a = .5f;
        color = c;
    }
    public void Reset()
    {
        //duration = _baseDuration;
    }
}