﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBounce : MonoBehaviour
{
    public float wallbounce_force = 50;
    public float repulsion_force = 50;
    public Vector3 direction;
    public List<Rigidbody> AffectedObjects;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int I = 0; I < AffectedObjects.Count; I++)
        {
            if(AffectedObjects[I])
                AffectedObjects[I].AddForce(direction * repulsion_force);
            else
            {
                AffectedObjects.RemoveAt(I);
                I--;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        //Debug.Log(name + " bounces " + col.name);
        if (col.tag.Equals("Enemy"))
        {
            //col.attachedRigidbody.AddForce(direction * wallbounce_force);
            AffectedObjects.Add(col.attachedRigidbody);
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.tag.Equals("Enemy"))
        {
            AffectedObjects.Remove(col.attachedRigidbody);
        }
    }

}
