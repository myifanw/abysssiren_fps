﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
public class TransitionScreenManager : MonoBehaviour {
	public Image img;
	public Text txst;
	public Grayscale grayscale;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GameController.controller.GameOverRegistered) {
			if (GameController.controller.GameOverTimer < 13.2f) {
				grayscale.rampOffset = Mathf.Lerp (grayscale.rampOffset, -1f, TimescaleManager.manager.getRealTimeDeltaTime()*10f);
				//img.color = Color.Lerp (img.color, Color.black, TimescaleManager.manager.getRealTimeDeltaTime()*10f);
			}
			if (GameController.controller.GameOverTimer < 6f) {
				txst.color = Color.Lerp (txst.color, Color.clear, TimescaleManager.manager.getRealTimeDeltaTime () * 0.25f);
			} else if (GameController.controller.GameOverTimer < 12.6f) {
				foreach (GameObject el in GameController.controller.CurrEnemies) {
					el.SetActive (false);
				}
				img.color = Color.black;
				grayscale.rampOffset = 0f;
				txst.text = "You made it to wave: \n"+GameController.controller.waveNum;
				txst.color = Color.Lerp (txst.color, Color.white, TimescaleManager.manager.getRealTimeDeltaTime () * 1f);
			}

		} else {
			img.color = Color.Lerp (img.color, Color.clear, TimescaleManager.manager.getRealTimeDeltaTime()*2f);
		}
	}
}
