﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (TimescaleManager.manager.ispause ()) {
				TimescaleManager.manager.unpause ();
			} else {
				TimescaleManager.manager.pause ();
			}
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			TimescaleManager.manager.setCurTimeScale(TimescaleManager.manager.getCurTimeScale() - 0.1f);
		}
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			TimescaleManager.manager.setCurTimeScale(TimescaleManager.manager.getCurTimeScale() + 0.1f);
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			TimescaleManager.manager.setCurTimeScale(1f);
		}
	}
}
