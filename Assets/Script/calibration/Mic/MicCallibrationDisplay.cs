﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MicCallibrationDisplay : MonoBehaviour {
	public Text displayText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		displayText.text = "Sensitivity - " + settingVariables.getVolume_Sensitivity () 
		+"\n Min Threshold- "+settingVariables.getShooter_minShootThreshold()
		+"\n PowerShot Threshold- "+settingVariables.getShooter_powerShotThreshold();
	}
}
