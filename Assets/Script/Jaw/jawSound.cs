﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jawSound : MonoBehaviour {
	public AudioClip jawShakeSound;
	public AudioClip jawOpenSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void playRumbleSound(){
		AudioManager.manager.playSFXOneShot (jawShakeSound,0f, jawShakeSound.length, 1f, 1f-(jaw.jawStatic.distanceRatio * 0.85f));
	}
	public void playOpenSound(){
		AudioManager.manager.playSFXOneShot (jawOpenSound, 0f, jawShakeSound.length, 1f, 1f-(jaw.jawStatic.distanceRatio * 0.85f));
	}
}
