﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jaw : MonoBehaviour {
	private Animator animator;
	public static jaw jawStatic;
	public float distanceRatio = 1f;
	public float distanceFar = -60;
	public float distanceNear = -500;

    public bool isOpened;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		if (jawStatic != null && jawStatic != this) {
			Destroy(jawStatic);
		}
		jawStatic = this;

	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = new Vector3(transform.localPosition.x,transform.localPosition.y,Mathf.Lerp(transform.localPosition.z, Mathf.Lerp(distanceNear, distanceFar, distanceRatio), Time.deltaTime));
	}
	/*
	void OnGUI (){
		if (GUI.Button (new Rect (20,40,80,20), "Jaw Open")){
			jaw.jawStatic.openJaw ();
		}

		if (GUI.Button (new Rect (20,100,80,20), "Jaw Close")){
			jaw.jawStatic.closeJaw ();
		}
		if (GUI.Button (new Rect (20,180,80,20), "Jaw NEAR")){
			jaw.jawStatic.setDistanceRatio (0f);
		}

		if (GUI.Button (new Rect (20,280,80,20), "Jaw FAR")){
			jaw.jawStatic.setDistanceRatio(1f);
		}
	}
    */
	public void openJaw(){
		animator.SetBool ("open", true);
	}
	public void closeJaw(){
		animator.SetBool ("open", false);
	}

	public void setDistanceRatio(float newDistanceRatio){
		distanceRatio = newDistanceRatio;
	}

    public void setIsOpened()
    {
        isOpened = true;
    }
    public void setIsClosed()
    {
        isOpened = false;
    }
}
