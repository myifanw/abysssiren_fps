﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jaw_skyboxCamSpin : MonoBehaviour {
	Quaternion defaultRot;
	// Use this for initialization
	void Start () {
		defaultRot = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localRotation = Quaternion.Euler(PlayerCharacter.player.transform.rotation.eulerAngles);
	}
}
