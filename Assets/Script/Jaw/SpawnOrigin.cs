﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOrigin : MonoBehaviour {
	public static SpawnOrigin spawnOrigin;
	public float defaultOffset_x = 100f;
	public float defaultOffset_y = 100f;
	public float defaultOffset_z = 100f;
	// Use this for initialization
	void Start () {
		if (spawnOrigin != null && spawnOrigin != this) {
			Destroy(spawnOrigin.gameObject);
		}	
		spawnOrigin = this;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public Vector3 getRandomSpawnPosition(){
		Vector3 result = Random.insideUnitSphere;
		result = new Vector3 (result.x * defaultOffset_x, result.y * defaultOffset_y, result.z * defaultOffset_z);
		return result + transform.position;

	}

}
