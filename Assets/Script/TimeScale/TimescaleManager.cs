﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimescaleManager : MonoBehaviour {
	public static TimescaleManager manager;

	private float curTimeScale;
	private bool isPaused;
	private float realTimePrevFrame;
	private float realTimeDeltaTime;
	// Use this for initialization
	void Awake () {
		if (manager != null) {
			Destroy (manager.gameObject);
		}
		manager = this;

		setCurTimeScale (1f);
		realTimeDeltaTime = 0f;
		realTimePrevFrame = Time.realtimeSinceStartup;
	}
	
	// Update is called once per frame
	void Update () {
		//refresh real time delta time
		realTimeDeltaTime = Time.realtimeSinceStartup - realTimePrevFrame;
		realTimePrevFrame = Time.realtimeSinceStartup;
	}

	public void setCurTimeScale(float val){
		curTimeScale = Mathf.Min(100f, Mathf.Max(0f, val));
		if (!isPaused) {
			Time.timeScale = curTimeScale;
		}
	}
	public float getCurTimeScale(){
		return Time.timeScale;
	}

	public bool ispause(){
		return isPaused;
	}
	public void pause(){
		isPaused = true;
		Time.timeScale = 0f;
	}
	public void unpause(){
		isPaused = false;
		//return timesclae to what ever the value it was
		setCurTimeScale (curTimeScale);
	}
	public float getRealTimeDeltaTime(){
		return realTimeDeltaTime;
	}
}
