﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagedScreen : MonoBehaviour {
    public bool damaged = false;
    public float base_duration = .5f;
    public float base_intensity = .5f;
    private float hurt_progress;
    private float hurt_duration;
    private float hurt_intensity;
    private float _normalizedTime;
    private SpriteRenderer sprite;
    private Color c;
    private float s;
    // Use this for initialization
    void Start () {
        sprite = gameObject.GetComponent<SpriteRenderer>();
    }
	
    public void hurt()
    {
        hurt(base_duration, base_intensity);
    }

    public void hurt(float duration, float intensity)
    {
        hurt_duration = duration;
        hurt_intensity = intensity;
        this.gameObject.SetActive(true);
        damaged = true;
    }

	// Update is called once per frame
	void Update () {
		if(damaged)
        {
            c = sprite.color;
            _normalizedTime = hurt_progress / hurt_duration;
            if (hurt_progress <= hurt_duration/4)
            {
                //ramp up
                s = Mathf.Lerp(0, hurt_intensity, _normalizedTime * 4);
            }
            else
            {
                //ramp down
                s = Mathf.Lerp(hurt_intensity, 0, _normalizedTime);
            }

            sprite.color = new Color(c.r, c.g, c.b, s);
            gameObject.GetComponent<SpriteRenderer>().color = sprite.color;

            hurt_progress += Time.deltaTime;

            if (hurt_progress >= hurt_duration)
            {
                damaged = false;
                this.gameObject.SetActive(false);
                hurt_progress = 0;
            }
            
        }
	}
}
