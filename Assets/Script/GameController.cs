﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
    //countdown is between waves
    public static GameController controller;
    public GameObject player;

    public GameState gameState;
    //planned waves
    [System.Serializable]
    public class EnemyList
    {
        public int wave_num; //the wave that these enemy lists appear
        public GameObject enemySet;
    }
    public EnemyList[] EnemyLists;
    int currEnemySet = 0;

    public int countdown;
    public int waveCap;
    public float spawn_delay = .1f;
    private float spawn_delay_rand = 0;
    private float curr_delay;
    [SerializeField]
    public GameObject[] Lvl1Enemies;

    [SerializeField]
    public GameObject[] Lvl2Enemies;

    [SerializeField]
    public GameObject[] Lvl3Enemies;

    [SerializeField]
    public GameObject[] Lvl4Enemies;

    [SerializeField]
    public GameObject[] Lvl5Enemies;
    float currCount;
    public int waveNum = 1; //starting wave number
    public System.Collections.Generic.List<GameObject> CurrEnemies = new System.Collections.Generic.List<GameObject>();

    public AudioClip BGM;

	public AudioClip GameOverSFX;
	public float GameOverTimer = 5f;
	public bool GameOverRegistered = false;

//	void OnGUI()
//	{
//		for(int i=0;i<CurrEnemies.Count;i++)
//		{
//			GUI.Label(new Rect(10,100+(i*20),500,20),CurrEnemies[i].name);
//		}
//
//
//	}

    void Awake()
    {
        if (controller != null && controller != this)
        {
            Destroy(controller.gameObject);
        }
        controller = this;
		if (SaveGame.checkIfSaveExist ()) {
			SaveGame.Load ();
			setGameState(GameState.Preparation);
		} else {
			setGameState(GameState.Tutorial_Calibrate);
		}
    }
    // Use this for initialization
    void Start () {
        currCount = countdown;
        curr_delay = spawn_delay;
        AudioManager.manager.playNewBgm(BGM, 2f, 1.05f, 1f);
    }

    public void startEndlessMode()
    {
        setGameState(GameState.EndlessWave);
    }
    public void setGameState(GameState state)
    {
        gameState = state;
        if (state == GameState.Preparation)
        {
			if (jaw.jawStatic != null) {
				jaw.jawStatic.setDistanceRatio (1f);
				jaw.jawStatic.closeJaw ();
			}
        }
        else if (state == GameState.EndlessWave)
        {
            jaw.jawStatic.openJaw();
        }
    }
    void Spawn(float time)
    {
        int waveTotal = waveNum;
        int number = 0;
        //do spawning things
        if (Lvl5Enemies.Length > 0)
        {
            number = NumberofLevelEnemies(waveNum, 5, 70, 15, 10, 5);
            StartCoroutine(SpawnEnemies(Lvl5Enemies, number, time, 20));
            waveTotal -= number * 5;
        }
        if (Lvl4Enemies.Length > 0)
        {
            number = NumberofLevelEnemies(waveNum, 4, 40, 20, 20, 5);
            StartCoroutine(SpawnEnemies(Lvl4Enemies, number, time, 15));
            waveTotal -= number * 4;
        }
        if (Lvl3Enemies.Length > 0)
        {
            number = NumberofLevelEnemies(waveNum, 3, 30, 25, 20, 5);
            StartCoroutine(SpawnEnemies(Lvl3Enemies, number, time, 10));
            waveTotal -= number * 3;
        }
        if (Lvl2Enemies.Length > 0)
        {
            number = NumberofLevelEnemies(waveNum, 2, 10, 15, 15, 10);
            StartCoroutine(SpawnEnemies(Lvl2Enemies, number, time, 0));
            waveTotal -= number * 2;
        }
        if (Lvl1Enemies.Length > 0)
            StartCoroutine(SpawnEnemies(Lvl1Enemies, waveTotal, time, 0));


        //wave limit
        if (waveNum > waveCap)
        {
            waveNum = waveCap;
        }
        currCount = countdown;
    }
    public int NumberofLevelEnemies(int waveNum, int lvl, int zero_weight, int one_weight, int two_weight, int remaining_weight)
    {
        int max = Mathf.RoundToInt(waveNum / lvl);
        int roll = Random.Range(0, 100);
        if(roll < zero_weight)
        {
            return 0;
        }
        else if (roll < zero_weight + one_weight)
        {
            return Mathf.Min(1, max);
        }
        else if (roll < zero_weight + one_weight + two_weight)
        {
            return Mathf.Min(2, max);
        }
        else
        {
            int final = roll - zero_weight - one_weight - two_weight;
            final = Mathf.RoundToInt(final / remaining_weight) + 2;
            final = Mathf.Min(final, max);
        }
        return 0;        
    }
    IEnumerator SpawnEnemies(GameObject[] EnemyArray, int number, float time, float extra_distance)
    {
        int maxRadius = 60;
        int minRadius = 40;
        int narrowAngle = 45;
        int narrowAngleVertical = 40;
        //for (int i = 1; i <= waveNum - strongCount * 3; i++)
        for (int i = 1; i <= number; i++)
        {
            float range = (maxRadius - minRadius);
            Vector3 tempV3 = Vector3.zero;
//            while (true)
//            {//break when sure that the point is in the narrow angle
//                tempV3 = Random.insideUnitSphere * range;
//                tempV3 = Vector3.Normalize(tempV3) * maxRadius + tempV3;
//                //make sure not negative z
//                if (tempV3.z < 0)
//                {
//                    tempV3 = new Vector3(tempV3.x, tempV3.y, -1 * tempV3.z - extra_distance);
//                }
//                //make sure its in narrow angle
//                if (Vector3.Angle(new Vector3(tempV3.x, 0, tempV3.z), Vector3.forward) <= narrowAngle && Vector3.Angle(new Vector3(0, tempV3.y, tempV3.z), Vector3.forward) <= narrowAngleVertical)
//                {
//                    break;
//                }
//            }
			tempV3 = SpawnOrigin.spawnOrigin.getRandomSpawnPosition();
			tempV3 = new Vector3 (tempV3.x, tempV3.y, tempV3.z + extra_distance);
            //Vector3 spawn = Random.onUnitSphere * Random.Range(0, 20) + player.transform.position + new Vector3(0, 0, 1) * 60;
            GameObject instantiatedEnemy;
            int rand = Random.Range(0, EnemyArray.Length);
            instantiatedEnemy = Instantiate(EnemyArray[rand],
                                                        tempV3,
                                                        Random.rotation)
            as GameObject;
            if (instantiatedEnemy.GetComponent<FishSpawn>())
            {
                FishSpawn cont = instantiatedEnemy.GetComponentInChildren<FishSpawn>();
                cont.playerObject = player;
            }
            CurrEnemies.Add(instantiatedEnemy);
            yield return new WaitForSeconds(time);
        }
    }
    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetKey(KeyCode.J))
        {
			setGameState(GameState.Tutorial_Calibrate);
        }
        if (Input.GetKey(KeyCode.K))
        {
            setGameState(GameState.Preparation);
        }

        if (gameState == GameState.EndlessWave && jaw.jawStatic.isOpened)
        {
            if (CurrEnemies.Count <= 0)
            {
                bool scripted_level = false;
                int count = 0;
                foreach (EnemyList list in EnemyLists)
                {
                    if (waveNum == list.wave_num)
                    {
                        scripted_level = true;
                        currEnemySet = count;
                    }
                }

                if (scripted_level)
                {
                    if (currCount <= 0)
                    {
                        //spawn next wave
                        if (EnemyLists[currEnemySet] != null)
                        {
                            CurrEnemies = new List<GameObject>();
                            GameObject enemies = EnemyLists[currEnemySet].enemySet;
                            enemies.SetActive(true);
                            foreach (Transform enemy in enemies.transform)
                            {
                                CurrEnemies.Add(enemy.gameObject);
                            }
                            currEnemySet++;
                        }
                        currCount = countdown;
                        waveNum = waveNum + 1;
                    }
                    else
                    {
                        currCount -= Time.fixedDeltaTime;
                    }
                }
                else
                {
                    if (currCount <= 0)
                    {
                        Spawn(spawn_delay + Random.Range(0, spawn_delay));
                        waveNum = waveNum + 1;
                    }
                    else
                        currCount -= Time.fixedDeltaTime;
                }

            }
            else
            {
                int i = CurrEnemies.Count - 1;
                while (i >= 0)
                {
                    if (CurrEnemies[i] == null || CurrEnemies[i].Equals(null))
                        CurrEnemies.RemoveAt(i);
                    i--;
                }
            }
        }
			
    }

	void Update(){
		if (GameOverRegistered) {
			GameOverTimer = Mathf.Max (0f, GameOverTimer - TimescaleManager.manager.getRealTimeDeltaTime());
			if (GameOverTimer == 0f) {
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
			}
		}
	}

	public void processGameOver(){
		GameOverRegistered = true;
		AudioManager.manager.playSFXOneShot (GameOverSFX);
	}
}
