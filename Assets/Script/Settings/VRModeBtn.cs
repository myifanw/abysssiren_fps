﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class VRModeBtn : MonoBehaviour, IPointerDownHandler {
	public bool vrMode;
	public string sceneName;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OnPointerDown(PointerEventData eventData)
	{
		GvrViewer.isUsingVRMode = vrMode;
		SceneManager.LoadScene (sceneName);
	}
}
