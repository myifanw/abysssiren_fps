﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class settingImplementer : MonoBehaviour {
	public GameObject frameRateDisplayObj;
	// Use this for initialization
	
	// Update is called once per frame
	void Update () {
		if (settingVariables.getShowFPS()) {
			frameRateDisplayObj.SetActive (true);
		} else {
			frameRateDisplayObj.SetActive (false);
		}

	}
}
