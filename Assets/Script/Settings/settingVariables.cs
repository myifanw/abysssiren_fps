﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class settingVariables : MonoBehaviour {
	//whether to show FPS
	private static bool FPSShow;
	//input MIC sensitivity
	public static readonly float Volume_SensitivityMin = 0f;
	public static readonly float Volume_SensitivityMax = 50f;

	private static float Volume_Sensitivity = 8f;

	private static float shooter_minShootThreshold = 0.1f;
	private static float shooter_powerShotThreshold = 0.8f;

	private static float audio_BGM_Volume = 1f;
	private static float audio_SFX_Volume = 1f;

	private static bool isTutorialFinished = false;


	public delegate void SettingChange();
	public static event SettingChange OnSettingChange;

	public static saveGameData SaveData(saveGameData sgd){
		sgd.showFPS = settingVariables.getShowFPS ();
		sgd.tutorialFinished = settingVariables.checkIfTutorialFinished ();
		sgd.Volume_Sensitivity = settingVariables.getVolume_Sensitivity ();
		sgd.shooter_minShootThreshold = settingVariables.getShooter_minShootThreshold ();
		sgd.shooter_powerShotThreshold = settingVariables.getShooter_powerShotThreshold ();
		sgd.audio_BGM_Volume = settingVariables.getVolume_BGM ();
		sgd.audio_SFX_Volume = settingVariables.getVolume_SFX ();

		return sgd;
	}

	public static void LoadData(saveGameData sgd){
		FPSShow = sgd.showFPS;
		isTutorialFinished = sgd.tutorialFinished;
		Volume_Sensitivity = sgd.Volume_Sensitivity;
		shooter_minShootThreshold = sgd.shooter_minShootThreshold;
		shooter_powerShotThreshold = sgd.shooter_powerShotThreshold;
		audio_BGM_Volume = sgd.audio_BGM_Volume;
		audio_SFX_Volume = sgd.audio_SFX_Volume;
	}

	public static void setShowFPS(bool val){
		FPSShow = val;
		if (OnSettingChange != null) {
			OnSettingChange ();
		}
		SaveGame.Save ();
	}
	public static bool getShowFPS(){
		return FPSShow;
	}

	public static void setVolume_Sensitivity(float val){
		Volume_Sensitivity = val;
		if (OnSettingChange != null) {
			OnSettingChange ();
		}
		SaveGame.Save ();
	}
	public static float getVolume_Sensitivity(){
		return Volume_Sensitivity;
	}

	public static void setShooter_minShootThreshold(float val){
		shooter_minShootThreshold = val;
		if (OnSettingChange != null) {
			OnSettingChange ();
		}
		SaveGame.Save ();
	}
	public static float getShooter_minShootThreshold(){
		return shooter_minShootThreshold;
	}

	public static void setShooter_powerShotThreshold(float val){
		shooter_powerShotThreshold = val;
		if (OnSettingChange != null) {
			OnSettingChange ();
		}
		SaveGame.Save ();
	}
	public static float getShooter_powerShotThreshold(){
		return shooter_powerShotThreshold;
	}

	public static void setVolume_BGM(float val){
		audio_BGM_Volume = Mathf.Min(1f,Mathf.Max(0f, val));
		if (OnSettingChange != null) {
			OnSettingChange ();
		}
		SaveGame.Save ();
	}
	public static float getVolume_BGM(){
		return audio_BGM_Volume;
	}

	public static void setVolume_SFX(float val){
		audio_SFX_Volume = Mathf.Min(1f,Mathf.Max(0f, val));
		if (OnSettingChange != null) {
			OnSettingChange ();
		}
		SaveGame.Save ();
	}
	public static float getVolume_SFX(){
		return audio_SFX_Volume;
	}

	public static bool checkIfTutorialFinished(){
		return isTutorialFinished;
	}
	public static void setIfTutorialFinished(bool val){
		isTutorialFinished = val;
		SaveGame.Save ();
	}
}
