﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameWaveController : MonoBehaviour {
    //countdown is between waves
    public GameObject player;
    public int countdown;
    [System.Serializable]
    public class EnemyList
    {
        public GameObject[] enemies;
    }
    public EnemyList[] EnemyLists;
    int currEnemySet = 0;
    float currCount;
    private List<GameObject> CurrEnemies = new List<GameObject>();

    // Use this for initialization
    void Start () {
        currCount = countdown;
    }

    public void waveDeath()
    {
        int i = CurrEnemies.Count - 1;
        while (i >= 0)
        {
            if (CurrEnemies[i] == null || CurrEnemies[i].Equals(null))
                CurrEnemies.RemoveAt(i);
            i--;
        }
    }
    // Update is called once per frame
    void FixedUpdate () {
        if (CurrEnemies.Count <= 0)
        {
            if(currCount > 0)
            {
                //spawn next wave
                if(EnemyLists[currEnemySet] != null)
                {
                    CurrEnemies = new List<GameObject>();
                    foreach (GameObject enemy in EnemyLists[currEnemySet].enemies)
                    {
                        enemy.SetActive(true);
                        CurrEnemies.Add(enemy);
                    }
                    currEnemySet ++;
                }
                currCount = countdown;
            }
            else
            {
                currCount -= Time.fixedDeltaTime;
            }
        }
    }
}
