﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState {
	Tutorial_Calibrate,
	Tutorial_Finalize,
    Preparation,
    EndlessWave,

}
