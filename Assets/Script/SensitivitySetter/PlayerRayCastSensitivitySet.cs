﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerRayCastSensitivitySet : MonoBehaviour {
	public float chargeSpeed = 2f;
	public GameObject upCollider;
	public GameObject downCollider;
	public GameObject showFPS_Yes;
	public GameObject showFPS_No;


	// Update is called once per frame
	void FixedUpdate () {
		Vector3 forward = transform.TransformDirection(Vector3.forward) * 1000;
		Debug.DrawRay(transform.position, forward, Color.green);

		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		RaycastHit hit;

		upCollider.transform.GetComponentInChildren<Image> ().color = Color.white;
		upCollider.transform.GetComponentInChildren<Text> ().color = Color.black;
		downCollider.transform.GetComponentInChildren<Image> ().color = Color.white;
		downCollider.transform.GetComponentInChildren<Text> ().color = Color.black;

		if (Physics.Raycast (transform.position, fwd, out hit, 1000)) {

			if (hit.collider.gameObject.tag.Equals ("SensitivitySetterCollider")) {
				if (hit.collider.gameObject.Equals (upCollider)) {
					//settingVariables.Volume_Sensitivity = Mathf.Min (settingVariables.Volume_Sensitivity + Time.deltaTime * chargeSpeed, settingVariables.Volume_SensitivityMax);
					settingVariables.setVolume_Sensitivity(Mathf.Min (settingVariables.getVolume_Sensitivity() + Time.deltaTime * chargeSpeed, settingVariables.Volume_SensitivityMax));
					upCollider.transform.GetComponentInChildren<Image> ().color = Color.black;
					upCollider.transform.GetComponentInChildren<Text> ().color = Color.white;
				} 
				if (hit.collider.gameObject.Equals (downCollider)) {
					//settingVariables.Volume_Sensitivity = Mathf.Max (settingVariables.getVolume_Sensitivity() - Time.deltaTime * chargeSpeed, settingVariables.Volume_SensitivityMin);
					settingVariables.setVolume_Sensitivity(Mathf.Max (settingVariables.getVolume_Sensitivity() - Time.deltaTime * chargeSpeed, settingVariables.Volume_SensitivityMin));
					downCollider.transform.GetComponentInChildren<Image> ().color = Color.black;
					downCollider.transform.GetComponentInChildren<Text> ().color = Color.white;
				} 

			} 
			if(hit.collider.gameObject.tag.Equals ("DisplayFPSCollider")) {
				if (hit.collider.gameObject.Equals (showFPS_Yes)) {
					settingVariables.setShowFPS(true);
				} 
				if (hit.collider.gameObject.Equals (showFPS_No)) {
					settingVariables.setShowFPS(false);
				}

			}
		} 
		if (settingVariables.getShowFPS()) {
			showFPS_Yes.transform.GetComponentInChildren<Image> ().color = Color.black;
			showFPS_Yes.transform.GetComponentInChildren<Text> ().color = Color.white;
			showFPS_No.transform.GetComponentInChildren<Image> ().color = Color.white;
			showFPS_No.transform.GetComponentInChildren<Text> ().color = Color.black;
		} else {
			showFPS_Yes.transform.GetComponentInChildren<Image> ().color = Color.white;
			showFPS_Yes.transform.GetComponentInChildren<Text> ().color = Color.black;
			showFPS_No.transform.GetComponentInChildren<Image> ().color = Color.black;
			showFPS_No.transform.GetComponentInChildren<Text> ().color = Color.white;
		}
	}
}
