﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponTypeButton : RaycastButtonParent {
	public static Enum_ProjectileType WeaponTypeSelected;
	public Enum_ProjectileType projectileType;
	public ProjectileBaseInfoDictionary baseInfoDict;
	public Text btnText;

	public bool isLocked;

	public delegate void CurWeaponChange();
	public static event CurWeaponChange OnLoadOutWeaponChange;

	// Use this for initialization
	void Start () {
		btnText.text = baseInfoDict.getProjectileInfo (projectileType).name;
	}
	
	// Update is called once per frame
	void Update () {
		//check if button locked
		//lock the none button for slot 1
		if (WeaponSlotButton.WeaponSlotSelected == 0 &&  projectileType==Enum_ProjectileType.None) {
			isLocked = true;
		} else {
			isLocked = false;
		}

		if (isLocked) {
			setVisual_Locked ();
		} else if (ProjectileLoadOutList.projectileLoadOutList.LoadOutList [ProjectileShooter.projectileShoorter.curLoadOutIndex].projectileTypeList[WeaponSlotButton.WeaponSlotSelected].Equals (projectileType)) {
			setVisual_Select ();
		} else {
			setVisual_Deselect ();
		}
	}
	public override void raycastEnter (){
		if (!isLocked) {
			//Set projectile
			WeaponTypeSelected = projectileType;
			ProjectileLoadOutList.projectileLoadOutList.setLoadoutWeapon (ProjectileShooter.projectileShoorter.curLoadOutIndex, WeaponSlotButton.WeaponSlotSelected, projectileType);
			//ProjectileLoadOutList.projectileLoadOutList.LoadOutList [ProjectileShooter.projectileShoorter.curLoadOutIndex].setWeaponToSlot (WeaponSlotButton.WeaponSlotSelected, projectileType);
			//notify listener that lodout weapon changed
			if (OnLoadOutWeaponChange != null) {
				OnLoadOutWeaponChange ();
			}

		}
	}
	public override void raycastExit (){
	}

	public void setVisual_Select(){
		GetComponentInChildren<Image> ().color = Color.black;
		//GetComponentInChildren<Text> ().color = Color.white;

	}
	public void setVisual_Deselect(){
		GetComponentInChildren<Image> ().color = Color.white;
		//GetComponentInChildren<Text> ().color = Color.black;
	}
	public void setVisual_Locked(){
		GetComponentInChildren<Image> ().color = Color.grey;
		//GetComponentInChildren<Text> ().color = Color.black;
	}
}
