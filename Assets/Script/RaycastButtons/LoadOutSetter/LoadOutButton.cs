﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadOutButton : RaycastButtonParent {
	public int loadOutIndex;
	//public static int loadOutSelected = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (ProjectileShooter.projectileShoorter.curLoadOutIndex.Equals (loadOutIndex)) {
			setVisual_Select ();
		} else {
			setVisual_Deselect ();
		}
	}

	public override void raycastEnter (){
		//change projectile shooter loadout
		ProjectileShooter.projectileShoorter.setCurLoadOutIdx(loadOutIndex);
		WeaponSlotButton.WeaponSlotSelected = 0;
	}

	public override void raycastExit (){
	}

	public void setVisual_Select(){
		GetComponentInChildren<Image> ().color = Color.gray;
		GetComponentInChildren<Text> ().color = Color.white;
	
	}
	public void setVisual_Deselect(){
		GetComponentInChildren<Image> ().color = Color.white;
		GetComponentInChildren<Text> ().color = Color.black;
	}

}
