﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSlotButton : RaycastButtonParent {
	public int weaponSlotIndex;
	public static int WeaponSlotSelected = 0;
	public Text weaponSlotText;
	public ProjectileBaseInfoDictionary baseInfoDict;

	public bool isLocked;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		//check if button locked
		//lock the higher index weapon slot if too many None weapon type are selected
		if (weaponSlotIndex > ProjectileLoadOutList.projectileLoadOutList.LoadOutList [ProjectileShooter.projectileShoorter.curLoadOutIndex].effectiveWeaponAmount) {
			isLocked = true;
		} else {
			isLocked = false;
		}

		if (isLocked) {
			setVisual_Locked ();
		} else if (WeaponSlotSelected.Equals (weaponSlotIndex)) {
			setVisual_Select ();
		} else {
			setVisual_Deselect ();
		}

		weaponSlotText.text = baseInfoDict.getProjectileInfo (ProjectileLoadOutList.projectileLoadOutList.LoadOutList[ProjectileShooter.projectileShoorter.curLoadOutIndex].projectileTypeList[weaponSlotIndex]).name;
	}

	public override void raycastEnter (){
		if (!isLocked) {
			//unselect other loadout Button
			WeaponSlotSelected = weaponSlotIndex;
			WeaponTypeButton.WeaponTypeSelected = ProjectileLoadOutList.projectileLoadOutList.LoadOutList [ProjectileShooter.projectileShoorter.curLoadOutIndex].projectileTypeList [WeaponSlotSelected];
		}
	}
	public override void raycastExit (){
	}

	public void setVisual_Select(){
		GetComponentInChildren<Image> ().color = Color.black;
		GetComponentInChildren<Text> ().color = Color.white;

	}
	public void setVisual_Deselect(){
		GetComponentInChildren<Image> ().color = Color.white;
		GetComponentInChildren<Text> ().color = Color.black;
	}
	public void setVisual_Locked(){
		GetComponentInChildren<Image> ().color = Color.grey;
		GetComponentInChildren<Text> ().color = Color.black;
	}
}
