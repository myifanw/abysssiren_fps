﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestEnemySpawnBtn : RaycastButtonParent {
	private bool RayCastHit = false; 
	public btnLocker btn_Locker;
	public float waitTime = 1f;
	private float timer = 0;
	public Image progressImg;
	public GameObject SpawnPosition;
	public GameObject EnemyPrefab;


	// Use this for initialization
	void Start () {
		
	}
	void Update () {
		
		if (RayCastHit) {
			timer = timer + TimescaleManager.manager.getRealTimeDeltaTime ();
			if (timer > waitTime) {
				SpawnEnemies ();
				btn_Locker.setActivate (false);
				timer = 0;
			}
		} else {
			timer = 0;
		}
		progressImg.fillAmount = timer / waitTime;
	}
	public override void raycastEnter (){
		RayCastHit = true;
	}

	public override void raycastExit (){
		RayCastHit = false;
	}


	public void SpawnEnemies()
	{
		
		GameObject instantiatedEnemy = Instantiate(EnemyPrefab,SpawnPosition.transform.position,Random.rotation)as GameObject;
		if (instantiatedEnemy.GetComponent<FishSpawn>())
		{
			FishSpawn cont = instantiatedEnemy.GetComponentInChildren<FishSpawn>();
			cont.playerObject = PlayerCharacter.player.gameObject;
		}
	}
}
