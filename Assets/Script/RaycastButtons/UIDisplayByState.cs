﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDisplayByState : MonoBehaviour
{
    public CanvasGroup cGroup;

    public float speed = 3f;
    public float AngleDeadZone = 10f;

    public Vector3 ori_Tutorial_Calibrate;
	public bool display_Tutorial_Calibrate;
	public Vector3 ori_Tutorial_Finalize;
	public bool display_Tutorial_Finalize;
    public Vector3 ori_Preparation;
    public bool display_Preparation;
    public Vector3 ori_Endless;
    public bool display_Endless;
    public Vector3 ori_Other;
    public bool display_Other;

	public bool prevBlockRaycast = false;

    // Use this for initialization
    void Start()
    {
		if (GameController.controller.gameState == GameState.Tutorial_Calibrate) {
			transform.localRotation = Quaternion.Euler (ori_Tutorial_Calibrate);
			cGroup.alpha = (display_Tutorial_Calibrate)? 1f: 0f;
			cGroup.interactable = display_Tutorial_Calibrate;
			cGroup.blocksRaycasts = display_Tutorial_Calibrate;
		} else if (GameController.controller.gameState == GameState.Tutorial_Finalize){
			transform.localRotation = Quaternion.Euler(ori_Tutorial_Finalize);
			cGroup.alpha = (display_Tutorial_Finalize)? 1f: 0f;
			cGroup.interactable = display_Tutorial_Finalize;
			cGroup.blocksRaycasts = display_Tutorial_Finalize;
		} else if (GameController.controller.gameState == GameState.Preparation){
			transform.localRotation = Quaternion.Euler(ori_Preparation);
			cGroup.alpha = (display_Preparation)? 1f: 0f;
			cGroup.interactable = display_Preparation;
			cGroup.blocksRaycasts = display_Preparation;
		} else if (GameController.controller.gameState == GameState.EndlessWave){
			transform.localRotation = Quaternion.Euler(ori_Endless);
			cGroup.alpha = (display_Endless)? 1f: 0f;
			cGroup.interactable = display_Endless;
			cGroup.blocksRaycasts = display_Endless;
		}else{
			transform.localRotation = Quaternion.Euler(ori_Other);
			cGroup.alpha = (display_Other)? 1f: 0f;
			cGroup.interactable = display_Other;
			cGroup.blocksRaycasts = display_Other;
		}
		Collider[] colList = gameObject.GetComponentsInChildren<Collider> (true);
		foreach (Collider c in colList) {
			if (cGroup.blocksRaycasts) {
				c.transform.gameObject.layer = LayerMask.NameToLayer ("UI");
			} else {
				c.transform.gameObject.layer = LayerMask.NameToLayer ("Ignore");
			}
		}
    }

    // Update is called once per frame
    void Update()
    {

		if (GameController.controller.gameState == GameState.Tutorial_Calibrate)
        {
			cGroup.interactable = (display_Tutorial_Calibrate && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Tutorial_Calibrate))) < AngleDeadZone);
			cGroup.blocksRaycasts = (display_Tutorial_Calibrate && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Tutorial_Calibrate))) < AngleDeadZone);
			if (display_Tutorial_Calibrate) {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            } else
            {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            }
			transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(ori_Tutorial_Calibrate), TimescaleManager.manager.getRealTimeDeltaTime() * speed);
		}else if (GameController.controller.gameState == GameState.Tutorial_Finalize)
		{
			cGroup.interactable = (display_Tutorial_Finalize && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Tutorial_Finalize))) < AngleDeadZone);
			cGroup.blocksRaycasts = (display_Tutorial_Finalize && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Tutorial_Finalize))) < AngleDeadZone);
			if (display_Tutorial_Finalize) {
				cGroup.alpha = Mathf.Lerp(cGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
			} else
			{
				cGroup.alpha = Mathf.Lerp(cGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
			}
			transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(ori_Tutorial_Finalize), TimescaleManager.manager.getRealTimeDeltaTime() * speed);
		}else if (GameController.controller.gameState == GameState.Preparation)
        {
            cGroup.interactable = (display_Preparation && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Preparation))) < AngleDeadZone);
            cGroup.blocksRaycasts = (display_Preparation && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Preparation))) < AngleDeadZone);
            if (display_Preparation)
            {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            }
            else
            {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            }
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(ori_Preparation), TimescaleManager.manager.getRealTimeDeltaTime() * speed);
        }
        else if (GameController.controller.gameState == GameState.EndlessWave)
        {
            cGroup.interactable = (display_Endless && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Endless))) < AngleDeadZone);
            cGroup.blocksRaycasts = (display_Endless && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Endless))) < AngleDeadZone);
            if (display_Endless)
            {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            }
            else
            {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            }
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(ori_Endless), TimescaleManager.manager.getRealTimeDeltaTime() * speed);
        }
        else
        {
            cGroup.interactable = (display_Other && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Other))) < AngleDeadZone);
            cGroup.blocksRaycasts = (display_Other && (Quaternion.Angle(transform.localRotation, Quaternion.Euler(ori_Other))) < AngleDeadZone);
            if (display_Other)
            {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            }
            else
            {
                cGroup.alpha = Mathf.Lerp(cGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime() * speed);
            }
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(ori_Other), TimescaleManager.manager.getRealTimeDeltaTime() * speed);
        }
		if (prevBlockRaycast != cGroup.blocksRaycasts) {
			Collider[] colList = gameObject.GetComponentsInChildren<Collider> (true);
			foreach (Collider c in colList) {
				if (cGroup.blocksRaycasts) {
					c.transform.gameObject.layer = LayerMask.NameToLayer ("UI");
				} else {
					c.transform.gameObject.layer = LayerMask.NameToLayer ("Ignore");
				}
			}
		}
		prevBlockRaycast = cGroup.blocksRaycasts;
    }
}
