﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycaster_general : MonoBehaviour {
	private List<RaycastButtonParent> hittingList;
	// Use this for initialization
	void Awake () {
		hittingList = new List<RaycastButtonParent> ();
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}

	void Update () {
		Vector3 forward = transform.TransformDirection (Vector3.forward) * 1000;
		Debug.DrawRay (transform.position, forward, Color.green);

		Vector3 fwd = transform.TransformDirection (Vector3.forward);
		RaycastHit[] hits = Physics.RaycastAll(transform.position, fwd, 1000, LayerMask.GetMask("UI"));

		List<RaycastButtonParent> DidntHitList = new List<RaycastButtonParent>( hittingList);

		for (int i = 0; i < hits.Length; i++) {
			if (hits[i].transform.GetComponents<RaycastButtonParent> ().Length > 0) {
				RaycastButtonParent temp = hits [i].transform.GetComponent<RaycastButtonParent> ();
				if (!hittingList.Contains (temp)) {
					temp.raycastEnter ();
					hittingList.Add (temp);
				} else {
					//hit so remove from didn't hit list
					DidntHitList.Remove (temp);
				}
			}
		}

		//process exit for hits that no longer hit
		for (int i = 0; i < DidntHitList.Count; i++) {
			DidntHitList [i].raycastExit();
			hittingList.Remove (DidntHitList [i]);
		}

//		RaycastHit hit;
//
//		if (Physics.Raycast (transform.position, fwd, out hit, 1000,LayerMask.GetMask("UI"))) {
//			Debug.LogError (hit.transform.gameObject.name);
//			if (hit.transform.GetComponents<RaycastButtonParent> ().Length > 0) {
//				hit.transform.GetComponent<RaycastButtonParent> ().raycastEnter ();
//			}
//		}
	}
}
