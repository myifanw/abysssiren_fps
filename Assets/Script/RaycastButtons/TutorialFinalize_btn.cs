﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialFinalize_btn : RaycastButtonParent {
	private bool RayCastHit = false; 
	public float waitTime = 1.5f;
	public float timer = 0;
	// Use this for initialization
	void Start () {
	}
	void Update () {
		if (RayCastHit) {
			timer = timer + TimescaleManager.manager.getRealTimeDeltaTime ();
			if (timer > waitTime) {
				GameController.controller.setGameState (GameState.Preparation);
			}
		} else {
			timer = 0;
		}
	}
	public override void raycastEnter (){
		RayCastHit = true;
	}

	public override void raycastExit (){
		RayCastHit = false;
	}
}
