﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SensitvityButton : RaycastButtonParent {
	public bool up;
	private bool hit;
	public float chargeSpeed;
	//public static int loadOutSelected = 0;

	// Use this for initialization
	void Start () {
		hit = false;
	}

	// Update is called once per frame
	void Update () {
		if (hit) {
			setVisual_Select ();
			if (up) {
				settingVariables.setVolume_Sensitivity (Mathf.Min (settingVariables.getVolume_Sensitivity () + TimescaleManager.manager.getRealTimeDeltaTime() * chargeSpeed, settingVariables.Volume_SensitivityMax));
			} else {
				settingVariables.setVolume_Sensitivity (Mathf.Min (settingVariables.getVolume_Sensitivity () - TimescaleManager.manager.getRealTimeDeltaTime() * chargeSpeed, settingVariables.Volume_SensitivityMax));
			}
		} else {
			setVisual_Deselect ();
		}
	}

	public override void raycastEnter (){
		//change projectile shooter loadout
		hit = true;
	}

	public override void raycastExit (){
		hit = false;
	}

	public void setVisual_Select(){
		GetComponentInChildren<Image> ().color = Color.black;
		GetComponentInChildren<Text> ().color = Color.white;

	}
	public void setVisual_Deselect(){
		GetComponentInChildren<Image> ().color = Color.white;
		GetComponentInChildren<Text> ().color = Color.black;
	}
}
