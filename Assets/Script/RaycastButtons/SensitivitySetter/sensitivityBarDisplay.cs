﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sensitivityBarDisplay : MonoBehaviour {
	public Image img;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		img.fillAmount = settingVariables.getVolume_Sensitivity()/settingVariables.Volume_SensitivityMax;
	}
}
