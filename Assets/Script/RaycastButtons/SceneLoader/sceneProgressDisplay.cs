﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class sceneProgressDisplay : MonoBehaviour {
	public Image img;
	public sceneLoaderButton scnBtn;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		img.fillAmount = (scnBtn.timer / scnBtn.waitTime);
	}
}
