﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class thresholdDisplay : MonoBehaviour {
	private Image img;
	public bool isNoiseBar;
	public bool isShootBar;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (img == null) {
			img = GetComponent<Image> ();
		}
		if (isNoiseBar) {
			img.fillAmount = settingVariables.getShooter_minShootThreshold();
		}
		if (isShootBar) {
			img.fillAmount = settingVariables.getShooter_powerShotThreshold();
		}
	}
}
