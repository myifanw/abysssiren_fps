﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class thresholdButton : RaycastButtonParent {
	public bool isShootThresh;
	public bool isSuperThresh;
	public bool up;
	private bool hit;
	public float chargeSpeed;
	//public static int loadOutSelected = 0;

	// Use this for initialization
	void Start () {
		hit = false;
	}

	// Update is called once per frame
	void Update () {
		if (hit) {
			setVisual_Select ();
			if (isShootThresh) {
				if (up) {
					settingVariables.setShooter_minShootThreshold (Mathf.Max (0f, Mathf.Min (settingVariables.getShooter_minShootThreshold () + TimescaleManager.manager.getRealTimeDeltaTime() * chargeSpeed, settingVariables.getShooter_powerShotThreshold ()-0.01f)));
				} else {
					settingVariables.setShooter_minShootThreshold (Mathf.Max (0f, Mathf.Min (settingVariables.getShooter_minShootThreshold () - TimescaleManager.manager.getRealTimeDeltaTime() * chargeSpeed, settingVariables.getShooter_powerShotThreshold ()-0.01f)));
				}
			} 
			if(isSuperThresh){
				if (up) {
					settingVariables.setShooter_powerShotThreshold (Mathf.Max (settingVariables.getShooter_minShootThreshold () +0.01f, Mathf.Min (settingVariables.getShooter_powerShotThreshold () + TimescaleManager.manager.getRealTimeDeltaTime() * chargeSpeed, 1f)));
				} else {
					settingVariables.setShooter_powerShotThreshold (Mathf.Max (settingVariables.getShooter_minShootThreshold () +0.01f, Mathf.Min (settingVariables.getShooter_powerShotThreshold () - TimescaleManager.manager.getRealTimeDeltaTime() * chargeSpeed, 1f)));
				}
			}
		} else {
			setVisual_Deselect ();
		}
	}

	public override void raycastEnter (){
		//change projectile shooter loadout
		hit = true;
	}

	public override void raycastExit (){
		hit = false;
	}

	public void setVisual_Select(){
		GetComponentInChildren<Image> ().color = Color.black;
		GetComponentInChildren<Text> ().color = Color.white;

	}
	public void setVisual_Deselect(){
		GetComponentInChildren<Image> ().color = Color.white;
		GetComponentInChildren<Text> ().color = Color.black;
	}
}
