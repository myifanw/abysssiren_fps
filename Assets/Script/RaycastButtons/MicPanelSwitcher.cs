﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicPanelSwitcher : MonoBehaviour {
    public float speed = 3f;

    public bool calibrationMode;
    public GameObject calibrationPanel;
    public GameObject micSettingPanel;


    // Use this for initialization
    void Start () {

		calibrationMode = (GameController.controller.gameState == GameState.Tutorial_Calibrate);
		if (calibrationMode) {
			transform.localRotation = Quaternion.Euler (Vector3.zero);
		} else {
			transform.localRotation = Quaternion.Euler (new Vector3 (0, 180, 0));
		}
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.N))
        {
            calibrationMode = true;
        }
        if (Input.GetKey(KeyCode.M))
        {
            calibrationMode = false;
        }
        if (calibrationMode)
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(Vector3.zero), TimescaleManager.manager.getRealTimeDeltaTime() * speed);
        }
        else
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(new Vector3(0,180,0)), TimescaleManager.manager.getRealTimeDeltaTime() * speed);
			if (GameController.controller.gameState == GameState.Tutorial_Calibrate) {
				if (Mathf.Abs (Mathf.Abs (transform.localRotation.eulerAngles.y) - 180) < 5) {
					GameController.controller.setGameState (GameState.Tutorial_Finalize);
				}
			}
		}
        calibrationPanel.SetActive((Mathf.Abs(Mathf.Abs(transform.localRotation.eulerAngles.y)- 180)) >= 90);
        micSettingPanel.SetActive((Mathf.Abs(Mathf.Abs(transform.localRotation.eulerAngles.y)- 180)) < 90);
        
    }
}
