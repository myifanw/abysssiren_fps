﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuContent : MonoBehaviour {
	public MenuScript menu;
	private bool childrenCollidersEnabled = true;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (menu.state == menuState.open && menu.reachedTargetScale()) {
			if (!childrenCollidersEnabled) {
				childrenCollidersEnabled = true;
				foreach (Collider c in GetComponentsInChildren<Collider>()) {
					c.enabled = true;
				}
			}
		} else {
			if (childrenCollidersEnabled) {
				childrenCollidersEnabled = false;
				foreach (Collider c in GetComponentsInChildren<Collider>()) {
					c.enabled = false;
				}
			}
		}
	}
}
