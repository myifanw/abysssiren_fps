﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuManager : MonoBehaviour {
	public MenuScript curMenu;

	public virtual void updateCurMenu(MenuScript menu ){
		if (menu.isMainMenu || (!menu.isMainMenu && curMenu!= null && curMenu.isMainMenu)) {
			curMenu = menu;
		}
	}
	public virtual void cancelMenu(MenuScript menu ){
		if (curMenu == menu) {
			curMenu = null;
		}
	}
	public MenuScript getcurMenu(){
		return curMenu;
	}
}
