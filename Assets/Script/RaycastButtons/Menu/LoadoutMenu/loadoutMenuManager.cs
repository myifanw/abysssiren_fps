﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loadoutMenuManager : menuManager {
	public static loadoutMenuManager manager;
	// Use this for initialization
	void Awake () {
		if (manager != null && manager != this) {
			Destroy (manager.gameObject);
		}
		manager = this;
	}
	public void updateCurMenu(loadoutMenu menu ){
		if (menu.isMainMenu || (!menu.isMainMenu && curMenu!= null && curMenu.isMainMenu)) {
			if (menu.switchLoadout) {
				ProjectileShooter.projectileShoorter.setCurLoadOutIdx (menu.loadoutIdx);
			}
			curMenu = menu;
		}
	}
	public void cancelMenu(loadoutMenu menu ){
		if (curMenu == menu) {
			curMenu = null;
		}
	}
}
