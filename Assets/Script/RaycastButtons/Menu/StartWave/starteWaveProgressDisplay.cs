﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class starteWaveProgressDisplay : MonoBehaviour
{
    public Image img;
    public starteWaveBtn swBtn;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        img.fillAmount = (swBtn.timer / swBtn.waitTime);
    }
}

