﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class starteWaveBtn : RaycastButtonParent
{
    
    public bool RayCastHit = false;
    public float waitTime = 2f;
    public float timer = 0;
    private bool loadable = true;
    // Use this for initialization
    void Start()
    {
        timer = 0;
        loadable = true;
    }
    void Update()
    {
        if (RayCastHit)
        {
            timer = timer + TimescaleManager.manager.getRealTimeDeltaTime();
            if (loadable && timer > waitTime)
            {
                GameController.controller.startEndlessMode();
                loadable = false;
            }
        }
        else
        {
            timer = 0;
        }
    }
    public override void raycastEnter()
    {
        RayCastHit = true;
    }

    public override void raycastExit()
    {
        RayCastHit = false;
    }
}
