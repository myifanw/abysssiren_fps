﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseMenuManager : menuManager {
	public static pauseMenuManager manager;
	// Use this for initialization
	void Awake () {
		if (manager != null && manager != this) {
			Destroy (manager.gameObject);
		}
		manager = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public override void updateCurMenu(MenuScript menu ){
		if (menu.isMainMenu || (!menu.isMainMenu && curMenu!= null && curMenu.isMainMenu)) {
			curMenu = menu;
			TimescaleManager.manager.pause ();
		}
	}
	public override void cancelMenu(MenuScript menu ){
		if (curMenu == menu) {
			TimescaleManager.manager.unpause ();
			curMenu = null;
		}
	}
	public MenuScript getcurMenu(){
		return curMenu;
	}
}
