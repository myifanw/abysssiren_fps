﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum menuState{
	unopenable,
	openable,
	open,

}

public class MenuScript : MonoBehaviour {
	public menuManager menuManager;
	public bool isMainMenu;
	public GameObject circle;
	public GameObject content;
	public menuState state;
	public float circleScale_Openable;

	public float targetContentScale = 0f;
	public float targetCircleScale = 0f;

	public float zoomSpeed = 5f;
	private float mainMenuScaleProgressForSubMenuOpen = 0.8f; //sub menu circle will be visible if main menu is this much open in local scale. This is a multiplier of the scale. 0.5 means 1.5 if 3f is open and 0f is close
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		setState ();
		updateScale ();
		circle.transform.localScale = Vector3.Slerp (circle.transform.localScale, Vector3.one * targetCircleScale, zoomSpeed * TimescaleManager.manager.getRealTimeDeltaTime ());
		content.transform.localScale = Vector3.Slerp (content.transform.localScale, Vector3.one * targetContentScale, zoomSpeed * TimescaleManager.manager.getRealTimeDeltaTime ());
	}

	private void setState(){
		if (menuManager.getcurMenu () == this) {
			state = menuState.open;
		} else {
			//menu not selected
			if (isMainMenu) {
				//main menu remain openable
				state = menuState.openable;
			} else {
				//submenu is openable if cur menu is main menu
				if (menuManager.getcurMenu () != null && menuManager.getcurMenu ().isMainMenu && menuManager.getcurMenu ().isCurScaleGreaterThan(mainMenuScaleProgressForSubMenuOpen)) {
					state = menuState.openable;
				} else {
					state = menuState.unopenable;
				}
			}
		}
	}

	private void updateScale(){
		switch (state) {
		case menuState.open:
			targetCircleScale = 1f;
			targetContentScale = 1f;
			break;
		case menuState.unopenable:
			targetCircleScale = 0f;
			targetContentScale = 0f;
			break;
		case menuState.openable:
			targetCircleScale = circleScale_Openable;
			targetContentScale = 0f;
			break;
		default:
			Debug.LogError ("Unrecognized state - "+state);
			break;
		}
	}

	public bool reachedTargetScale(){
		if (Mathf.Abs ((content.transform.localScale.x - targetContentScale)) < 0.05f)
			return true;
		return false;
	}

	public bool isCurScaleGreaterThan(float progress){
		return (Mathf.Abs ((content.transform.localScale.x) / (1f - 0f)) > progress);
	}
}
