﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuRaycast : RaycastButtonParent {
	public menuManager manager;
	public bool RayCastHit = false; 
	public MenuScript menu;
	public bool isOpen;
	// Use this for initialization
	void Start () {

	}

	public override void raycastEnter (){
		if (isOpen) {
			manager.updateCurMenu (menu);
		}
	}

	public override void raycastExit (){
		if (!isOpen) {
			manager.cancelMenu (menu);
		}
	}
}
