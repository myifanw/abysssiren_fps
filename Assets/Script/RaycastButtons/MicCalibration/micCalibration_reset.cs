﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class micCalibration_reset : RaycastButtonParent {
	public micCalibration calibration;
	public bool RayCastHit = false; 
	public float waitTime = 2f;
	public float timer = 0;
	// Use this for initialization
	void Start () {
		timer = 0;
	}
	void Update () {
		if (RayCastHit) {
			timer = timer + TimescaleManager.manager.getRealTimeDeltaTime ();
			if (timer > waitTime) {
				calibration.reset ();
			}
		} else {
			timer = 0;
		}
	}
	public override void raycastEnter (){
		RayCastHit = true;
	}

	public override void raycastExit (){
		RayCastHit = false;
	}
}
