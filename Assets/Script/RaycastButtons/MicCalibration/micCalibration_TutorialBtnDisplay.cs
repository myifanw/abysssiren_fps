﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class micCalibration_TutorialBtnDisplay : MonoBehaviour {
	public Image img;
	public micCalibration_TutorialBtn btn;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		img.fillAmount = (btn.timer / btn.waitTime);
	}
}
