﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pingPongSize : MonoBehaviour {
	public float speed;
	public float minSclae = 1.2f;
	public float maxSclae = 0.8f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = Vector3.one * Mathf.Lerp (minSclae, maxSclae, Mathf.PingPong (Time.time * speed, 1f));
	}
}
