﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class micCalibration_display : MonoBehaviour {
	public Image img;
	public micCalibration_btn calBtn;
	public micCalibration_reset resBtn;
	public bool isCal;
	public bool isReset;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (isCal) {
			img.fillAmount = (calBtn.timer / calBtn.waitTime);
		} else if(isReset) {
			img.fillAmount = (resBtn.timer / resBtn.waitTime);
		}
	}
}
