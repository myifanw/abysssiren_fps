﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class micCalibration_TutorialBtn : RaycastButtonParent {
	private bool RayCastHit = false; 
	public micCalibration calibration;
	public float waitTime = 2f;
	public float timer = 0;
	// Use this for initialization
	void Start () {
	}
	void Update () {
		if (RayCastHit) {
			timer = timer + TimescaleManager.manager.getRealTimeDeltaTime ();
			if (timer > waitTime) {
				calibration.setState (micCalibrationState.TutorialText1);
				timer = 0;
			}
		} else {
			timer = 0;
		}
	}
	public override void raycastEnter (){
		RayCastHit = true;
	}

	public override void raycastExit (){
		RayCastHit = false;
	}
}
