﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum micCalibrationState{
	TutorialStart,
	TutorialText1,
	NoiseSampling,
	MidSampling,
	HighSampling,
	Completed,

}
public class micCalibration : MonoBehaviour {
	public bool isRecording = false;
	public micCalibrationState curState = micCalibrationState.NoiseSampling;

	private Queue<float> rawVolumeQ;
	private int queueSize = 10;
	private float queueSum = 0;
	private float[] ArrayOfLocalMax;
	private int arraySize = 3;


	public float noiseSample;
	public float midSample;
	public float highSample;

	public MicPanelSwitcher panelSwitcher;
	// Use this for initialization
	void Start () {
		reset ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isRecording) {
			rawVolumeQ.Enqueue(MicTestFinishedBuild_Input.micInput.getRawVolume ());
			queueSum = queueSum + MicTestFinishedBuild_Input.micInput.getRawVolume ();
			if (rawVolumeQ.Count > queueSize) {
				queueSum = queueSum - rawVolumeQ.Dequeue ();
			}
			if ((queueSum / queueSize) > ArrayOfLocalMax [ArrayOfLocalMax.Length - 1]) {
				ArrayOfLocalMax [ArrayOfLocalMax.Length - 1] = queueSum / queueSize;
				Array.Sort(ArrayOfLocalMax);
				Array.Reverse (ArrayOfLocalMax);
			}

		}
	}

	public void beginRecord(){
		isRecording = true;

	}

	public void endRecord(){
		isRecording = false;
		float avg = 0f;
		for (int i = 0; i < arraySize; i++) {
			avg = avg + ArrayOfLocalMax [i];
		}
		avg = avg / arraySize;

		switch (curState) {
		case micCalibrationState.NoiseSampling:
			noiseSample = avg;
			setState (micCalibrationState.MidSampling);
			break;
		case micCalibrationState.MidSampling:
			midSample = avg;
			setState (micCalibrationState.HighSampling);
			break;
		case micCalibrationState.HighSampling:
			highSample = avg;
			setState (micCalibrationState.Completed);
			calibrateVlaues ();
			panelSwitcher.calibrationMode = false;
			break;
		default:
			Debug.LogError ("NOT SUPPSOED TO BE HERE");
			break;
		}

		queueSum = 0;
		rawVolumeQ = new Queue<float> ();
		ArrayOfLocalMax = new float[arraySize + 1];
		for (int i = 0; i < ArrayOfLocalMax.Length; i++) {
			ArrayOfLocalMax [i] = 0f;
		}

	}
	public void setState(micCalibrationState state){
		curState = state;
	}
	public void reset(){
		isRecording = false;
		setState(micCalibrationState.NoiseSampling);
		queueSum = 0;
		rawVolumeQ = new Queue<float> ();
		ArrayOfLocalMax = new float[arraySize + 1];
		for (int i = 0; i < ArrayOfLocalMax.Length; i++) {
			ArrayOfLocalMax [i] = 0f;
		}
		if (GameController.controller.gameState == GameState.Tutorial_Calibrate) {
			setState (micCalibrationState.TutorialStart);
		} else {
			setState (micCalibrationState.NoiseSampling);
		}
	}

	public void calibrateVlaues(){
		settingVariables.setVolume_Sensitivity (1f / highSample);
		settingVariables.setShooter_powerShotThreshold ((1f + midSample * settingVariables.getVolume_Sensitivity())/2);
		settingVariables.setShooter_minShootThreshold ((noiseSample * settingVariables.getVolume_Sensitivity()) + ( ((midSample * settingVariables.getVolume_Sensitivity()) - (noiseSample * settingVariables.getVolume_Sensitivity())) /4));
		Debug.LogError ("HIGH - "+highSample +" MID - "+ midSample + " NOISE - "+ noiseSample);

		Debug.LogError ("sensitivity - "+settingVariables.getVolume_Sensitivity() +" lowThresh - "+ settingVariables.getShooter_minShootThreshold() + " high Thresh - "+ settingVariables.getShooter_powerShotThreshold());
	}
}
