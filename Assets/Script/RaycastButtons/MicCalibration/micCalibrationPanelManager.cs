﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class micCalibrationPanelManager : MonoBehaviour {
	public micCalibration calibration;

	public GameObject recordBtn;
	public GameObject endRecordBtn;

	public CanvasGroup tutorialStartGroup;
	public CanvasGroup tutorialText1Group;
	public CanvasGroup CalibrationGroup;
	public float fadeTime = 0.7f;
	public float fadeinDelayTimer = 1.4f;
	public float tutorialTextDisplayTimer = 0f;

	public micCalibrationState oldState;

    public TMPro.TextMeshProUGUI infoText;

    public GameObject recordingDisplay;

	private bool layerSet_TutStartLayerChanged;
	private bool layerSet_TutTextLayerChanged;
	private bool layerSet_CalibrLayerChanged;
	private Collider[] colList;
	// Use this for initialization
	void Start () {
		oldState = calibration.curState;
		fadeinDelayTimer = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		//process tutorial panels
		if (oldState != calibration.curState) {
			fadeinDelayTimer = fadeTime * 2;
			tutorialTextDisplayTimer = 0;

			layerSet_TutStartLayerChanged = false;
			layerSet_TutTextLayerChanged = false;
			layerSet_CalibrLayerChanged = false;

		}
		oldState = calibration.curState;
		//fade out old group
		if (calibration.curState != micCalibrationState.TutorialStart) {
			tutorialStartGroup.alpha = Mathf.Lerp (tutorialStartGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime()*(1f/fadeTime)*1.3f);
			tutorialStartGroup.interactable = false;
			tutorialStartGroup.blocksRaycasts = false;
			if (!layerSet_TutStartLayerChanged) {
				colList = tutorialStartGroup.gameObject.GetComponentsInChildren<Collider> (true);
				foreach (Collider c in colList) {
					c.transform.gameObject.layer = LayerMask.NameToLayer ("Ignore");
				}
				layerSet_TutStartLayerChanged = true;
			}
		} 
		if (calibration.curState != micCalibrationState.TutorialText1) {
			tutorialText1Group.alpha = Mathf.Lerp (tutorialText1Group.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime()*(1f/fadeTime)*1.3f);
			tutorialText1Group.interactable = false;
			tutorialText1Group.blocksRaycasts = false;
			if (!layerSet_TutTextLayerChanged) {
				colList = tutorialText1Group.gameObject.GetComponentsInChildren<Collider> (true);
				foreach (Collider c in colList) {
					c.transform.gameObject.layer = LayerMask.NameToLayer ("Ignore");
				}
				layerSet_TutTextLayerChanged = true;
			}
		} 
		if (calibration.curState != micCalibrationState.NoiseSampling && calibration.curState != micCalibrationState.MidSampling && calibration.curState != micCalibrationState.HighSampling && calibration.curState != micCalibrationState.Completed) {
			CalibrationGroup.alpha = Mathf.Lerp (CalibrationGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime()*(1f/fadeTime)*1.3f);
			CalibrationGroup.interactable = false;
			CalibrationGroup.blocksRaycasts = false;
			if (!layerSet_CalibrLayerChanged) {
				colList = CalibrationGroup.gameObject.GetComponentsInChildren<Collider> (true);
				foreach (Collider c in colList) {
					c.transform.gameObject.layer = LayerMask.NameToLayer ("Ignore");
				}
				layerSet_CalibrLayerChanged = true;
			}
			
		}
		//fade in new group after delay ended
		if (fadeinDelayTimer == 0f) {
			if (calibration.curState == micCalibrationState.TutorialStart) {
				tutorialStartGroup.alpha = Mathf.Lerp (tutorialStartGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime()*(1f/fadeTime));
				if (tutorialStartGroup.alpha > 0.95f) {
					tutorialStartGroup.interactable = true;
					tutorialStartGroup.blocksRaycasts = true;
					if (!layerSet_TutStartLayerChanged) {
						colList = tutorialStartGroup.gameObject.GetComponentsInChildren<Collider> (true);
						foreach (Collider c in colList) {
							c.transform.gameObject.layer = LayerMask.NameToLayer ("UI");
						}
						layerSet_TutStartLayerChanged = true;
					}
				}
			} else if (calibration.curState == micCalibrationState.TutorialText1) {
				tutorialText1Group.alpha = Mathf.Lerp (tutorialText1Group.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime()*(1f/fadeTime));
				if (tutorialText1Group.alpha > 0.95f) {
					tutorialText1Group.interactable = true;
					tutorialText1Group.blocksRaycasts = true;
					if (!layerSet_TutTextLayerChanged) {
						colList = tutorialText1Group.gameObject.GetComponentsInChildren<Collider> (true);
						foreach (Collider c in colList) {
							c.transform.gameObject.layer = LayerMask.NameToLayer ("UI");
						}
						layerSet_TutTextLayerChanged = true;
					}
				}

				if (tutorialTextDisplayTimer > fadeTime * 5f) {
					calibration.setState (micCalibrationState.NoiseSampling);
				}
			} else {
				CalibrationGroup.alpha = Mathf.Lerp (CalibrationGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime()*(1f/fadeTime));
				if (CalibrationGroup.alpha > 0.95f) {
					CalibrationGroup.interactable = true;
					CalibrationGroup.blocksRaycasts = true;
					if (!layerSet_CalibrLayerChanged) {
						colList = CalibrationGroup.gameObject.GetComponentsInChildren<Collider> (true);
						foreach (Collider c in colList) {
							c.transform.gameObject.layer = LayerMask.NameToLayer ("UI");
						}
						layerSet_CalibrLayerChanged = true;
					}
				}
			}
			tutorialTextDisplayTimer = tutorialTextDisplayTimer + TimescaleManager.manager.getRealTimeDeltaTime ();
		}

		fadeinDelayTimer = Mathf.Max(0f, fadeinDelayTimer - TimescaleManager.manager.getRealTimeDeltaTime ());


		//process calibration panels
		if (calibration.curState == micCalibrationState.Completed) {
			recordBtn.SetActive (false);
			endRecordBtn.SetActive (false);
			recordingDisplay.SetActive (false);
		} else {
			recordBtn.SetActive (!calibration.isRecording);
			endRecordBtn.SetActive (calibration.isRecording);
			recordingDisplay.SetActive (calibration.isRecording);
		}


		switch (calibration.curState) {
		case micCalibrationState.NoiseSampling:
			infoText.text = "Stay Silent Record Ambient Noise Level";
			break;
		case micCalibrationState.MidSampling:
			infoText.text = "Speak a few words at Normal Volume";
			break;
		case micCalibrationState.HighSampling:
			infoText.text = "Speak at a High Volume";
			break;
		case micCalibrationState.Completed:
			infoText.text = "COMPLETE";
			break;
		}



	}
}
