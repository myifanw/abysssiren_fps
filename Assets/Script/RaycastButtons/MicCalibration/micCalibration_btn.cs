﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class micCalibration_btn : RaycastButtonParent {
	private bool RayCastHit = false; 
	public micCalibration calibration;
	public bool isRecord = false;
	public float waitTime = 2f;
	public float timer = 0;
	// Use this for initialization
	void Start () {
	}
	void Update () {
		if (RayCastHit) {
			timer = timer + TimescaleManager.manager.getRealTimeDeltaTime ();
			if (timer > waitTime) {
				if (isRecord) {
					calibration.beginRecord ();
				} else {
					calibration.endRecord ();
				}
			}
		} else {
			timer = 0;
		}
	}
	public override void raycastEnter (){
		RayCastHit = true;
	}

	public override void raycastExit (){
		RayCastHit = false;
	}
}
