﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FPSDisplayButton : RaycastButtonParent {
	public bool yes;
	//public static int loadOutSelected = 0;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (settingVariables.getShowFPS() == yes) {
			setVisual_Select ();
		} else {
			setVisual_Deselect ();
		}
	}

	public override void raycastEnter (){
		settingVariables.setShowFPS(yes);
	}

	public override void raycastExit (){
	}

	public void setVisual_Select(){
		GetComponentInChildren<Image> ().color = Color.black;
		GetComponentInChildren<Text> ().color = Color.white;

	}
	public void setVisual_Deselect(){
		GetComponentInChildren<Image> ().color = Color.white;
		GetComponentInChildren<Text> ().color = Color.black;
	}

}
