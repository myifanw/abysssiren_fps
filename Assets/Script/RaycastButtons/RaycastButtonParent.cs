﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RaycastButtonParent : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public abstract void raycastEnter ();
	public abstract void raycastExit ();
}
