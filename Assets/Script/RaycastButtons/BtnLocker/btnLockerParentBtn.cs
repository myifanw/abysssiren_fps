﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class btnLockerParentBtn : MonoBehaviour {
	public Image fillImg;
	public GameObject OtherChildren;
	public float fillAmt = 0f;
	public bool isActivated = false;
	private Quaternion fixedFillRotation;
	private Quaternion fixedFillRotation_otherChildren;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		fillImg.fillAmount = fillAmt;
		fixedFillRotation = fillImg.transform.rotation;
		if(OtherChildren!=null) fixedFillRotation_otherChildren = OtherChildren.transform.rotation;
		if (isActivated) {
			Debug.LogError (transform.forward);
			transform.RotateAround (transform.position, transform.forward, TimescaleManager.manager.getRealTimeDeltaTime () * 180f);
		} else {
			transform.localRotation = Quaternion.identity;
		}
	}

	void LateUpdate () {
		fillImg.transform.rotation = fixedFillRotation;
		if(OtherChildren!=null) OtherChildren.transform.rotation = fixedFillRotation_otherChildren;
	}
}
