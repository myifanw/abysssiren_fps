﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class btnLocker : RaycastButtonParent {
	private bool RayCastHit = false; 
	public btnLockerParentBtn parentBtn;
	public btnLockerPanel lockerPanel;
	private bool isActivated = false;
	public float waitTime = 1f;
	private float timer = 0;
	public float speed = 7f;

	public GameObject[] ChildBtns;
	private Vector3[] childBtnPosition;
	private float[] childBtnPosDelta;
	// Use this for initialization
	void Start () {
		setActivate (false);
		parentBtn.gameObject.SetActive (true);
		childBtnPosition = new Vector3[ChildBtns.Length];
		childBtnPosDelta = new float[ChildBtns.Length];
		for (int i = 0; i < ChildBtns.Length; i++) {
			childBtnPosition [i] = ChildBtns [i].transform.localPosition;
			childBtnPosDelta [i] = Vector3.Magnitude (parentBtn.transform.localPosition - ChildBtns [i].transform.localPosition)*0.05f;
			ChildBtns [i].SetActive (false);
			ChildBtns [i].GetComponent<Collider> ().enabled = false;
		}
	}
	void Update () {
		parentBtn.isActivated = RayCastHit;
		parentBtn.fillAmt = (timer / waitTime);

		if (isActivated) {
			if (!lockerPanel.RayCastHit) {
				setActivate (false);
				timer = 0;
			}
		} else {
			if (RayCastHit) {
				timer = timer + TimescaleManager.manager.getRealTimeDeltaTime ();
				if (timer > waitTime) {
					setActivate (true);
				}
			} else {
				timer = 0;
			}
		}

		//manage parent and child btn
		if (isActivated) {
			//deactivate parent if not yet
			if (parentBtn.gameObject.activeSelf) {
				parentBtn.gameObject.SetActive (false);
				for (int i = 0; i < ChildBtns.Length; i++) {
					ChildBtns [i].SetActive (true);
					ChildBtns [i].transform.localPosition = parentBtn.transform.localPosition;
				}
			}
			for (int i = 0; i < ChildBtns.Length; i++) {
				ChildBtns [i].transform.localPosition = Vector3.Lerp (ChildBtns [i].transform.localPosition, childBtnPosition [i], TimescaleManager.manager.getRealTimeDeltaTime () * speed);
				if (Vector3.Magnitude (ChildBtns [i].transform.localPosition - childBtnPosition [i])<childBtnPosDelta[i]) {
					ChildBtns [i].GetComponent<Collider> ().enabled = true;
				}
			}


		} else {
			for (int i = 0; i < ChildBtns.Length; i++) {
				ChildBtns [i].GetComponent<Collider> ().enabled = false;
				ChildBtns [i].transform.localPosition = Vector3.Lerp (ChildBtns [i].transform.localPosition, parentBtn.transform.localPosition, TimescaleManager.manager.getRealTimeDeltaTime () * speed);
				if (Vector3.Magnitude (ChildBtns [i].transform.localPosition - parentBtn.transform.localPosition)<childBtnPosDelta[i]) {
					parentBtn.gameObject.SetActive (true);
					ChildBtns [i].SetActive (false);
				}
			}
		}

		
	}
	public override void raycastEnter (){
		RayCastHit = true;
	}

	public override void raycastExit (){
		RayCastHit = false;
	}
	public void setActivate(bool val){
		isActivated = val;
	}
}
