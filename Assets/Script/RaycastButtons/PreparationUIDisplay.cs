﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreparationUIDisplay : MonoBehaviour {
    public CanvasGroup cGroup;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameController.controller.gameState == GameState.Preparation)
        {
            cGroup.interactable = true;
            cGroup.blocksRaycasts = true;
            cGroup.alpha = Mathf.Lerp(cGroup.alpha, 1f, TimescaleManager.manager.getRealTimeDeltaTime() * 1.5f);
        } else
        {
            cGroup.interactable = false;
            cGroup.blocksRaycasts = false;
            cGroup.alpha = Mathf.Lerp(cGroup.alpha, 0f, TimescaleManager.manager.getRealTimeDeltaTime() * 1.5f);
        }
	}
}
