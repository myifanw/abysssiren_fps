﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VolumeDisplay : MonoBehaviour {
	public Image img;
	public AudioCategoryEnum audioType;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (audioType == AudioCategoryEnum.BackgroundMusic) {
			img.fillAmount = settingVariables.getVolume_BGM();
		} else if (audioType == AudioCategoryEnum.SoundEffect){
			img.fillAmount = settingVariables.getVolume_SFX();
		}
	}
}