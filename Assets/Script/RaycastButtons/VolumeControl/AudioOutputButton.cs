﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AudioOutputButton : RaycastButtonParent {
	public bool up;
	public AudioCategoryEnum audioType;
	public float speed;
	//public static int loadOutSelected = 0;
	private bool hit;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (hit) {
			setVisual_Select ();

			if (audioType == AudioCategoryEnum.BackgroundMusic) {
				if (up) {
					settingVariables.setVolume_BGM (settingVariables.getVolume_BGM () + TimescaleManager.manager.getRealTimeDeltaTime() * speed);
				} else {
					settingVariables.setVolume_BGM (settingVariables.getVolume_BGM () - TimescaleManager.manager.getRealTimeDeltaTime() * speed);
				}
			} else if (audioType == AudioCategoryEnum.SoundEffect) {
				if (up) {
					settingVariables.setVolume_SFX (settingVariables.getVolume_SFX () + TimescaleManager.manager.getRealTimeDeltaTime() * speed);
				} else {
					settingVariables.setVolume_SFX (settingVariables.getVolume_SFX () - TimescaleManager.manager.getRealTimeDeltaTime() * speed);
				}
			}
		} else {
			setVisual_Deselect ();
		}
	}

	public override void raycastEnter (){
		hit = true;
	}

	public override void raycastExit (){
		hit = false;
	}

	public void setVisual_Select(){
		GetComponentInChildren<Image> ().color = Color.black;
		GetComponentInChildren<Text> ().color = Color.white;

	}
	public void setVisual_Deselect(){
		GetComponentInChildren<Image> ().color = Color.white;
		GetComponentInChildren<Text> ().color = Color.black;
	}

}
