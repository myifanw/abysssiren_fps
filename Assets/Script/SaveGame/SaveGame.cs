﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class SaveGame : MonoBehaviour {
	public static void Save() {
		saveGameData sgd = new saveGameData();

		sgd = settingVariables.SaveData (sgd);
		sgd = ProjectileLoadOutList.SaveData (sgd);


		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd");
		bf.Serialize(file, sgd);
		file.Close();
	}
		
	public static void Load() {
		if(checkIfSaveExist()) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
			saveGameData sgd = (saveGameData)bf.Deserialize(file);

			settingVariables.LoadData(sgd);		
			ProjectileLoadOutList.LoadData (sgd);


			file.Close();
		}
	}

	public static bool checkIfSaveExist(){
		return File.Exists (Application.persistentDataPath + "/savedGames.gd");
	}
}
