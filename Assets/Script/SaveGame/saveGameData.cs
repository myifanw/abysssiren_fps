﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class saveGameData { 

	public bool showFPS;

	public bool tutorialFinished;

	public float Volume_Sensitivity = 8f;

	public float shooter_minShootThreshold = 0.1f;
	public float shooter_powerShotThreshold = 0.8f;

	public float audio_BGM_Volume = 1f;
	public float audio_SFX_Volume = 1f;

	public List<LoadOut> LoadOutList;
}