﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyCharacter_Test : Character {

	//public Image dmgindicator;

	// Use this for initialization
	void Start () {
		Reset();
	}
	public void Reset(){
		base.Reset ();
		this.gameObject.GetComponent<MeshRenderer> ().enabled = true;
	}

	protected override void processDeath(){
		Debug.LogError ("ENEMY");
		Destroy (this.gameObject);
		this.gameObject.GetComponent<MeshRenderer> ().enabled = false;
	}
}
