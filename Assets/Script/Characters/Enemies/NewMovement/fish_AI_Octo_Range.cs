﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fish_AI_Octo_Range : fish_AI_Base {
    //public AudioClip warningSound;
    //public  IK_CircleMover circleMover;


    //Octo variables: 8 extra heads
    public GameObject[] limbs;
    [System.Serializable]
    public class LimbConnector
    {
        public Vector3 limb_shooting_position;

        public Vector3 limb_shooting_left_down;

        public Vector3 limb_shooting_right_down;
        public moveTowards connector;
        protected Vector3 initial_scale;
        public void setScale(Vector3 scale)
        {
            initial_scale = scale;
        }
        public Vector3 getScale()
        {
            return initial_scale;
        }
    }
    public LimbConnector[] limbConnector;
    public FishSegment[] cloaking_objects;
    public FishSegment[] cloaking_objects2;
    public FishSegment[] cloaking_objects3;
    private float curr_cloak;
    private float curr_set_limb_count;

    //AI specific varaibles
    public int numOfWaitWaypoints = 2;//every two way points, generate a limb back.
	public float minAttackCD = 1f;
	private float attackCDTimer = 1f;
	public float shootChargetime = 1f;

	public float min_WP_Gap = 5f; //min distance between random generated waypoints

	public float randomWP_minRadius = 10f;
	public float randomWP_maxRadius = 25f;
	public float randomWP_narrowAngle = 60f;
	public float randomWP_narrowAngleVertical = 60f;

	//state change event
	public delegate void StateChangeAction(Enum_fishAIState NewState);
	public static event StateChangeAction OnStateChange;

	public float bulletDamage = 10f;
	public int bulletPoolAmt = 5;
	public GameObject bulletPrefab;

	//heavy stagger
	public float heavyStagger_repulse_force = 50f;
	public float heavyStagger_repulse_torque = 50f;
	public float heavyStagger_stun = 1.5f;

	//rage mode (when it's weakened, allow for more aggressive AI. They will continuously attack. Makes final enemies more challenging. Lowers stun values)
	[Range(0,1f)]
	public float rage_percentage = 0f; //at this percent of health, start raging.
	public bool rage = false;

	//PRIVATE VARS
	public bool WaypointSetYet = false;
	public bool AttackDelayTimerSetYet = false;
	private float timer_time;

	private Queue<fish_AI_projectile> bulletQueue;

	public bool FinalWaypointReached = false;
    public float attack_cd;
    private float curr_attack_cd;
    private bool cloaked = false;
    private enum octo_state
    {
        circle_shoot, //shoots with all limbs in a circle
        left_down_shoot, //shoots with left side down
        right_down_shoot, //shoots with right side down
        melee, //the limbs hit you
        mix, //mix of shooting and melee
        begin_cloak,
        cloaking, //hiding, animating. 
        cloaked, //hidden, regenerating limbs
    }

    private octo_state curr_octo_state = octo_state.circle_shoot;
	// Use this for initialization
	void Start () {
		reset ();
        if (gameObject.GetComponentInChildren<WaypointSystem>())
        {
            //wp_fart_enabled = false;
            WaypointSystem ways = gameObject.GetComponentInChildren<WaypointSystem>();
            //override current waypoints with external, pretty waypoints
            head.GetComponent<wayPointMoverPhy>().waypoints = new Vector3[ways.waypoints.Count];
            int counter = 0;
            foreach (Transform way in ways.waypoints)
            {
                head.GetComponent<wayPointMoverPhy>().waypoints[counter] = way.position;
                counter++;
                //Debug.Log(way.position.ToString() + "waypoints"); 
            }
            //waypointMover.OnWaypointReached += shootProjectile;
        }

        curr_set_limb_count = 8;
        //sets up scales for each connector
        foreach(LimbConnector conn in limbConnector)
        {
            conn.setScale(conn.connector.transform.localScale);
        }
    }

	// Update is called once per frame
	protected override void FixedUpdate () {
        int limbcount = 8;
        int i = limbs.Length - 1;
        while (i >= 0)
        {
            if (limbs[i] == null || limbs[i].Equals(null))
                limbcount--;
            i--;
        }
        if (limbcount <= curr_set_limb_count - 1 && !cloaked)
        {
            curr_octo_state = octo_state.begin_cloak;
            cloaked = true;
        }
		switch (AIState) {
		case Enum_fishAIState.Wait:
               switch(curr_octo_state)
                {
                    case octo_state.begin_cloak:
                        //begin cloaking
                        curr_octo_state = octo_state.cloaking;
                        AIState = Enum_fishAIState.Wait;

                        //delete limb count
                        curr_set_limb_count = 0;
                        //release remaining limbs to attack
                        foreach (GameObject limb in limbs)
                        {
                            if (limb)
                            {
                                limb.GetComponent<octo_AI_limb>().limb_AI_state = octo_AI_limb.enum_limb_attack_state.melee;
                                Debug.Log("limb released");
                            }
                        }
                        foreach (FishSegment obj in cloaking_objects)
                        {
                            obj.rend.material = obj.transparent_mat;
                        }
                        foreach (FishSegment obj in cloaking_objects2)
                        {
                            obj.rend.material = obj.transparent_mat;
                        }
                        foreach (FishSegment obj in cloaking_objects3)
                        {
                            obj.rend.material = obj.transparent_mat;
                        }
                        break;
                    case octo_state.cloaking:
                        //lerp the shaders to nothing
                        foreach (FishSegment obj in cloaking_objects)
                        {
                            Renderer rend = obj.rend;
                            MaterialPropertyBlock _propBlock = new MaterialPropertyBlock();
                            _propBlock.SetFloat("_Alpha", Mathf.Lerp(1, 0.3f, curr_cloak/2));
                                 rend.SetPropertyBlock(_propBlock);
                        }
                        foreach (FishSegment obj in cloaking_objects2)
                        {
                            Renderer rend = obj.rend;
                            MaterialPropertyBlock _propBlock = new MaterialPropertyBlock();
                            _propBlock.SetFloat("_Alpha", Mathf.Lerp(1, 0, curr_cloak / 1.6f));
                            rend.SetPropertyBlock(_propBlock);
                        }
                        foreach (FishSegment obj in cloaking_objects3)
                        {
                            Renderer rend = obj.rend;
                            MaterialPropertyBlock _propBlock = new MaterialPropertyBlock();
                            _propBlock.SetFloat("_Alpha", Mathf.Lerp(1, 0, curr_cloak / 1.25f));
                            rend.SetPropertyBlock(_propBlock);
                        }

                        foreach(LimbConnector connector in limbConnector)
                        {
                            Vector3 curr_scale = Vector3.Lerp(connector.getScale(), new Vector3(.001f,.001f,.001f), curr_cloak);
                            connector.connector.transform.localScale = curr_scale;
                        }

                        if(curr_cloak >= 3f)
                        {
                            //next, the cloaking AI
                            waypointMover.enabled = true;
                            curr_octo_state = octo_state.cloaked;
                            Debug.Log("transition to cloaked");
                            //temporarily disable the connectors
                            foreach (LimbConnector connector in limbConnector)
                            {
                                connector.connector.attracting = false;
                            }

                        }

                        curr_cloak += Time.fixedDeltaTime;
                        
                        break;
                    case octo_state.cloaked:
                        //in this state, swim around
                        if (!WaypointSetYet)
                        {
                            waypointMover.enabled = true;
                            waypointMover.attacking = false;
                            if (!knows_the_way)
                            {
                                attacking_soon = false;
                                //the limb goes to the position predefined for it.
                                waypointMover.resetWayPoints(generateRandomWaitWaypoint());
                            }
                            else
                            {
                                knows_the_way = false;
                            }
                            WaypointSetYet = true;
                        }

                        else if (waypointMover.checkIfFinalWaypointReached())
                        {
                            //reset the waypoints, add a limb.
                            curr_set_limb_count += 1;
                            //to-do: do an animation when this happens so that the player might know where this is.
                            WaypointSetYet = false;
                        }
                            break;
                    case octo_state.circle_shoot:
                        curr_octo_state = octo_state.circle_shoot;
                        setState(Enum_fishAIState.Attack);
                        break;

                    case octo_state.left_down_shoot:
                        curr_octo_state = octo_state.left_down_shoot;
                        setState(Enum_fishAIState.Attack);
                        break;

                    case octo_state.right_down_shoot:
                        curr_octo_state = octo_state.right_down_shoot;
                        setState(Enum_fishAIState.Attack);
                        break;
                }
                break;
		case Enum_fishAIState.Attack:
			if (attackCDTimer > 0f) {
				setState(Enum_fishAIState.Wait);
			}
			else if (!AttackDelayTimerSetYet) {
               //attack with limbs.
               switch(curr_octo_state)
                {
                    case octo_state.circle_shoot:
                        foreach (GameObject limb in limbs)
                        {
                            if (limb != null)
                                limb.GetComponent<octo_AI_limb>().limb_AI_state = octo_AI_limb.enum_limb_attack_state.shoot;
                        }
                        break;

                    case octo_state.left_down_shoot:
                        foreach (GameObject limb in limbs)
                        {
                            if(limb != null)
                                limb.GetComponent<octo_AI_limb>().limb_AI_state = octo_AI_limb.enum_limb_attack_state.left_down_shoot;
                        }
                        break;


                    case octo_state.right_down_shoot:
                        foreach (GameObject limb in limbs)
                        {
                            if (limb != null)
                                limb.GetComponent<octo_AI_limb>().limb_AI_state = octo_AI_limb.enum_limb_attack_state.right_down_shoot;
                        }
                        break;

                    case octo_state.melee:
                        foreach (GameObject limb in limbs)
                        {
                            if (limb != null)
                                limb.GetComponent<octo_AI_limb>().limb_AI_state = octo_AI_limb.enum_limb_attack_state.melee;
                        }
                        break;
                }
               

            }
            setState(Enum_fishAIState.Wait);
            break;
        
		default:
			Debug.LogError ("STATE NOT RECOGNIZED!!!!! - " + AIState);
			break;
		}
		attackCDTimer = attackCDTimer - Time.fixedDeltaTime;
        //Debug.Log(attackCDTimer + " " + AttackDelayTimerSetYet);
	}
	public void reset()
	{
		waypointMover = head.GetComponent<wayPointMoverPhy> ();
		setState (Enum_fishAIState.Wait);
		waypointMover.OnFinalWaypointReached += NotifyFinalWaypointReached;
		Greyman.OffScreenIndicator.offscreenIndic.AddIndicator (head.transform, 1);
		//lock on if enemy spawn inside lock on zone
		playerLockOnTriggerZone plotz = playerLockOnTriggerZone.lockOnZone;
		if (Vector3.Angle(head.transform.position, plotz.transform.forward)< Vector3.Angle((plotz.lockOnZoneDistance*plotz.transform.forward + plotz.lockOnZoneRadius*plotz.transform.up),plotz.transform.forward)) {
			playerLockOnTriggerZone.lockOnZone.checkIfCanAdd (head.transform);
		}
		timer_time = Time.time;

		//create bullet pool
		bulletQueue = new Queue<fish_AI_projectile>();
		GameObject bulletHolder = new GameObject("bulletPool");
		bulletHolder.transform.SetParent (transform);
		for (int i = 0; i < bulletPoolAmt; i++)
		{
			GameObject instantiatedProjectile = Instantiate(bulletPrefab, new Vector3(999,999,9999), new Quaternion(0,0,0,0)) as GameObject;
			//instantiatedProjectile.GetComponent<Collider>().enabled = false;
			instantiatedProjectile.transform.SetParent (bulletHolder.transform);
			instantiatedProjectile.SetActive (false);
			bulletQueue.Enqueue(instantiatedProjectile.GetComponent<fish_AI_projectile>());
		}
		attackCDTimer = 0f;
        //GameObject tee = Instantiate(bulletPrefab, new Vector3(0,0,0), new Quaternion(0,0,0,0)) as GameObject;

	}


	private void setState(Enum_fishAIState NewState){
		AIState = NewState;

		WaypointSetYet = false;
		AttackDelayTimerSetYet = false;
		FinalWaypointReached = false;

		if(OnStateChange != null)
			OnStateChange(AIState);
	}


	private Vector3[] generateRandomWaitWaypoint(){
		Vector3[] result = new Vector3[numOfWaitWaypoints];
		for (int i = 0; i < numOfWaitWaypoints; i++) {
			Vector3 tempV3 = Vector3.zero;
			while (true) {//break when sure that the point is in the narrow angle
				tempV3 = Random.insideUnitSphere * (randomWP_maxRadius - randomWP_minRadius);
				tempV3 = Vector3.Normalize (tempV3) * randomWP_minRadius  + tempV3;

				//Check min gap between each waypoint, make sure far enough apart
				if (i > 0) {
					if (Vector3.Magnitude (tempV3 - result [i - 1]) < min_WP_Gap) {
						tempV3 = result [i - 1] + ((tempV3 - result [i - 1]) * (min_WP_Gap));
					}
				}
				//make sure not negative z
				if (tempV3.z < 0) {
					tempV3 = new Vector3 (tempV3.x, tempV3.y, -1 * tempV3.z);
				}
				//make sure its in narrow angle
				if (Vector3.Angle (new Vector3(tempV3.x,0,tempV3.z), Vector3.forward) <= randomWP_narrowAngle && Vector3.Angle(new Vector3(0, tempV3.y, tempV3.z), Vector3.forward) <= randomWP_narrowAngleVertical) {
					break;
				}
			}
			result [i] = tempV3;
		}
		return result;
	}


	public void NotifyFinalWaypointReached(){
		FinalWaypointReached = true;
	}

	protected override void processDeath(){
		//issue total break!
		head.GetComponent<FishSegment> ().jointBreak ();
		head.GetComponent<wayPointMoverPhy>().enabled = false;
		Greyman.OffScreenIndicator.offscreenIndic.RemoveIndicator (head.transform);
	}


	public override void applyHeavyStagger(Vector3 contactPosition){
		//applyHeavyStagger stun
		applyStun(heavyStagger_stun, .5f);
		head.AddForce ((head.position - contactPosition).normalized * heavyStagger_repulse_force, ForceMode.Impulse);
		head.AddTorque (Random.insideUnitSphere * heavyStagger_repulse_torque, ForceMode.Impulse);
		//if attacking, cancel the attack
		if (AIState.Equals (Enum_fishAIState.Attack)) {
			//change state
			setState(Enum_fishAIState.Wait);
		}
		else
		{
			//whenever something breaks, reset AI basically
			waypointMover.resetWayPoints(generateRandomWaitWaypoint());
		}
	}

	//register collision with player
	public override void collisionWithPlayer (){
//		if (AIState.Equals (Enum_fishAIState.Attack)) {
//			//deal damage
//			playerObject.GetComponent<PlayerCharacter>().deltaHealth(-1*sucessful_attack_damage);
//			//change state
//			setState(Enum_fishAIState.Wait);
//			//apply stun and bounce
//			applyStun(sucessful_attack_stun);
//			head.AddForce ((head.position - playerObject.transform.position).normalized * sucessful_attack_repulse_force, ForceMode.Impulse);
//			head.AddTorque (Random.insideUnitSphere * sucessful_attack_repulse_torque, ForceMode.Impulse);
//		}
	}


	public void setTimer(float time){
		timer_time = Time.time + time;
	}
	public bool isTimerDone(){
		return timer_time < Time.time;
	}
}
