﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class octo_AI_limb : fish_AI_Base {
	//public AudioClip warningSound;
    public AudioClip attack_sound;
    public AudioClip incoming_sound;
    public AudioClip death_sound;
    public WhiteSoundwaveRipple attack_warning_circle;
    public float pitch;
	public float fartAccelerationSpeed = 200f;
	//AI STATE IN PARENT CLASS
    
	//AI specific varaibles
	public int numOfWaitWaypoints = 3;
	public float min_WP_Gap = 5f; //min distance between random generated waypoints

	public float randomWP_minRadius = 10f;
	public float randomWP_maxRadius = 25f;
	public float randomWP_narrowAngle = 60f;
    public float randomWP_narrowAngleVertical = 60f;

    //state change event
    public delegate void StateChangeAction(Enum_fishAIState NewState);
	public static event StateChangeAction OnStateChange;

	public float sucessful_attack_damage = 10f;
	public float sucessful_attack_repulse_force = 100f;
	public float sucessful_attack_repulse_torque = 20f;
	public float sucessful_attack_stun = 1.3f;

	//heavy stagger
	public float heavyStagger_repulse_force = 50f;
	public float heavyStagger_repulse_torque = 50f;
	public float heavyStagger_stun = 1.5f;

    //rage mode (when it's weakened, allow for more aggressive AI. They will continuously attack. Makes final enemies more challenging. Lowers stun values)
    [Range(0,1f)]
    public float rage_percentage = 0f; //at this percent of health, start raging.
    public bool rage = false;
    public bool wp_fart_enabled = false; //so this is to disable farts on pathed fishes. Should reset after getting hit.

    // initial positions, for posing and shooting
    public Vector3 limb_position;

    public Vector3 limb_pos_left_down;
    public Vector3 limb_pos_right_down;
    //PRIVATE VARS
    public bool WaypointSetYet = false;
	public bool FinalWaypointReached = false;


    public enum enum_limb_attack_state
    {
        wait,
        shoot, //shoots with all limbs in a circle
        left_down_shoot,
        right_down_shoot,
        melee, //the limbs hit you
    }
    public enum_limb_attack_state limb_AI_state;
    public enum_limb_attack_state prev_limb_AI_state;
    public float ranged_charged_timer;
    private float curr_charge_timer;
    public Transform eye;
    public GameObject charge_aura;
    public GameObject beam;
    public Color aura_initial = new Color(30/255f, 50 / 255f, 160 / 255f);
    private Color aura_white = new Color(1f,1f,1f);
    private float aura_init_size = 4;
    private float aura_final_size = 25;

    private Color aura_break_color_init;
    private float aura_break_size_init;
    private float aura_break_timer;
    private bool aura_fade = false;
    private bool dead = false;
    private bool charge_break = false;

    // Use this for initialization
    void Start () {
		base.Start ();

        aura_break_color_init = aura_initial;
        //randomize pitch when nobody fills pitch out
        if(pitch == 0)
        {
            pitch = 1 + Random.Range(-0.1f, 0.1f);
        }
        reset ();
        if (gameObject.GetComponentInChildren<WaypointSystem>())
        {
            //wp_fart_enabled = false;
            WaypointSystem ways = gameObject.GetComponentInChildren<WaypointSystem>();
            //override current waypoints with external, pretty waypoints
            head.GetComponent<wayPointMoverPhy>().waypoints = new Vector3[ways.waypoints.Count];
            int counter = 0;
            foreach (Transform way in ways.waypoints)
            {
                head.GetComponent<wayPointMoverPhy>().waypoints[counter] = way.position;
                counter++;
            }
        }
        if(!playerObject)
        {
            playerObject = GameObject.Find("Player");
        }
    }
	
	// Update is called once per frame
	protected override void FixedUpdate () {
        //we're doing this because frankly the player feels super cool when they stun an enemy at close range. Stun is also changed to be over a threshold to make it more rewarding for well placed/grouped shots.

        base.FixedUpdate();
        //activating rage
        if(!rage && currHealth <= rage_percentage * maxHealth)
        {
            //Debug.Log("RAGE! " + name);
            rage = true;
            //stuff that makes rage fun for melee, make the enemy stay closer, and only have one waypoint. testing speed increase lul
            randomWP_minRadius /= 1.5f;
            randomWP_maxRadius /= 1.5f;
            numOfWaitWaypoints = 2;
            waypointMover.stun_threshold *= 1.5f; //maybe too hard lol
            waypointMover.acceleration *= 1.2f;
            waypointMover.acceleration_att *= 1.2f;
            waypointMover.maxSpeed *= 1.2f;
            waypointMover.maxSpeed_att *= 1.2f;
        }

		if (willEmergencyRecover) {
			if (emerRecov_DmgCounter >= emerRecov_DmgAmt) {
				emerRecov_DmgCounter = 0f;
				setState (Enum_fishAIState.Wait);
				emergencyRecoverAcceleration (head.transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f));
			}
		}
        switch (limb_AI_state)
        {
            case enum_limb_attack_state.wait:
                AIState = Enum_fishAIState.Wait;
            break;
            case enum_limb_attack_state.melee:
                if (prev_limb_AI_state != limb_AI_state)
                {
                    waypointMover.reset();
                    AIState = Enum_fishAIState.Wait;
                }
                meleeAI();
            break;

            case enum_limb_attack_state.shoot:
                if (prev_limb_AI_state != limb_AI_state)
                {
                    waypointMover.reset();
                    AIState = Enum_fishAIState.Wait;
                }
                rangeAI(limb_position);
                //Debug.Log("shoots");
            break;

            case enum_limb_attack_state.left_down_shoot:
                if (prev_limb_AI_state != limb_AI_state)
                {
                    waypointMover.reset();
                    AIState = Enum_fishAIState.Wait;
                }
                rangeAI(limb_pos_left_down);
                //Debug.Log("shoots");
                break;

            case enum_limb_attack_state.right_down_shoot:
                if (prev_limb_AI_state != limb_AI_state)
                {
                    waypointMover.reset();
                    AIState = Enum_fishAIState.Wait;
                }
                rangeAI(limb_pos_right_down);
                //Debug.Log("shoots");
                break;
        }
        prev_limb_AI_state = limb_AI_state;

        if(charge_aura)
        {
            charge_aura.transform.position = eye.position + eye.forward * 2;
        }

        if(currHealth <= 0)
        {
            waypointMover.enabled = false;
        }
        if(currHealth <= 0 || limb_AI_state == enum_limb_attack_state.melee || limb_AI_state == enum_limb_attack_state.wait)
        {
            if (!aura_fade)
            {
                aura_break_timer = 0;
                aura_break_size_init = charge_aura.transform.localScale.x;
                beam.SetActive(false);
                aura_fade = true;
            }
            float aura_size = Mathf.Lerp(aura_break_size_init, .02f, aura_break_timer);
            charge_aura.transform.localScale = new Vector3(aura_size, aura_size, aura_size);
            ParticleSystem ps = charge_aura.GetComponent<ParticleSystem>();
            var main = ps.main;
            main.startColor = Color.Lerp(aura_break_color_init, Color.black, aura_break_timer);
            aura_break_timer += Time.fixedDeltaTime;
        }
	}
    private void meleeAI()
    {
        switch (AIState)
        {
            case Enum_fishAIState.Wait:
                waypointMover.enabled = true;
                if (!WaypointSetYet)
                {
                    waypointMover.attacking = false;
                    if (!knows_the_way)
                    {
                        attacking_soon = false;
                        waypointMover.resetWayPoints(generateRandomWaitWaypoint());
                        //reenable farts whenever anything happens
                        wp_fart_enabled = true;
                    }
                    else
                    {
                        knows_the_way = false;
                    }
                    WaypointSetYet = true;
                }
                else if (waypointMover.checkIfFinalWaypointReached())
                {
                    wp_fart_enabled = true;
                    setState(Enum_fishAIState.Attack);
                    //Attack sound
                    if (incoming_sound)
                        AudioManager.manager.playSFXOneShot(incoming_sound, 0, incoming_sound.length, pitch);
                    if (attack_warning_circle)
                    {
                        attack_warning_circle.Reset();
                        attack_warning_circle.transform.position = this.head.transform.position;
                        attack_warning_circle.transform.LookAt(playerObject.transform);
                    }
                }

                else if (!attacking_soon && waypointMover.curWaypointIdx >= waypointMover.waypoints.Length - 2)
                {
                    attacking_soon = true;
                }
                break;
            case Enum_fishAIState.Attack:
                if (!WaypointSetYet)
                {
                    waypointMover.resetWayPoint(playerObject.transform);
                    waypointMover.attacking = true;
                    WaypointSetYet = true;
                }
                if (waypointMover.stunned)
                {
                    setState(Enum_fishAIState.Wait);
                }
                break;

            default:
                Debug.LogError("STATE NOT RECOGNIZED!!!!! - " + AIState);
                break;
        }
    }

    private void rangeAI(Vector3 limb_pos)
    {
        
        switch (AIState)
        {
            case Enum_fishAIState.Wait:
                if (!WaypointSetYet)
                {
                    waypointMover.enabled = true;
                    waypointMover.attacking = false;
                    if (!knows_the_way)
                    {
                        attacking_soon = false;
                        //the limb goes to the position predefined for it.
                        waypointMover.resetWayPoint(this.transform.parent.transform.position+limb_pos * transform.parent.transform.localScale.x); //scaled to octoboss's size.
                        //reenable farts whenever anything happens
                        wp_fart_enabled = true;
                    }
                    else
                    {
                        knows_the_way = false;
                    }
                    WaypointSetYet = true;
                }
                else if (waypointMover.checkIfFinalWaypointReached())
                {
                    wp_fart_enabled = true;
                    setState(Enum_fishAIState.Attack);
                    if (charge_aura)
                    {
                        charge_aura.SetActive(true);
                        charge_aura.transform.position = eye.position;
                        charge_aura.transform.localScale = new Vector3(aura_init_size, aura_init_size, aura_init_size);
                        ParticleSystem ps = charge_aura.GetComponent<ParticleSystem>();
                        var main = ps.main;
                        //main.startColor = aura_initial;
                    }
                    curr_charge_timer = ranged_charged_timer;
                }
                break;
            case Enum_fishAIState.Attack:
                if (waypointMover.stunned)
                {
                    aura_break_timer += Time.fixedDeltaTime;
                    aura_break_size_init = charge_aura.transform.localScale.x;
                    limb_AI_state = enum_limb_attack_state.wait;
                    beam.SetActive(false);
                    setState(Enum_fishAIState.Wait);
                }
                //begin charging by changing colors
                if(charge_aura)
                {
                    aura_fade = false;
                    waypointMover.enabled = false;
                    charge_aura.transform.position = eye.position + eye.forward * 2;
                    float aura_size = Mathf.Lerp(aura_init_size, aura_final_size,  curr_charge_timer/ranged_charged_timer);
                    charge_aura.transform.localScale = new Vector3(aura_size, aura_size, aura_size);
                    ParticleSystem ps = charge_aura.GetComponent<ParticleSystem>();
                    var main = ps.main;
                    main.startColor = Color.Lerp(aura_white, aura_initial,  curr_charge_timer/ranged_charged_timer);
                }
                if(curr_charge_timer <= 0)
                {
                    //shoot

                    setState(Enum_fishAIState.Wait);
                    charge_aura.SetActive(false);
                    if (beam)
                    {
                        beam.SetActive(true);
                        beam.transform.position = eye.position;
                        beam.transform.rotation = Quaternion.LookRotation((playerObject.transform.position + new Vector3(Random.value * 2 - 1, Random.value - 1)) - beam.transform.position);
                    }
                }
                curr_charge_timer -= Time.deltaTime;
                break;

            default:
                Debug.LogError("STATE NOT RECOGNIZED!!!!! - " + AIState);
                break;
        }
    }

    //public void shootProjectile()
    //{
    //    if (attackCDTimer > 0f)
    //    {
    //        return;
    //    }
    //    attackCDTimer = minAttackCD;
    //    //swim in circle for the charge time then spawn the bullet
    //    AttackDelayTimerSetYet = true;
    //    waypointMover.attacking = true;


    //    //waypointMover
    //    //waypointMover.enabled = false;

    //    //spawn projectile
    //    fish_AI_projectile projectile_ai = bulletQueue.Dequeue();
    //    projectile_ai.playerObject = playerObject;
    //    projectile_ai.transform.position = head.transform.position;
    //    projectile_ai.transform.rotation = head.transform.rotation;
    //    projectile_ai.transform.Rotate(new Vector3(0f, Random.Range(150f, 210f), 0f));
    //    projectile_ai.reset(shootChargetime);
    //    projectile_ai.head.GetComponent<Rigidbody>().AddForce((projectile_ai.transform.forward).normalized * 15f, ForceMode.VelocityChange);
    //    bulletQueue.Enqueue(projectile_ai);
    //    head.AddForce(head.transform.forward.normalized * 30f, ForceMode.VelocityChange);
    //    waypointMover.activateFartDashSpeedCap();
    //}
    public void reset()
	{
		waypointMover = head.GetComponent<wayPointMoverPhy> ();

        //squid limbs aren't active by default
        waypointMover.enabled = false;
        setState (Enum_fishAIState.Wait);
        limb_AI_state = enum_limb_attack_state.wait;
		waypointMover.OnFinalWaypointReached += NotifyFinalWaypointReached;
		Greyman.OffScreenIndicator.offscreenIndic.AddIndicator (head.transform, 1);
        //lock on if enemy spawn inside lock on zone
        
        playerLockOnTriggerZone plotz = playerLockOnTriggerZone.lockOnZone;
        /*
		if (Vector3.Angle(head.transform.position, plotz.transform.forward)< Vector3.Angle((plotz.lockOnZoneDistance*plotz.transform.forward + plotz.lockOnZoneRadius*plotz.transform.up),plotz.transform.forward)) {
			playerLockOnTriggerZone.lockOnZone.checkIfCanAdd (head.transform);
		}
        */
		head.GetComponent<wayPointMoverPhy> ().OnNextWaypointChange += fartAcceleration;

        wp_fart_enabled = true;
	}


	private void setState(Enum_fishAIState NewState){
		AIState = NewState;

		WaypointSetYet = false;
		FinalWaypointReached = false;

		if(OnStateChange != null)
			OnStateChange(AIState);
	}


    private Vector3[] generateRandomWaitWaypoint()
    {
        return generateRandomWaitWaypoint(numOfWaitWaypoints);
    }

    private Vector3[] generateRandomWaitWaypoint(int waypoints){
		Vector3[] result = new Vector3[waypoints];
        float range = (randomWP_maxRadius - randomWP_minRadius);
        float section = range / numOfWaitWaypoints;
        for (int i = 0; i < numOfWaitWaypoints; i++) {
			Vector3 tempV3 = Vector3.zero;
			while (true) {//break when sure that the point is in the narrow angle
				tempV3 = Random.insideUnitSphere * section;
				tempV3 = Vector3.Normalize (tempV3) * (randomWP_maxRadius - (section * (i)))  + tempV3;

				//Check min gap between each waypoint, make sure far enough apart
				if (i > 0) {
					if (Vector3.Magnitude (tempV3 - result [i - 1]) < min_WP_Gap) {
						tempV3 = result [i - 1] + ((tempV3 - result [i - 1]) * (min_WP_Gap));
					}
				}
				//make sure not negative z
				if (tempV3.z < 0) {
					tempV3 = new Vector3 (tempV3.x, tempV3.y, -1 * tempV3.z);
				}
				//make sure its in narrow angle
				if (Vector3.Angle (new Vector3(tempV3.x,0,tempV3.z), Vector3.forward) <= randomWP_narrowAngle && Vector3.Angle(new Vector3(0, tempV3.y, tempV3.z), Vector3.forward) <= randomWP_narrowAngleVertical) {
					break;
				}
			}
//Debug.Log(this.name + " " + tempV3);
			result [i] = tempV3;
		}
		return result;
	}

    public override void applyStun(float stunDuration, float max_stun_scale)
    {
        if (rage)
        {
            stunDuration = stunDuration / 2;
        }
        base.applyStun(stunDuration, max_stun_scale);
    }

    public void NotifyFinalWaypointReached(){
		FinalWaypointReached = true;
	}

	protected override void processDeath(){
		//issue total break!
		head.GetComponent<FishSegment> ().jointBreak ();
        head.GetComponent<wayPointMoverPhy>().enabled = false;
		Greyman.OffScreenIndicator.offscreenIndic.RemoveIndicator (head.transform);
       
        //add hit sound
        if (death_sound)
            AudioManager.manager.playSFXOneShot(death_sound, 0, death_sound.length, pitch);

    }


	public override void applyHeavyStagger(Vector3 contactPosition){
		//applyHeavyStagger stun
		applyStun(sucessful_attack_stun, 0.5f);
		head.AddForce ((head.position - contactPosition).normalized * heavyStagger_repulse_force, ForceMode.Impulse);
		head.AddTorque (Random.insideUnitSphere * heavyStagger_repulse_torque, ForceMode.Impulse);
		//if attacking, cancel the attack
		if (AIState.Equals (Enum_fishAIState.Attack)) {
			//change state
			setState(Enum_fishAIState.Wait);
		}
        else
        {
            //whenever something breaks, reset AI basically
            //waypointMover.resetWayPoints(generateRandomWaitWaypoint());
        }
	}

	//register collision with player
	public override void collisionWithPlayer (){
		if (AIState.Equals (Enum_fishAIState.Attack)) {
            //add hit sound
            if (attack_sound)
                AudioManager.manager.playSFXOneShot(attack_sound, 0, attack_sound.length, pitch);
            //deal damage
            playerObject.GetComponent<PlayerCharacter>().deltaHealth(-1*sucessful_attack_damage);
			//change state
			setState(Enum_fishAIState.Wait);
			//apply stun and bounce
			applyStun(sucessful_attack_stun, 0.5f);
			head.AddForce ((head.position - playerObject.transform.position).normalized * sucessful_attack_repulse_force, ForceMode.Impulse);
			head.AddTorque (Random.insideUnitSphere * sucessful_attack_repulse_torque, ForceMode.Impulse);
		}
	}


	public void fartAcceleration(Vector3 NewWaypoint){
        if (wp_fart_enabled)
        {
            head.GetComponent<Rigidbody>().AddForce(head.transform.forward.normalized * fartAccelerationSpeed, ForceMode.VelocityChange);
            waypointMover.activateFartDashSpeedCap();
        }
	}

	public void emergencyRecoverAcceleration(Vector3 NewWaypoint){
		Debug.LogError ("HERE");
		head.GetComponent<Rigidbody> ().AddForce ((NewWaypoint - head.transform.position).normalized * fartAccelerationSpeed * 2, ForceMode.VelocityChange);
		waypointMover.activateFartDashSpeedCap ();
	}
}
