﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fish_AI_Melee : fish_AI_Base {
	//public AudioClip warningSound;
    public AudioClip attack_sound;
    public AudioClip incoming_sound;
    public AudioClip death_sound;
    public WhiteSoundwaveRipple attack_warning_circle;
    public float pitch;
	public float fartAccelerationSpeed = 200f;
	//AI STATE IN PARENT CLASS



	//AI specific varaibles
	public int numOfWaitWaypoints = 3;
	public float min_WP_Gap = 5f; //min distance between random generated waypoints

	public float randomWP_minRadius = 10f;
	public float randomWP_maxRadius = 25f;
	public float randomWP_narrowAngle = 60f;
    public float randomWP_narrowAngleVertical = 60f;

    //state change event
    public delegate void StateChangeAction(Enum_fishAIState NewState);
	public static event StateChangeAction OnStateChange;

	public float sucessful_attack_damage = 10f;
	public float sucessful_attack_repulse_force = 100f;
	public float sucessful_attack_repulse_torque = 20f;
	public float sucessful_attack_stun = 1.3f;

	//heavy stagger
	public float heavyStagger_repulse_force = 50f;
	public float heavyStagger_repulse_torque = 50f;
	public float heavyStagger_stun = 1.5f;

    //rage mode (when it's weakened, allow for more aggressive AI. They will continuously attack. Makes final enemies more challenging. Lowers stun values)
    [Range(0,1f)]
    public float rage_percentage = 0f; //at this percent of health, start raging.
    public bool rage = false;
    public bool wp_fart_enabled = false; //so this is to disable farts on pathed fishes. Should reset after getting hit.
    
    //PRIVATE VARS
    public bool WaypointSetYet = false;
	public bool FinalWaypointReached = false;
	// Use this for initialization
	void Start () {
		base.Start ();
        
        //randomize pitch when nobody fills pitch out
        if(pitch == 0)
        {
            pitch = 1 + Random.Range(-0.1f, 0.1f);
        }
        reset ();
        if (gameObject.GetComponentInChildren<WaypointSystem>())
        {
            //wp_fart_enabled = false;
            WaypointSystem ways = gameObject.GetComponentInChildren<WaypointSystem>();
            //override current waypoints with external, pretty waypoints
            head.GetComponent<wayPointMoverPhy>().waypoints = new Vector3[ways.waypoints.Count];
            int counter = 0;
            foreach (Transform way in ways.waypoints)
            {
                head.GetComponent<wayPointMoverPhy>().waypoints[counter] = way.position;
                counter++;
                //Debug.Log(way.position.ToString() + "waypoints"); 
            }
        }
    }
	
	// Update is called once per frame
	protected override void FixedUpdate () {
        //we're doing this because frankly the player feels super cool when they stun an enemy at close range. Stun is also changed to be over a threshold to make it more rewarding for well placed/grouped shots.

        base.FixedUpdate();
        //activating rage
        if(!rage && currHealth <= rage_percentage * maxHealth)
        {
            //Debug.Log("RAGE! " + name);
            rage = true;
            //stuff that makes rage fun for melee, make the enemy stay closer, and only have one waypoint. testing speed increase lul
            randomWP_minRadius /= 1.5f;
            randomWP_maxRadius /= 1.5f;
            numOfWaitWaypoints = 2;
            waypointMover.stun_threshold *= 1.5f; //maybe too hard lol
            waypointMover.acceleration *= 1.2f;
            waypointMover.acceleration_att *= 1.2f;
            waypointMover.maxSpeed *= 1.2f;
            waypointMover.maxSpeed_att *= 1.2f;
        }

		if (willEmergencyRecover) {
			if (emerRecov_DmgCounter >= emerRecov_DmgAmt) {
				emerRecov_DmgCounter = 0f;
				setState (Enum_fishAIState.Wait);
				emergencyRecoverAcceleration (head.transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f));
			}
		}
		switch (AIState) {
		case Enum_fishAIState.Wait:
			if (!WaypointSetYet) {
				waypointMover.attacking = false;
                if (!knows_the_way)
                {
                    attacking_soon = false;
                    waypointMover.resetWayPoints(generateRandomWaitWaypoint());
                        //reenable farts whenever anything happens
                        wp_fart_enabled = true;
                    }
                else
                {
                    knows_the_way = false;
                }
				WaypointSetYet = true;
			} else if (waypointMover.checkIfFinalWaypointReached()) {
                    wp_fart_enabled = true;
                    setState (Enum_fishAIState.Attack);
                //Attack sound
                if(incoming_sound)
                    AudioManager.manager.playSFXOneShot(incoming_sound,0,incoming_sound.length,pitch);
                if(attack_warning_circle)
                {
                        attack_warning_circle.Reset();
                        attack_warning_circle.transform.position = this.head.transform.position;
                        attack_warning_circle.transform.LookAt(playerObject.transform);
                }
            }
            else if (!attacking_soon && waypointMover.curWaypointIdx >= waypointMover.waypoints.Length - 2)
            {
                attacking_soon = true;
            }
			break;
		case Enum_fishAIState.Attack:
			if (!WaypointSetYet) {
				waypointMover.resetWayPoint (playerObject.transform);
				waypointMover.attacking = true;
				WaypointSetYet = true;
			}
            if (waypointMover.stunned)
            {
                setState(Enum_fishAIState.Wait);
            }
                break;
		default:
			Debug.LogError ("STATE NOT RECOGNIZED!!!!! - " + AIState);
			break;
		}
	}
    public void reset()
	{
		waypointMover = head.GetComponent<wayPointMoverPhy> ();
		setState (Enum_fishAIState.Wait);
		waypointMover.OnFinalWaypointReached += NotifyFinalWaypointReached;
		Greyman.OffScreenIndicator.offscreenIndic.AddIndicator (head.transform, 1);
        //lock on if enemy spawn inside lock on zone
        
        playerLockOnTriggerZone plotz = playerLockOnTriggerZone.lockOnZone;
        /*
		if (Vector3.Angle(head.transform.position, plotz.transform.forward)< Vector3.Angle((plotz.lockOnZoneDistance*plotz.transform.forward + plotz.lockOnZoneRadius*plotz.transform.up),plotz.transform.forward)) {
			playerLockOnTriggerZone.lockOnZone.checkIfCanAdd (head.transform);
		}
        */
		head.GetComponent<wayPointMoverPhy> ().OnNextWaypointChange += fartAcceleration;

        wp_fart_enabled = true;
	}


	private void setState(Enum_fishAIState NewState){
		AIState = NewState;

		WaypointSetYet = false;
		FinalWaypointReached = false;

		if(OnStateChange != null)
			OnStateChange(AIState);
	}


	private Vector3[] generateRandomWaitWaypoint(){
		Vector3[] result = new Vector3[numOfWaitWaypoints];

		if (GameController.controller.gameState != GameState.Preparation && GameController.controller.gameState != GameState.Tutorial_Finalize) {
			float range = (randomWP_maxRadius - randomWP_minRadius);
			float section = range / numOfWaitWaypoints;
			for (int i = 0; i < numOfWaitWaypoints; i++) {
				Vector3 tempV3 = Vector3.zero;
				while (true) {//break when sure that the point is in the narrow angle
					tempV3 = Random.insideUnitSphere * section;
					tempV3 = Vector3.Normalize (tempV3) * (randomWP_maxRadius - (section * (i))) + tempV3;

					//Check min gap between each waypoint, make sure far enough apart
					if (i > 0) {
						if (Vector3.Magnitude (tempV3 - result [i - 1]) < min_WP_Gap) {
							tempV3 = result [i - 1] + ((tempV3 - result [i - 1]) * (min_WP_Gap));
						}
					}
					//make sure not negative z
					if (tempV3.z < 0) {
						tempV3 = new Vector3 (tempV3.x, tempV3.y, -1 * tempV3.z);
					}
					//make sure its in narrow angle
					if (Vector3.Angle (new Vector3 (tempV3.x, 0, tempV3.z), Vector3.forward) <= randomWP_narrowAngle && Vector3.Angle (new Vector3 (0, tempV3.y, tempV3.z), Vector3.forward) <= randomWP_narrowAngleVertical) {
						break;
					}
				}
				//Debug.Log(this.name + " " + tempV3);
				result [i] = tempV3;
			}
		} else {
			for (int i = 0; i < numOfWaitWaypoints; i++) {
				Vector3 tempV3 = TestEnemySpawnPoint.spawnPoint.transform.position + Random.insideUnitSphere * 5f;
				result [i] = tempV3;
			}
		}
		return result;
	}

    public override void applyStun(float stunDuration, float max_stun_scale)
    {
        if (rage)
        {
            stunDuration = stunDuration / 2;
        }
        base.applyStun(stunDuration, max_stun_scale);
    }

    public void NotifyFinalWaypointReached(){
		FinalWaypointReached = true;
	}

	protected override void processDeath(){
		//issue total break!
		head.GetComponent<FishSegment> ().jointBreak ();
        head.GetComponent<wayPointMoverPhy>().enabled = false;
		Greyman.OffScreenIndicator.offscreenIndic.RemoveIndicator (head.transform);
       
        //add hit sound
        if (death_sound)
            AudioManager.manager.playSFXOneShot(death_sound, 0, death_sound.length, pitch);

    }


	public override void applyHeavyStagger(Vector3 contactPosition){
		//applyHeavyStagger stun
		applyStun(sucessful_attack_stun, 0.5f);
		head.AddForce ((head.position - contactPosition).normalized * heavyStagger_repulse_force, ForceMode.Impulse);
		head.AddTorque (Random.insideUnitSphere * heavyStagger_repulse_torque, ForceMode.Impulse);
		//if attacking, cancel the attack
		if (AIState.Equals (Enum_fishAIState.Attack)) {
			//change state
			setState(Enum_fishAIState.Wait);
		}
        else
        {
            //whenever something breaks, reset AI basically
            waypointMover.resetWayPoints(generateRandomWaitWaypoint());
        }
	}

	//register collision with player
	public override void collisionWithPlayer (){
		if (AIState.Equals (Enum_fishAIState.Attack)) {
            //add hit sound
            if (attack_sound)
                AudioManager.manager.playSFXOneShot(attack_sound, 0, attack_sound.length, pitch);
            //deal damage
            playerObject.GetComponent<PlayerCharacter>().deltaHealth(-1*sucessful_attack_damage);
			//change state
			setState(Enum_fishAIState.Wait);
			//apply stun and bounce
			applyStun(sucessful_attack_stun, 0.5f);
			head.AddForce ((head.position - playerObject.transform.position).normalized * sucessful_attack_repulse_force, ForceMode.Impulse);
			head.AddTorque (Random.insideUnitSphere * sucessful_attack_repulse_torque, ForceMode.Impulse);
		}
	}


	public void fartAcceleration(Vector3 NewWaypoint){
        if (wp_fart_enabled)
        {
            head.GetComponent<Rigidbody>().AddForce(head.transform.forward.normalized * fartAccelerationSpeed, ForceMode.VelocityChange);
            waypointMover.activateFartDashSpeedCap();
        }
	}

	public void emergencyRecoverAcceleration(Vector3 NewWaypoint){
		head.GetComponent<Rigidbody> ().AddForce ((NewWaypoint - head.transform.position).normalized * fartAccelerationSpeed * 2, ForceMode.VelocityChange);
		waypointMover.activateFartDashSpeedCap ();
	}
}
