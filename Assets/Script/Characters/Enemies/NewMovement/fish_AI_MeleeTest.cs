﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fish_AI_MeleeTest : MonoBehaviour {
	//public AudioClip warningSound;
	public GameObject followObj;
	public wayPointMoverPhy waypointMover;
	public Vector3 offset;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	protected void FixedUpdate () {
		waypointMover.resetWayPoint (followObj.transform.position + followObj.transform.forward * offset.z + followObj.transform.right * offset.x + followObj.transform.up * offset.y);
	}


}
