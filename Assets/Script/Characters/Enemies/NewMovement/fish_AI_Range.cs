﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fish_AI_Range : fish_AI_Base {
	//public AudioClip warningSound;
	//public  IK_CircleMover circleMover;




	//AI specific varaibles
	public int numOfWaitWaypoints = 3;
	public float minAttackCD = 1f;
	private float attackCDTimer = 1f;
	public float shootChargetime = 1f;

	public float min_WP_Gap = 5f; //min distance between random generated waypoints

	public float randomWP_minRadius = 10f;
	public float randomWP_maxRadius = 25f;
	public float randomWP_narrowAngle = 60f;
	public float randomWP_narrowAngleVertical = 60f;

	//state change event
	public delegate void StateChangeAction(Enum_fishAIState NewState);
	public static event StateChangeAction OnStateChange;

	public float bulletDamage = 10f;
	public int bulletPoolAmt = 5;
	public GameObject bulletPrefab;

	//heavy stagger
	public float heavyStagger_repulse_force = 50f;
	public float heavyStagger_repulse_torque = 50f;
	public float heavyStagger_stun = 1.5f;

	//rage mode (when it's weakened, allow for more aggressive AI. They will continuously attack. Makes final enemies more challenging. Lowers stun values)
	[Range(0,1f)]
	public float rage_percentage = 0f; //at this percent of health, start raging.
	public bool rage = false;

	//PRIVATE VARS
	public bool WaypointSetYet = false;
	public bool AttackDelayTimerSetYet = false;
	private float timer_time;

	private Queue<fish_AI_projectile> bulletQueue;

	public bool FinalWaypointReached = false;
	// Use this for initialization
	void Start () {
		reset ();
        if (gameObject.GetComponentInChildren<WaypointSystem>())
        {
            //wp_fart_enabled = false;
            WaypointSystem ways = gameObject.GetComponentInChildren<WaypointSystem>();
            //override current waypoints with external, pretty waypoints
            head.GetComponent<wayPointMoverPhy>().waypoints = new Vector3[ways.waypoints.Count];
            int counter = 0;
            foreach (Transform way in ways.waypoints)
            {
                head.GetComponent<wayPointMoverPhy>().waypoints[counter] = way.position;
                counter++;
                //Debug.Log(way.position.ToString() + "waypoints"); 
            }
            waypointMover.OnWaypointReached += shootProjectile;
        }
    }

	// Update is called once per frame
	protected override void FixedUpdate () {

		//we're doing this because frankly the player feels super cool when they stun an enemy at close range. Stun is also changed to be over a threshold to make it more rewarding for well placed/grouped shots.
		if (waypointMover.attacking)
		{

		}
		base.FixedUpdate();
		//activating rage
		if(!rage && currHealth <= rage_percentage * maxHealth)
		{
			//Debug.Log("RAGE! " + name);
			rage = true;
			//stuff that makes rage fun for melee, make the enemy stay closer, and only have one waypoint. testing speed increase lul
			randomWP_minRadius /= 1.5f;
			randomWP_maxRadius /= 1.5f;
			numOfWaitWaypoints = 2;
			waypointMover.stun_threshold *= 1.5f; //maybe too hard lol
			waypointMover.acceleration *= 1.2f;
			waypointMover.acceleration_att *= 1.2f;
			waypointMover.maxSpeed *= 1.2f;
			waypointMover.maxSpeed_att *= 1.2f;
		}
		switch (AIState) {
		case Enum_fishAIState.Wait:
			if (!WaypointSetYet) {
				waypointMover.enabled = true;
				//circleMover.enabled = false;
				waypointMover.attacking = false;
				if (!knows_the_way)
				{
                        attacking_soon = false;
					waypointMover.resetWayPoints(generateRandomWaitWaypoint());
				}
				else
				{
					knows_the_way = false;
				}
				WaypointSetYet = true;
				waypointMover.attacking = false;
			} else if (waypointMover.checkIfFinalWaypointReached()) {
				setState (Enum_fishAIState.Attack);
			}
            else if (!attacking_soon && waypointMover.curWaypointIdx >= waypointMover.waypoints.Length - 2)
            {
                attacking_soon = true;
            }
			break;
		case Enum_fishAIState.Attack:
			if (attackCDTimer > 0f) {
				setState(Enum_fishAIState.Wait);
			}
			else if (!AttackDelayTimerSetYet) {
                    shootProjectile();
			}

                setState(Enum_fishAIState.Wait);
                break;
		default:
			Debug.LogError ("STATE NOT RECOGNIZED!!!!! - " + AIState);
			break;
		}
		attackCDTimer = attackCDTimer - Time.fixedDeltaTime;
	}
    public void shootProjectile(Vector3 newWaypoint)
    {
        shootProjectile();
    }
    public void shootProjectile()
    {
        if (attackCDTimer > 0f)
        {
            return;
        }
        attackCDTimer = minAttackCD;
        //swim in circle for the charge time then spawn the bullet
        AttackDelayTimerSetYet = true;
        waypointMover.attacking = true;


        //waypointMover
        //waypointMover.enabled = false;

        //spawn projectile
        fish_AI_projectile projectile_ai = bulletQueue.Dequeue();
        projectile_ai.playerObject = playerObject;
        projectile_ai.transform.position = head.transform.position;
        projectile_ai.transform.rotation = head.transform.rotation;
        projectile_ai.transform.Rotate(new Vector3(0f, Random.Range(150f, 210f), 0f));
        projectile_ai.reset(shootChargetime);
        projectile_ai.head.GetComponent<Rigidbody>().AddForce((projectile_ai.transform.forward).normalized * 15f, ForceMode.VelocityChange);
        bulletQueue.Enqueue(projectile_ai);
        head.AddForce(head.transform.forward.normalized * 30f, ForceMode.VelocityChange);
        waypointMover.activateFartDashSpeedCap();
    }
	public void reset()
	{
		waypointMover = head.GetComponent<wayPointMoverPhy> ();
		setState (Enum_fishAIState.Wait);
		waypointMover.OnFinalWaypointReached += NotifyFinalWaypointReached;
		Greyman.OffScreenIndicator.offscreenIndic.AddIndicator (head.transform, 1);
		//lock on if enemy spawn inside lock on zone
		playerLockOnTriggerZone plotz = playerLockOnTriggerZone.lockOnZone;
		if (Vector3.Angle(head.transform.position, plotz.transform.forward)< Vector3.Angle((plotz.lockOnZoneDistance*plotz.transform.forward + plotz.lockOnZoneRadius*plotz.transform.up),plotz.transform.forward)) {
			playerLockOnTriggerZone.lockOnZone.checkIfCanAdd (head.transform);
		}
		timer_time = Time.time;

		//create bullet pool
		bulletQueue = new Queue<fish_AI_projectile>();
		GameObject bulletHolder = new GameObject("bulletPool");
		bulletHolder.transform.SetParent (transform);
		for (int i = 0; i < bulletPoolAmt; i++)
		{
			GameObject instantiatedProjectile = Instantiate(bulletPrefab, new Vector3(999,999,9999), new Quaternion(0,0,0,0)) as GameObject;
			//instantiatedProjectile.GetComponent<Collider>().enabled = false;
			instantiatedProjectile.transform.SetParent (bulletHolder.transform);
			instantiatedProjectile.SetActive (false);
			bulletQueue.Enqueue(instantiatedProjectile.GetComponent<fish_AI_projectile>());
		}
		attackCDTimer = 0f;
        //GameObject tee = Instantiate(bulletPrefab, new Vector3(0,0,0), new Quaternion(0,0,0,0)) as GameObject;

	}


	private void setState(Enum_fishAIState NewState){
		AIState = NewState;

		WaypointSetYet = false;
		AttackDelayTimerSetYet = false;
		FinalWaypointReached = false;

		if(OnStateChange != null)
			OnStateChange(AIState);
	}


	private Vector3[] generateRandomWaitWaypoint(){
		Vector3[] result = new Vector3[numOfWaitWaypoints];
		for (int i = 0; i < numOfWaitWaypoints; i++) {
			Vector3 tempV3 = Vector3.zero;
			while (true) {//break when sure that the point is in the narrow angle
				tempV3 = Random.insideUnitSphere * (randomWP_maxRadius - randomWP_minRadius);
				tempV3 = Vector3.Normalize (tempV3) * randomWP_minRadius  + tempV3;

				//Check min gap between each waypoint, make sure far enough apart
				if (i > 0) {
					if (Vector3.Magnitude (tempV3 - result [i - 1]) < min_WP_Gap) {
						tempV3 = result [i - 1] + ((tempV3 - result [i - 1]) * (min_WP_Gap));
					}
				}
				//make sure not negative z
				if (tempV3.z < 0) {
					tempV3 = new Vector3 (tempV3.x, tempV3.y, -1 * tempV3.z);
				}
				//make sure its in narrow angle
				if (Vector3.Angle (new Vector3(tempV3.x,0,tempV3.z), Vector3.forward) <= randomWP_narrowAngle && Vector3.Angle(new Vector3(0, tempV3.y, tempV3.z), Vector3.forward) <= randomWP_narrowAngleVertical) {
					break;
				}
			}
			result [i] = tempV3;
		}
		return result;
	}


	public void NotifyFinalWaypointReached(){
		FinalWaypointReached = true;
	}

	protected override void processDeath(){
		//issue total break!
		head.GetComponent<FishSegment> ().jointBreak ();
		head.GetComponent<wayPointMoverPhy>().enabled = false;
		Greyman.OffScreenIndicator.offscreenIndic.RemoveIndicator (head.transform);
	}


	public override void applyHeavyStagger(Vector3 contactPosition){
		//applyHeavyStagger stun
		applyStun(heavyStagger_stun, .5f);
		head.AddForce ((head.position - contactPosition).normalized * heavyStagger_repulse_force, ForceMode.Impulse);
		head.AddTorque (Random.insideUnitSphere * heavyStagger_repulse_torque, ForceMode.Impulse);
		//if attacking, cancel the attack
		if (AIState.Equals (Enum_fishAIState.Attack)) {
			//change state
			setState(Enum_fishAIState.Wait);
		}
		else
		{
			//whenever something breaks, reset AI basically
			waypointMover.resetWayPoints(generateRandomWaitWaypoint());
		}
	}

	//register collision with player
	public override void collisionWithPlayer (){
//		if (AIState.Equals (Enum_fishAIState.Attack)) {
//			//deal damage
//			playerObject.GetComponent<PlayerCharacter>().deltaHealth(-1*sucessful_attack_damage);
//			//change state
//			setState(Enum_fishAIState.Wait);
//			//apply stun and bounce
//			applyStun(sucessful_attack_stun);
//			head.AddForce ((head.position - playerObject.transform.position).normalized * sucessful_attack_repulse_force, ForceMode.Impulse);
//			head.AddTorque (Random.insideUnitSphere * sucessful_attack_repulse_torque, ForceMode.Impulse);
//		}
	}


	public void setTimer(float time){
		timer_time = Time.time + time;
	}
	public bool isTimerDone(){
		return timer_time < Time.time;
	}
}
