﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fish_AI_Button : fish_AI_Base {
	//public AudioClip warningSound;
    public AudioClip attack_sound;
    public AudioClip incoming_sound;
    public AudioClip death_sound;
    public WhiteSoundwaveRipple attack_warning_circle;
    public float pitch;
	public float fartAccelerationSpeed = 200f;
	//AI STATE IN PARENT CLASS



	//AI specific varaibles
	public int numOfWaitWaypoints = 1;
    //state change event
    public delegate void StateChangeAction(Enum_fishAIState NewState);
	public static event StateChangeAction OnStateChange;


	//heavy stagger
	public float heavyStagger_repulse_force = 50f;
	public float heavyStagger_repulse_torque = 50f;
	public float heavyStagger_stun = 1.5f;

  
    //PRIVATE VARS
    public bool WaypointSetYet = false;
	public bool FinalWaypointReached = false;
	// Use this for initialization
	void Start () {
		base.Start ();
        
        //randomize pitch when nobody fills pitch out
        if(pitch == 0)
        {
            pitch = 1 + Random.Range(-0.1f, 0.1f);
        }
        reset ();
        if (gameObject.GetComponentInChildren<WaypointSystem>())
        {
            //wp_fart_enabled = false;
            WaypointSystem ways = gameObject.GetComponentInChildren<WaypointSystem>();
            //override current waypoints with external, pretty waypoints
            head.GetComponent<wayPointMoverPhy>().waypoints = new Vector3[ways.waypoints.Count];
            int counter = 0;
            foreach (Transform way in ways.waypoints)
            {
                head.GetComponent<wayPointMoverPhy>().waypoints[counter] = way.position;
                counter++;
                //Debug.Log(way.position.ToString() + "waypoints"); 
            }
        }
    }
	
	// Update is called once per frame
	protected override void FixedUpdate () {
        //we're doing this because frankly the player feels super cool when they stun an enemy at close range. Stun is also changed to be over a threshold to make it more rewarding for well placed/grouped shots.

        base.FixedUpdate();
		if (willEmergencyRecover) {
			if (emerRecov_DmgCounter >= emerRecov_DmgAmt) {
				emerRecov_DmgCounter = 0f;
				setState (Enum_fishAIState.Wait);
				emergencyRecoverAcceleration (head.transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f));
			}
		}
    }

    //register collision with player
    public override void collisionWithPlayer()
    {
        //		if (AIState.Equals (Enum_fishAIState.Attack)) {
        //			//deal damage
        //			playerObject.GetComponent<PlayerCharacter>().deltaHealth(-1*sucessful_attack_damage);
        //			//change state
        //			setState(Enum_fishAIState.Wait);
        //			//apply stun and bounce
        //			applyStun(sucessful_attack_stun);
        //			head.AddForce ((head.position - playerObject.transform.position).normalized * sucessful_attack_repulse_force, ForceMode.Impulse);
        //			head.AddTorque (Random.insideUnitSphere * sucessful_attack_repulse_torque, ForceMode.Impulse);
        //		}
    }

    public void reset()
	{
		waypointMover = head.GetComponent<wayPointMoverPhy> ();
		setState (Enum_fishAIState.Wait);
		waypointMover.OnFinalWaypointReached += NotifyFinalWaypointReached;
		Greyman.OffScreenIndicator.offscreenIndic.AddIndicator (head.transform, 1);
        //lock on if enemy spawn inside lock on zone
        
        playerLockOnTriggerZone plotz = playerLockOnTriggerZone.lockOnZone;
        /*
		if (Vector3.Angle(head.transform.position, plotz.transform.forward)< Vector3.Angle((plotz.lockOnZoneDistance*plotz.transform.forward + plotz.lockOnZoneRadius*plotz.transform.up),plotz.transform.forward)) {
			playerLockOnTriggerZone.lockOnZone.checkIfCanAdd (head.transform);
		}
        */
        
	}


	private void setState(Enum_fishAIState NewState){
		AIState = NewState;

		WaypointSetYet = false;
		FinalWaypointReached = false;

		if(OnStateChange != null)
			OnStateChange(AIState);
	}



    public override void applyStun(float stunDuration, float max_stun_scale)
    {
        head.GetComponent<QRotateTowards>().stunned = .5f ;
        //base.applyStun(stunDuration, max_stun_scale);
    }

    public void NotifyFinalWaypointReached(){
		FinalWaypointReached = true;
	}

	protected override void processDeath(){
		//issue total break!
		head.GetComponent<FishSegment> ().jointBreak ();
        head.GetComponent<wayPointMoverPhy>().enabled = false;
        head.GetComponent<QRotateTowards>().enabled = false;
		Greyman.OffScreenIndicator.offscreenIndic.RemoveIndicator (head.transform);
       
        //add hit sound
        if (death_sound)
            AudioManager.manager.playSFXOneShot(death_sound, 0, death_sound.length, pitch);
        GameController.controller.startEndlessMode();
    }


	public override void applyHeavyStagger(Vector3 contactPosition){
		//applyHeavyStagger stun
		head.AddForce ((head.position - contactPosition).normalized * heavyStagger_repulse_force, ForceMode.Impulse);
		head.AddTorque (Random.insideUnitSphere * heavyStagger_repulse_torque, ForceMode.Impulse);
		//if attacking, cancel the attack
		if (AIState.Equals (Enum_fishAIState.Attack)) {
			//change state
			setState(Enum_fishAIState.Wait);
		}
        else
        {
            //whenever something breaks, reset AI basically
        }
	}
    

	public void emergencyRecoverAcceleration(Vector3 NewWaypoint){
		Debug.LogError ("HERE");
		head.GetComponent<Rigidbody> ().AddForce ((NewWaypoint - head.transform.position).normalized * fartAccelerationSpeed * 2, ForceMode.VelocityChange);
		waypointMover.activateFartDashSpeedCap ();
	}
}
