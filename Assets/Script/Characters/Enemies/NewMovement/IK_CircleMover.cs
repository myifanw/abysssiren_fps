﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IK_CircleMover : MonoBehaviour {
	public Vector3 centerPt;
	public float degree;
	public float speed;
	public float radius;
	private Vector3 oldPos;
	private bool flatg = false;
	// Use this for initialization
//	void Start () {
//		centerPt = transform.position;
//	}

	public void reset(float radius_in){
		degree = 0;
		radius = radius_in;
		float radian = 180 * Mathf.Deg2Rad;
		Vector3 p = new Vector3 (Mathf.Cos(radian), Mathf.Sin(radian), 0);
		centerPt = (p* radius) + transform.position;
	}
	// Update is called once per frame
	void Update () {
		degree = degree + Time.deltaTime * speed;
		float radian = degree * Mathf.Deg2Rad;
		Vector3 p = new Vector3 (Mathf.Cos(radian), Mathf.Sin(radian), 0);
		transform.position = centerPt + (p* radius);
		if (flatg) {
			transform.rotation = Quaternion.LookRotation (transform.position - oldPos);
		}
		flatg = true;
		oldPos = transform.position;
	}
//	void Update () {
//		transform.RotateAround (centerPt, Vector3.up, speed * Time.deltaTime);
//	}
}
