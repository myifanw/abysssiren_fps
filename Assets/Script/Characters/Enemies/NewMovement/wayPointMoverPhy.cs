﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using VacuumBreather;

public class wayPointMoverPhy : MonoBehaviour {
	private Rigidbody rb;

	//speed var
	public float maxSpeed= 50f;
	public float maxSpeed_fart= 100f;
	public float maxSpeed_lerpSpeed= 1f;
	public float maxSpeed_actual;
	public float acceleration=10f;

	public bool attacking = false;
	public float maxSpeed_att=200f;
	public float maxSpeed_att_fart=300f;
	public float maxSpeed_att_actual;
	public float acceleration_att=50f;
	//waypoint
	public Transform[] initial_waypoints_transform;
	public Vector3[] waypoints;
	public int curWaypointIdx;
	public float WatpointTouchDistance;

	//force direction correction
	public bool stunned;
    [Range(0, 2f)]
    public float stun_threshold = 0; //how much stun time does it take before it actually happens for this unit?
    public float stun_multiplier = 1;
	private float stunTimer = 0f;
	public float forceDirectionCorrectionMultiplier = 5f;
	public float finalWaypoint_forceDirectionCorrectionMultiplier = 999f;

	public float Kp;
	public float Ki;
	public float Kd;
	public float finalWaypoint_Kp = 999f; //final waypoint has increased turning accuracy
	public float finalWaypoint_Kp_maxDistance = 25f;
	public float finalWaypoint_Kp_minDistance = 5f;

	public float waypoint_maxwait = 10f; //switch to next waypoint if timer is up
	public float waypoint_maxwait_timer;

	private readonly PidQuaternionController _pidController = new PidQuaternionController(8.0f, 0.0f, 0.05f);

    private bool final_reached = false;

	//change to next waypoint event
	public delegate void NextWaypointChangeAction(Vector3 NewWaypoint);
	public event NextWaypointChangeAction OnNextWaypointChange;

	//waypoint reached
	public delegate void WaypointReachedAction(Vector3 ReachedWaypoint);
	public event WaypointReachedAction OnWaypointReached;

	//final waypoint reached
	public delegate void FinalWaypointReachedAction();
	public event FinalWaypointReachedAction OnFinalWaypointReached;


	//up vector
	public Vector3 rotateUpVector = Vector3.up;
	public bool ignoreRotateUpVector = false;
	public bool willDrift = false;
	void Awake() {
        reset ();
    }


	public void reset(){
		rb = GetComponent<Rigidbody>();
		stunned = false;
		stunTimer = 0f;
        curWaypointIdx = 0;
        waypoint_maxwait_timer = waypoint_maxwait;
        final_reached = false;
		maxSpeed_actual = maxSpeed;
		maxSpeed_att_actual = maxSpeed_att;
        //resetWayPoints (initial_waypoints_transform);
    }

	//***** WAYPOINT LIST MANAGEMENT*****************
	public void resetWayPoints(Vector3[] waypoints){
		this.waypoints = waypoints;
		curWaypointIdx = 0;
		waypoint_maxwait_timer = waypoint_maxwait;
        final_reached = false;
    }
	public void resetWayPoint(Vector3 waypoint){
		resetWayPoints (new Vector3[]{waypoint});
	}
	public void resetWayPoints(Transform[] waypoints_transform){
		Vector3[] temp = new Vector3[waypoints_transform.Length];
		for (int i = 0; i < waypoints_transform.Length; i++) {
			temp [i] = waypoints_transform [i].position;
		}
		resetWayPoints (temp);
	}
	public void resetWayPoint(Transform waypoint_transform){
		resetWayPoints (new Transform[]{waypoint_transform});
	}

	public void addWayPoints(Vector3[] waypoints_vec){
		Vector3[] tempArr = new Vector3[this.waypoints.Length + waypoints_vec.Length];
		this.waypoints.CopyTo (tempArr, 0);
		waypoints_vec.CopyTo (tempArr, this.waypoints.Length);
		this.waypoints = tempArr;
        final_reached = false;
    }
	public void addWayPoint(Vector3 waypoint_vec){
		addWayPoints (new Vector3[]{waypoint_vec});
	}
	public void addWayPoints(Transform[] waypoints_transform){
		Vector3[] temp = new Vector3[waypoints_transform.Length];
		for (int i = 0; i < waypoints_transform.Length; i++) {
			temp [i] = waypoints_transform [i].position;
		}
		addWayPoints (temp);
	}
	public void addWayPoint(Transform waypoint_transform){
		addWayPoints (new Transform[]{waypoint_transform});
	}
    //***** END WAYPOINT LIST MANAGEMENT*****************

    //***** Stun Management ************
    public void applyStun(float stunnedDuration)
    {
        applyStun(stunnedDuration, .5f);
    }
    public void applyStun(float stunnedDuration, float stun_scale_max){
        //stunTimer = Mathf.Max (stunTimer, stunnedDuration * stun_multiplier);
        
        if (stunTimer > stun_scale_max)
        {
            stunTimer += stunnedDuration * Mathf.Pow(stunnedDuration + 1, -(stunTimer+0.5f)/2);
        }
        else
        {
            stunTimer += stunnedDuration;
        }
        //only stun when it goes over the threshold.
        if (stun_threshold <= stunTimer)
        {
            stunned = true;
        }
    }
	//***** END Stun Management ************


	void FixedUpdate() {
		//check which speed and accel to use
		//use max speed extra to calcuate dash extra speed cap
		maxSpeed_actual = Mathf.Lerp(maxSpeed_actual, maxSpeed, Time.fixedDeltaTime * maxSpeed_lerpSpeed);
		maxSpeed_att_actual = Mathf.Lerp(maxSpeed_att_actual, maxSpeed_att, Time.fixedDeltaTime * maxSpeed_lerpSpeed);

		float effective_Maxspeed = (attacking)? maxSpeed_att_actual : maxSpeed_actual;
		float effective_Acceleration = (attacking)? acceleration_att : acceleration;

		//check stunned
		if (stunned) {
			stunTimer = Mathf.Max(0f, stunTimer - Time.fixedDeltaTime);
			if (stunTimer == 0f) {
				stunned = false;
			}
		}
        else
        {
            //decay stun time when below stun threshold, but not as intensely as usual.
            stunTimer = Mathf.Max(0f, stunTimer - Time.fixedDeltaTime/3);
        }
			
		if (!stunned) {
			//add forward moving force
			float multiplier = effective_Acceleration * Time.fixedDeltaTime;
			rb.AddForce (transform.forward * multiplier, ForceMode.VelocityChange);
		}
		//forward force CORRECTION
		if (!willDrift) {
			if (!stunned) {
				if (curWaypointIdx < waypoints.Length - 1) {
					rb.velocity = Vector3.Lerp (rb.velocity, rb.velocity.magnitude * transform.forward, Time.deltaTime * forceDirectionCorrectionMultiplier); //keep direction forward
				} else {
					rb.velocity = Vector3.Lerp (rb.velocity, rb.velocity.magnitude * transform.forward, Time.deltaTime * finalWaypoint_forceDirectionCorrectionMultiplier); //keep direction forward

				}
			}
		}
		//check MAX SPEED
		if (!stunned && rb.velocity.magnitude > effective_Maxspeed){ 
			rb.velocity = rb.velocity.normalized * effective_Maxspeed;
		}

		//TURN to face towards the current waypoint
		if (!stunned) {
            if (curWaypointIdx < waypoints.Length)
            {
                looktowardsNextWayPoint(waypoints[curWaypointIdx]);
            }
		}

		//waypoint countdown 
		if (!stunned) {
			waypoint_maxwait_timer = waypoint_maxwait_timer - Time.fixedDeltaTime;
		}
		//MOVE TO NEXT waypoint if close enough
		Vector3 targetDelta = waypoints[curWaypointIdx] - transform.position;
		//move to next waypoint if current one reached or timer is done
		if (targetDelta.magnitude < WatpointTouchDistance || waypoint_maxwait_timer < 0) {
			waypoint_maxwait_timer = waypoint_maxwait;
			//signal event
			if(OnWaypointReached != null)
				OnWaypointReached(waypoints[curWaypointIdx]);
			
			if (curWaypointIdx < waypoints.Length-1) {
				curWaypointIdx = Mathf.Min (waypoints.Length-1, curWaypointIdx + 1);
				if (OnNextWaypointChange != null)
					OnNextWaypointChange (waypoints [curWaypointIdx]);
			} else {
                Debug.Log("Final reached!");
                final_reached = true;
				if (OnFinalWaypointReached != null)
					OnFinalWaypointReached ();
			}
		}
	}

	public bool checkIfFinalWaypointReached(){
        if (final_reached)
        {
            return true;
        }
		return false;
	}

	private void looktowardsNextWayPoint(Vector3 target){
		if (target == null || transform == null || rb == null)
		{
			return;
		}
		Quaternion DesiredOrientation = Quaternion.identity;
		if (ignoreRotateUpVector) {
			DesiredOrientation = Quaternion.LookRotation (target - transform.position);
		} else {
			DesiredOrientation = Quaternion.LookRotation (target - transform.position, rotateUpVector);
		}

		if (curWaypointIdx < waypoints.Length - 1) {
			this._pidController.Kp = this.Kp;
		} else {
			float lerpProgress = ((finalWaypoint_Kp_maxDistance - finalWaypoint_Kp_minDistance) - (Vector3.Distance (gameObject.transform.position, PlayerCharacter.player.gameObject.transform.position) - finalWaypoint_Kp_minDistance)) / (finalWaypoint_Kp_maxDistance - finalWaypoint_Kp_minDistance);
			this._pidController.Kp = Mathf.Lerp(this.Kp, finalWaypoint_Kp, lerpProgress);
			//Debug.LogError (this._pidController.Kp);
		}
		this._pidController.Ki = this.Ki;
		this._pidController.Kd = this.Kd;

		// The PID controller takes the current orientation of an object, its desired orientation and the current angular velocity
		// and returns the required angular acceleration to rotate towards the desired orientation.
		Vector3 requiredAngularAcceleration = this._pidController.ComputeRequiredAngularAcceleration(
			transform.rotation,
			DesiredOrientation,
			rb.angularVelocity,
			Time.fixedDeltaTime);
		rb.AddTorque(requiredAngularAcceleration, ForceMode.Acceleration);
	}

	public void activateFartDashSpeedCap(){
		maxSpeed_actual = maxSpeed_fart;
		maxSpeed_att_actual = maxSpeed_att_fart;

	}
}
