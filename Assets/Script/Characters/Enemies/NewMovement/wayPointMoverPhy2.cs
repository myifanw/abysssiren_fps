﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using VacuumBreather;

public class wayPointMoverPhy2 : MonoBehaviour {
	private Rigidbody rb;

	//speed var
	public float maxSpeed= 50f;
	public float acceleration=10f;

	public bool attacking = false;
	public float maxSpeed_att=200f;
	public float acceleration_att=50f;

	//waypoint
	public GameObject[] waypoints_obj;
	public int curWaypointIdx;
	public float WatpointTouchDistance;

	//force direction correction
	public bool stunned;
    [Range(0, 2f)]
    public float stun_threshold = 0; //how much stun time does it take before it actually happens for this unit?
    public float stun_multiplier = 1;
	private float stunTimer = 0f;

	public float Kp;
	public float Ki;
	public float Kd;
	public float finalWaypoint_Kp = 999f; //final waypoint has increased turning accuracy

	public float waypoint_maxwait = 10f; //switch to next waypoint if timer is up
	public float waypoint_maxwait_timer;

	private readonly PidQuaternionController _pidController = new PidQuaternionController(8.0f, 0.0f, 0.05f);

    private bool final_reached = false;

	//change to next waypoint event
	public delegate void NextWaypointChangeAction(GameObject NewWaypoint);
	public event NextWaypointChangeAction OnNextWaypointChange;

	//waypoint reached
	public delegate void WaypointReachedAction(GameObject ReachedWaypoint);
	public event WaypointReachedAction OnWaypointReached;

	//final waypoint reached
	public delegate void FinalWaypointReachedAction();
	public event FinalWaypointReachedAction OnFinalWaypointReached;


	//up vector
	public Vector3 rotateUpVector = Vector3.up;

	void Awake() {
        reset ();
    }


	public void reset(){
		rb = GetComponent<Rigidbody>();
		stunned = false;
		stunTimer = 0f;
        curWaypointIdx = 0;
        waypoint_maxwait_timer = waypoint_maxwait;
        final_reached = false;
    }

	//***** WAYPOINT LIST MANAGEMENT*****************
	public void resetWayPoints(GameObject[] waypoints){
		this.waypoints_obj = waypoints;
		curWaypointIdx = 0;
		waypoint_maxwait_timer = waypoint_maxwait;
        final_reached = false;
    }
	public void resetWayPoint(GameObject waypoint){
		resetWayPoints (new GameObject[]{waypoint});
	}

	public void addWayPoints(GameObject[] waypoints){
		GameObject[] tempArr = new GameObject[waypoints_obj.Length + waypoints.Length];
		this.waypoints_obj.CopyTo (tempArr, 0);
		waypoints.CopyTo (tempArr, this.waypoints_obj.Length);
		this.waypoints_obj = tempArr;
        final_reached = false;
    }
	public void addWayPoint(GameObject waypoint){
		addWayPoints (new GameObject[]{waypoint});
	}
    //***** END WAYPOINT LIST MANAGEMENT*****************

    //***** Stun Management ************
    public void applyStun(float stunnedDuration)
    {
        applyStun(stunnedDuration, .5f);
    }
    public void applyStun(float stunnedDuration, float stun_scale_max){
        //stunTimer = Mathf.Max (stunTimer, stunnedDuration * stun_multiplier);
        
        if (stunTimer > stun_scale_max)
        {
            stunTimer += stunnedDuration * Mathf.Pow(stunnedDuration + 1, -(stunTimer+0.5f)/2);
        }
        else
        {
            stunTimer += stunnedDuration;
        }
        //only stun when it goes over the threshold.
        if (stun_threshold <= stunTimer)
        {
            stunned = true;
        }
    }
	//***** END Stun Management ************


	void FixedUpdate() {
		//check which speed and accel to use
		float effective_Maxspeed = (attacking)? maxSpeed_att : maxSpeed;
		float effective_Acceleration = (attacking)? acceleration_att : acceleration;

		//check stunned
		if (stunned) {
			stunTimer = Mathf.Max(0f, stunTimer - Time.fixedDeltaTime);
			if (stunTimer == 0f) {
				stunned = false;
			}
		}
        else
        {
            //decay stun time when below stun threshold, but not as intensely as usual.
            stunTimer = Mathf.Max(0f, stunTimer - Time.fixedDeltaTime/3);
        }

		//add forward moving force
		float multiplier = effective_Acceleration * Time.fixedDeltaTime;
		rb.AddForce (transform.forward * multiplier, ForceMode.VelocityChange);

		//check MAX SPEED
		if (!stunned && rb.velocity.magnitude > effective_Maxspeed){ 
			rb.velocity = rb.velocity.normalized * effective_Maxspeed;
		}

		//TURN to face towards the current waypoint
		if (!stunned) {
			if (curWaypointIdx < waypoints_obj.Length)
            {
				looktowardsNextWayPoint(waypoints_obj[curWaypointIdx].transform.position);
            }
		}

		//waypoint countdown 
		if (!stunned) {
			waypoint_maxwait_timer = waypoint_maxwait_timer - Time.fixedDeltaTime;
		}
		//MOVE TO NEXT waypoint if close enough
		Vector3 targetDelta = waypoints_obj[curWaypointIdx].transform.position - transform.position;
		//move to next waypoint if current one reached or timer is done
		if (targetDelta.magnitude < WatpointTouchDistance || waypoint_maxwait_timer < 0) {
			waypoint_maxwait_timer = waypoint_maxwait;
			//signal event
			if(OnWaypointReached != null)
				OnWaypointReached(waypoints_obj[curWaypointIdx]);
			
			if (curWaypointIdx < waypoints_obj.Length-1) {
				curWaypointIdx = Mathf.Min (waypoints_obj.Length-1, curWaypointIdx + 1);
				if (OnNextWaypointChange != null)
					OnNextWaypointChange (waypoints_obj [curWaypointIdx]);
			} else {
                final_reached = true;
				if (OnFinalWaypointReached != null)
					OnFinalWaypointReached ();
			}
		}
	}

	public bool checkIfFinalWaypointReached(){
        if (final_reached)
        {
            return true;
        }
		return false;
	}

	private void looktowardsNextWayPoint(Vector3 target){
		if (target == null || transform == null || rb == null)
		{
			return;
		}
		Quaternion DesiredOrientation = Quaternion.LookRotation(target - transform.position, rotateUpVector);

		if (curWaypointIdx < waypoints_obj.Length - 1) {
			this._pidController.Kp = this.Kp;
		} else {
			this._pidController.Kp = finalWaypoint_Kp;
		}
		this._pidController.Ki = this.Ki;
		this._pidController.Kd = this.Kd;

		// The PID controller takes the current orientation of an object, its desired orientation and the current angular velocity
		// and returns the required angular acceleration to rotate towards the desired orientation.
		Vector3 requiredAngularAcceleration = this._pidController.ComputeRequiredAngularAcceleration(
			transform.rotation,
			DesiredOrientation,
			rb.angularVelocity,
			Time.fixedDeltaTime);
		rb.AddTorque(requiredAngularAcceleration, ForceMode.Acceleration);
	}
}
