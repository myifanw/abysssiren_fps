﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fish_AI_projectile : fish_AI_Base {
	//public AudioClip warningSound;

	//AI specific varaibles
	public float sucessful_attack_damage = 10f;
	private Vector3 originalScale;
	void Awake(){
		originalScale = transform.localScale;
	}

	public float ChargeUpTimer = 0f;// govern the size of the projectile as its being generated, It grow insize until chargeUpTimer is counted down to zero then it begin moving to player
	public float ChargeUpTimerSize = 0f;// record of how long the timer was supposed to wait originally, ChargeUpTimer will count down as time goes on.

	public void reset()
	{
		reset (0f);
	}

	public void reset(float chargeUpTimerVal)
	{
		base.Reset ();
		head.GetComponent<FishSegment> ().dead = false;
		head.GetComponent<FishSegment> ().resetColor();
		GetComponent<Rigidbody> ().velocity = Vector3.zero;

		waypointMover = GetComponent<wayPointMoverPhy> ();
		waypointMover.reset ();
		ChargeUpTimer = chargeUpTimerVal;
		ChargeUpTimerSize = Mathf.Max (1f, chargeUpTimerVal);
		gameObject.SetActive (true);
        this.GetComponent<Rigidbody>().velocity = new Vector3();
        this.GetComponent<Rigidbody>().angularVelocity = new Vector3();

        waypointMover.enabled = false;
		waypointMover.resetWayPoint (playerObject.transform.position);
		Greyman.OffScreenIndicator.offscreenIndic.AddIndicator (gameObject.transform, 1);
		//lock on if enemy spawn inside lock on zone
		playerLockOnTriggerZone plotz = playerLockOnTriggerZone.lockOnZone;
		if (Vector3.Angle(gameObject.transform.position, plotz.transform.forward)< Vector3.Angle((plotz.lockOnZoneDistance*plotz.transform.forward + plotz.lockOnZoneRadius*plotz.transform.up),plotz.transform.forward)) {
			playerLockOnTriggerZone.lockOnZone.checkIfCanAdd (gameObject.transform);
		}
		GetComponent<Rigidbody> ().AddForce (transform.forward * 10f, ForceMode.Impulse);
		transform.localScale = originalScale * 0.2f;

        //update rotate towards
        gameObject.GetComponent<RotateTowards>().target = playerObject.transform;
	}

	void Update(){
		//decrease timer
		ChargeUpTimer = Mathf.Max(0f, ChargeUpTimer - Time.deltaTime);
		//gameObject.transform.localScale = originalScale * Mathf.Min(1f,((ChargeUpTimerSize - ChargeUpTimer + (ChargeUpTimerSize/4)) / ChargeUpTimerSize));
		if (ChargeUpTimer <= 0f) {
			waypointMover.enabled = true;
			GetComponent<Collider> ().enabled = true;
		}
		//GetComponent<Rigidbody> ().drag = GetComponent<Rigidbody> ().drag - Time.deltaTime;
		transform.localScale = Vector3.Lerp (transform.localScale, originalScale, Time.deltaTime);
		//Debug.LogError (transform.localScale);
	}

	protected override void processDeath(){
		//issue total break!
		//waypointMover.enabled = false;
		Greyman.OffScreenIndicator.offscreenIndic.RemoveIndicator (gameObject.transform);
		gameObject.SetActive (false);
	}

	//register collision with player
	public override void collisionWithPlayer (){
		Debug.LogError ("COLLIDE");
		playerObject.GetComponent<PlayerCharacter>().deltaHealth(-1*sucessful_attack_damage);
		processDeath ();
	}

}

