﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Greyman;
//handle state and export function to be called by others (such as collision detection with player as limb)
public abstract class fish_AI_Base : Character {
    public GameObject Container;
    public Enum_fishAIState AIState;
	public Rigidbody head; //rigidbody that contains the waypoint mover script
    private float time_to_die = 5f;
    public bool knows_the_way = false; //non-random path setting
    public float hitstop_time = 0; //for hitstun, for limbs to read from.

    protected wayPointMoverPhy waypointMover;

    public GameObject playerObject;
    public abstract void collisionWithPlayer ();

	//for emergency recovery
	public bool willEmergencyRecover = false;
	public float emerRecov_DmgCounterFallOffRate = 50f;
	public float emerRecov_DmgAmt = 200;
	protected float emerRecov_DmgCounter = 0f;
    public bool attacking_soon = false;

    protected void Start()
    {
        //Debug.Log("does this happen at all");
        waypointMover = head.GetComponent<wayPointMoverPhy>();
        List<GameObject> parts = childLoop(transform);
        //Debug.Log(parts.Count + "count parts?");
        int x = 1;
        //ignores all interal collisions
        foreach (GameObject part in parts)
        {
            int curr = x;
            while (curr < parts.Count)
            {
                if (parts[curr].GetComponent<Collider>() && part.GetComponent<Collider>())
                {
                    foreach (Collider col in part.GetComponents<Collider>())
                    {
                        Physics.IgnoreCollision(parts[curr].GetComponent<Collider>(), col);
                        //Debug.Log(parts[curr].name + " ignoring " + part.name);
                    }
                }
				curr++;
            }
            x++;
        }
        
    }
    private List<GameObject> childLoop (Transform t)
    {
        List<GameObject> parts = new List<GameObject>();
        foreach (Transform child in t)
        {
            if (child.childCount > 0)
            {
                parts.AddRange(childLoop(child));
            }
            
                parts.Add(child.gameObject);
        }
        return parts;
    }
	protected override void FixedUpdate () {
		base.FixedUpdate();
        if(currHealth <= 0)
        {
            time_to_die -= Time.fixedDeltaTime;
            if(time_to_die <= 0)
            {
                Destroy(Container);
            }
        }

		//record dmg over short time
		emerRecov_DmgCounter = Mathf.Max(0f, emerRecov_DmgCounter - (TimescaleManager.manager.getCurTimeScale () * Time.fixedDeltaTime * emerRecov_DmgCounterFallOffRate));
	}

    public override void applyStun(float stunDuration, float max_stun_scale)
    {
        waypointMover.applyStun(stunDuration, max_stun_scale);
    }

    public void Hitstop(float stop_time)
    {
        //cause all fish segments to be stalled for a moment. recursively go through attached parts.
        hitstop_time = stop_time;
        head.GetComponent<FishSegment>().Hitstop();
    }

	public override void deltaHealth(float deltaAmt){
		base.deltaHealth (deltaAmt, false);
		if (willEmergencyRecover) {
			emerRecov_DmgCounter = emerRecov_DmgCounter + (Mathf.Min(0f, deltaAmt) * -1);
		}
	}
}
