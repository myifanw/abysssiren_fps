﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Each segment has character joint on it self connecting towards the head (head will in turn have no cahracter joints)
// Each segment also has listener that listen to when the connected body (the segment towards the head) breaks
// When this segment breaks, it will 
//	1. Notify any dangling segment that is listening to it
//	2. destroy the character joint connecting it to the connected body (the segment towards the head). 
//	3. Destroy itself physcially
// 
public class FishSegment : MonoBehaviour {
	public fish_AI_Base attachedAI;
    public float health = 500;
    private float death_time_scale = .5f;
    private float damaged_time_scale = 5;
    private float damaged_time_scale_decline = 2; //after it gets hit how fast it goes back to normal
    public bool head = false;
    public float head_damage_multiplier = 2;
    public float death_exploding_multiplier = 2;
    public bool dead = false;
    private bool all_dead = false; //check for entire fish death, since I want the death animations to be different on limb break and entire break
    private bool fast_break = false;
    private bool fatal_segment = false; //if this segment took the final damage.
    private bool damaged = false;
    private bool damaged_flashing = false;
    private float death_progress = 0;
    private float damaged_progress = 0;
    public float damaged_length = .5f; //how long it stays the injured color. might be based on damage
    private float original_damaged_length;
    private bool damaged_mass = false;
    public float max_damaged_length = 2;
    //private float initial_break_joint_strength = .01f;
    
    private bool broken = false; //for moving breaks into the updates
    //private Color color = new Color(193/255f, 216/255f, 1, 1);
    public Color color = new Color(96/255f, 107/255f, 128/255f, 1);
    public Color damaged_color = new Color(1, 70 / 255f, 70 / 255f, 1);
    private Color dead_color = new Color(0,0,0, 1);
    public Color attacking_color = new Color(193/255f, 216 / 255f, 1, 1);

    private float attack_progress = 0;
    private bool attack_coloring = false;

    //event delegate for the dangling segments to listen for
    public delegate void BreakAction();
	public event BreakAction OnJointBreak;
    //honestly no clue, just copying you PT
    public delegate void HitstopAction();
    public event HitstopAction OnHitstop;

    //must be filled out
    public Texture tex ;
    public Texture tex_damaged ;

    private Vector3 lastRecordedContactPoint = Vector3.zero;
    private Vector3 hitstop_last_velocity;
    private float hitstop_countdown;
    private float hitstop_time_till_stop = .25f;
    public bool hitstopped = false;
    private Bullet fatal_bullet; //bullet that killed ya

    public Renderer rend;

    private MaterialPropertyBlock _propBlock;
    public AudioClip hitSound;
    public AudioClip deathSound;


    public Material transparent_mat;
    private Material og_mat;
    private bool trans;
    private float trans_time;
    private float curr_trans_time;
    private float final_opacity;
    private bool fading;

    private Rigidbody rb;
    private float og_mass;
    void Start()
    {
		reset ();
		resetColor ();
        rb = this.GetComponent<Rigidbody>();
        og_mass = rb.mass;
        if(transparent_mat)
            og_mat = this.GetComponent<Renderer>().material;
    }
	//color related reset
	public void resetColor(){
        if(!rend)
    		rend = GetComponent<Renderer>();
		_propBlock = new MaterialPropertyBlock();
		_propBlock.SetColor("_Color", color);
		rend.SetPropertyBlock(_propBlock);
		original_damaged_length = damaged_length;
	}
	public void reset(){
		//set listener
		if (transform.GetComponent<CharacterJoint> ()) {
			CharacterJoint[] c_joints = transform.GetComponents<CharacterJoint> ();
			foreach (CharacterJoint c_joint in c_joints) {
				c_joint.connectedBody.GetComponent<FishSegment>().OnJointBreak += jointBreak;
                c_joint.connectedBody.GetComponent<FishSegment>().OnHitstop += Hitstop;
            }
		}
	}

    //changes materials
    public void setTransparent(bool on)
    {
        if (on)
        {
            this.GetComponent<MeshRenderer>().material = transparent_mat;
        }
        else
        {
            this.GetComponent<MeshRenderer>().material = og_mat;
        }
    }
    public void setFade(float time, float final_opacity)
    {
        this.final_opacity = final_opacity;
        this.trans_time = time;
    }

	public void processBulletContact(Bullet bullet, bool applyStun){
		lastRecordedContactPoint = bullet.gameObject.transform.position;
		float damage = -bullet.power;
        if (head)
            damage = damage * head_damage_multiplier;
        damaged = true;
        //deal damage, and also specific limb damage.
        float was_alive = attachedAI.currHealth;
        if (applyStun)
        {
            attachedAI.applyStun(bullet.stunDuration, bullet.stun_scale_max);
            rb.mass = og_mass * 5;
            damaged_mass = true;
        }
		attachedAI.deltaHealth(damage);
		deltaHealth(damage);
        //if the fish dies, have hitstop effect  && was_alive > 0 && !hitstopped
        if (attachedAI.currHealth <= 0 && was_alive > 0)
        {
            //use stun duration for hitstop time, may change in the future
            if (!(hitstop_countdown > 0))
            {
                attachedAI.Hitstop(bullet.hitstop);
                fatal_bullet = bullet;
                if (bullet.GetComponent<Rigidbody>())
                {
                    bullet.GetComponent<Rigidbody>().mass = bullet.GetComponent<Rigidbody>().mass * 4;
                    bullet.big_spark = true;
                    rb.mass = rb.mass * 3;
                }
            }
            //bullet.Hitstop(bullet.stunDuration, this.gameObject);

            if (deathSound)
                AudioManager.manager.playSFXOneShot(deathSound, 0, deathSound.length, 1 + UnityEngine.Random.Range(-0.2f, 0.2f));
            else if (hitSound)
                AudioManager.manager.playSFXOneShot(hitSound, 0, hitSound.length, 1 + UnityEngine.Random.Range(-0.2f, 0.2f));
        }
        else
        {
            if (hitSound && attachedAI.currHealth >= 0)
                AudioManager.manager.playSFXOneShot(hitSound, 0, hitSound.length, 1 + UnityEngine.Random.Range(-0.2f, 0.2f));
        }
        
        //((FishEnemyController)attachedChar).currStun += 5;
    }
    public void Hitstop()
    {
        //moving this to update so that the initial hit goes through
        //hitstop_last_velocity = this.gameObject.GetComponent<Rigidbody>().velocity;
        //rb.isKinematic = true;


        hitstop_countdown = attachedAI.hitstop_time;
        //notify any listener listening to this segment (mostly gonna be segment that depend on it)... apparently. Again, copying you PT
        if (OnHitstop != null)
            OnHitstop();

        //dropping mass for better feel
        if(fatal_segment)
            rb.mass /= 2;
        else
        {
            rb.mass /= 4;
            rb.angularDrag /= 4;
        }
    }

	void OnCollisionEnter(Collision col)
	{

		if (col.gameObject.tag == "Player")
		{
			attachedAI.collisionWithPlayer ();
		}

		if (col.gameObject.tag == "Enemy")
		{
			//Debug.LogError ("ENEMY LKOG - "+attachedAI.gameObject.name + " " + gameObject.name + " - "+ col.gameObject.GetComponent<FishSegment>().attachedAI.name + " " + col.gameObject.name);
		}
	}

    void deltaHealth(float delta)
	{
		health += delta;

		//check if joint health is gone but fish character health still remain > 0
		if (attachedAI.currHealth > 0){
			if (health <= 0) {
				if (head) {
					//head dead force remove all remaining health of the fish character
					attachedAI.deltaHealth (-attachedAI.currHealth, true);
				} else {
					//joint break, only happen when fish character still have health. Otherwise the fish character will issue total break
                    fast_break = true;
                    fatal_segment = true;
                    attachedAI.applyHeavyStagger (lastRecordedContactPoint);
					jointBreak ();
				}
			}
        }
        //check for parent death in order to break this joint "out of turn"
        else
        {
            fast_break = true;
            jointBreak();
        }

        //stay red longer when more hurt, for intellegent HP stuff.
        damaged_length = original_damaged_length + max_damaged_length * (attachedAI.currHealth / attachedAI.maxHealth); 
    }

    //when your joint breaks, your parent dies too
    public void jointBreak()
    {
        /*
		//notify any listener listening to this segment (mostly gonna be segment that depend on it)
		if (OnJointBreak != null)
			OnJointBreak ();
        */
        /*
		//stop self from listening too / break all things below
		if (transform.GetComponent<CharacterJoint> ()) {
			CharacterJoint[] c_joints = transform.GetComponents<CharacterJoint> ();
			foreach (CharacterJoint c_joint in c_joints) {
				c_joint.connectedBody.GetComponent<FishSegment>().OnJointBreak -= jointBreak;
			}
		}
		*/
        if (attachedAI.currHealth <= 0)
        {
            all_dead = true;
        }
        rb.mass = 1;
        dead = true;
    }
   

    void FixedUpdate()
    {
        if (rb.velocity.magnitude > 40)
        {
            //Debug.Log("lol what" + this.name + " " + rb.velocity.ToString());
            //rb.velocity /= rb.velocity.magnitude / 40;
        }
        //hitstop code
        if (hitstop_countdown > 0)
        {
            if (!hitstopped && hitstop_time_till_stop <= 0)
            {
                //damage the limbs visually on the non-fatal limbs
                damaged = true;
                if (!fatal_segment)
                    damaged_progress = 1 + damaged_length;

                _propBlock.SetTexture("_Ramp", tex_damaged);
                rend.SetPropertyBlock(_propBlock);

                //after a moment, hitstop
                hitstop_last_velocity = this.gameObject.GetComponent<Rigidbody>().velocity;
                rb.isKinematic = true;
                hitstopped = true;
                //stop the bullet too
                if(fatal_bullet)
                   fatal_bullet.Hitstop(fatal_bullet.stunDuration, this.gameObject);
            }
            hitstop_time_till_stop -= Time.fixedDeltaTime * 10;
            hitstop_countdown -= Time.fixedDeltaTime * 10; //multiply by factor because the variable is currently stun which is too strong.
        }
        else //do literally everything else. Hitstop is basically super stun
        {
            if(hitstopped)
            {
                //countdown must be over, resume normal actions
                hitstop_countdown = 0;
                hitstopped = false;
                rb.isKinematic = false;
                _propBlock.SetTexture("_Ramp", tex);
                /*
                if (fatal_segment)
                    rb.velocity = hitstop_last_velocity * 2;
                else
                */
                rb.velocity = hitstop_last_velocity;
                //Debug.Log(hitstop_last_velocity + " " + this.name);
                //let the bullets frozen by this go
            }
            if (dead)
            {
                //break attachment towards the head. If head, start breaking immediately. Otherwise, if it's all dying, wait a bit. OR, if it's just breaking off a piece OR THE FATAL PIECE, do it quicker.
                if (!broken && (head || (all_dead && death_progress >= .15f) || (fast_break)))
                {
                    //notify any listener listening to this segment (mostly gonna be segment that depend on it)
                    if (OnJointBreak != null)
                        OnJointBreak();
                    if (transform.GetComponent<CharacterJoint>())
                    {
                        CharacterJoint[] c_joints = transform.GetComponents<CharacterJoint>();
                        foreach (CharacterJoint c_joint in c_joints)
                        {
                            //break children joints in time
                            c_joint.connectedBody.GetComponent<FishSegment>().OnJointBreak -= jointBreak;

                            //Destroy (c_joint);
                            c_joint.breakForce = .0000001f;
                            c_joint.breakTorque = .0000001f;

                            //make loose
                            if (head)
                                rb.mass = rb.mass * 2;
                            rb.drag = 1f;
                            rb.angularDrag = 0;
                            //explode a little if it's dying
                            if (all_dead && !fast_break)
                            {
                                rb.velocity = rb.velocity * death_exploding_multiplier;
                            }
                            this.tag = "Untagged";
                            //c_joint.breakTorque = Mathf.Lerp(initial_break_joint_strength, .0000001f, death_progress * 1.5f);
                            //c_joint.breakForce = c_joint.breakTorque;
                        }
                    }
                    broken = true;
                }
                if (death_progress <= 1)
                {
                    _propBlock.SetColor("_Color", Color.Lerp(color, dead_color, death_progress * 1.5f));
                    rend.SetPropertyBlock(_propBlock);
                }
                death_progress += Time.deltaTime * death_time_scale;

                transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0.001f, 0.001f, 0.001f), Mathf.Max(0, death_progress - 0.8f));

                if (death_progress > 3f)
                {
                    //Destroy(this.gameObject);
                    this.gameObject.SetActive(false);
                }
            }
        }

        if (attachedAI.attacking_soon)
        {
            //Debug.Log("ATTACKING SOON");
            attack_coloring = true;
            _propBlock.SetColor("_Color", Color.Lerp(color, attacking_color, attack_progress / 2));
            //_propBlock.SetColor("_Color", attacking_color);
            attack_progress += Time.deltaTime;
            rend.SetPropertyBlock(_propBlock);
        }
        else if (attack_coloring)
        {
            //Debug.Log("else attacking");
            attack_progress -= Time.deltaTime;
            _propBlock.SetColor("_Color", Color.Lerp(attacking_color, color, attack_progress));
            if (attack_progress <= 0)
            {
                attack_coloring = false;
            }
            rend.SetPropertyBlock(_propBlock);
        }

        if (damaged) //damage goes outside hitstop loop because it can and helps sell the effect.
        {
            //knock it out of transparency
            curr_trans_time = 0;

            if (damaged_progress == 0)
            {
                _propBlock.SetTexture("_Ramp", tex_damaged);
                damaged_flashing = true;
            }
            if (damaged_progress <= 1)
            {
                _propBlock.SetColor("_Color", Color.Lerp(color, damaged_color, damaged_progress));
                if(damaged_progress >= .7f && damaged_mass)
                {
                    damaged_mass = false;
                    rb.mass = og_mass;
                }
            }
            else if (damaged_progress > 1 && damaged_progress <= (2.2f + damaged_length))
            {
                if (damaged_flashing == true)
                {
                    rb.mass = og_mass;
                    _propBlock.SetTexture("_Ramp", tex);
                }

                _propBlock.SetColor("_Color", Color.Lerp(damaged_color, color, damaged_progress - (1 + damaged_length)));
            }
            else
            {
                damaged = false;
                damaged_progress = 0;
            }
            damaged_progress += Time.deltaTime * damaged_time_scale;
            rend.SetPropertyBlock(_propBlock);
        }

        //fading code
        if(fading)
        {
            curr_trans_time += Time.fixedDeltaTime;
            _propBlock.SetFloat("_Alpha", Mathf.Lerp(1, final_opacity, curr_trans_time / trans_time));

            rend.SetPropertyBlock(_propBlock);
        }
    }
	public bool isSegmentDead(){
		return dead;
	}
}
