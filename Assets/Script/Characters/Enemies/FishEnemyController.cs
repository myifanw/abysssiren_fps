﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FishEnemyController : Character
{
    //public AudioClip warningSound;
    public Rigidbody head;
    private Rigidbody rb;
    public GameObject playerObject;

    public float movingSpeed = 8;
    public float attackingSpeed = 8;
    public float rotateTowardsSpeed = 360; //can't be too small or else it won't turn properly
    public float initialD; //initial distance away, not properly used
    public float distance; //how far away it waits
    public Vector3 waitPoint; //where it ends up waiting
    public float entryDelay; //how long until it enters the scene

    public float attackTimer; //how long it waits before attacking
    public float countdown; //countdown to timer



    public bool random = true; //randomizes certain behaviors, like initial position
    private float randW = 0; 
    private float randH = 0;
    public float startRotX, startRotY; //initial rotation, for trippy shit
    public enum FishType //various things I haven't decided exact mechanics for
    {
        Dumb,
        Fast,
        Slow,
        Wait
    }
    public enum FishAI //directional
    {
        Straight,
        Left,
        Right,
        Up,
        Down
    }

    public FishAI AI;
    public FishType type;
    //where the fish going left, right, etc, will turn. So they'll go that far first, then hit a plane at the turning point, and then turn at the player.
    public float aiDistance; //height/angle away from the player the enemy aims for. (radius)
    public float turningPoint;//distance to player before behavior changing
    private Plane threshold; //plane defined by turning point and player

    //used for determining attack paths
    private Vector3 startPoint;
    private Vector3 endPoint;

    public float currStun; //how long it is stunned

    //public bool attacked = false; //have you attacked

    //code tracing where the player is. Not relevant for mobile, but needed for a PC VR port.
    private Vector3 playerTrackedPos; 
    private Vector3 playerTrackedFor;
    private Vector3 playerTrackedUp;
    private Vector3 playerTrackedRight;
    private Vector3 ogScale; //used for tracking original scale while enemies are hidden at 0,0,0
    private bool attackSounding = false;

    public bool dead;
    public float death_timer = 0;

    // Use this for initialization
    void Start()
    {
        init();

    }

    public void init()
    {
        //rb = GetComponent<Rigidbody>();
        //use the passed head as the controllable thing.
        rb = head;

        //give countdown some flavor nvm, don't. strict times are best.
        countdown = attackTimer;

        if (initialD == 0)
            initialD = distance;



        //for fish going to a target.

        //get world point of a different local point, after fixing scale. Not finished
        startPoint = (rb.transform.position);
        endPoint = endPoint - rb.transform.localPosition;
        Vector3 scale = rb.transform.localScale;
        scale.Set(1 / scale.x, 1 / scale.z, 1 / scale.z);
        endPoint = Vector3.Scale(endPoint, scale);
        endPoint = rb.transform.TransformPoint(endPoint);

        //might as well give em a little randomness
        randW = Random.value * 360;
        randH = Random.value * 40 - 25;


        threshold = new Plane();

        //start this enemy up after the entry delay...
        ogScale = transform.localScale;
        transform.localScale = new Vector3(0, 0, 0);

        // Spawn delay
        StartCoroutine(ExecuteAfterTime(entryDelay, "begin"));

        //get a position relative to the player in a sphere.
        float trueD;
        if (type == FishType.Wait)
            trueD = initialD;
        else
            trueD = distance;

        if (random)
        {
            //randomly generate
            Quaternion rotation = Quaternion.Euler(randW, randH, 0);
            rotation = rb.transform.rotation * rotation;
            waitPoint = rotation * playerObject.transform.forward * trueD;

        }
        else
        {
            Quaternion rotation = Quaternion.Euler(startRotX, startRotY, 0);

            Debug.Log(gameObject.name + " " + rotation + " " + rb.transform.rotation);
            rotation = rb.transform.rotation * rotation;
            waitPoint = rotation * playerObject.transform.forward * trueD;

            Debug.Log(rotation + " " + playerObject.transform.position + " " + playerObject.transform.forward + " " + waitPoint);
        }
    }
    IEnumerator ExecuteAfterTime(float time, string e)
    {
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        switch (e)
        {
            case "begin":
                transform.localScale = ogScale;
                break;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
    //this was the attack direction stuff. Mostly unneeded.
    void DrawLine(Vector3 start, Vector3 end, float duration = 0.2f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = new Color(1, 0, 0, .5f);
        lr.endColor = new Color(0.5f, 0.5f, 0.5f, .3f);
        lr.startWidth =.3f;
        lr.endWidth = .03f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }


    void FixedUpdate()
    {
        //health check
        base.FixedUpdate();
        //DEATH CHECK
        if (currHealth <= 0 || dead)
        {
            dead = true;
            if(head)
                //head.GetComponent<FishSegment>().dead = true;
            death_timer += Time.deltaTime;
            if (death_timer > 4)
            {
                Destroy(this.gameObject);
            }
        }

        //check for getting hit.

        else if (currStun > 0)
        {
            currStun -= 10 * Time.deltaTime;
        }
        else
        {
            float factor;
            Vector3 final;
            switch (type)
            {
                default:
                    // --- waiting, like a dumb ass fish, or a waiting fish.
                    if (countdown > 15 && (type == FishType.Dumb || type == FishType.Wait))
                    {
                        // --- Target position ---
                        
                        Vector3 targetPos = waitPoint;
                        //actual velocity setting. Final is velocity
                        final = targetPos - rb.transform.position;

                        //go to it's waiting position. Factor is the speed it goes to the waiting position, I guess.
                        factor = 1;
                        if (final.magnitude < 3)
                            factor = 0.1f;
                        else if (final.magnitude < 10)
                            factor = 0.5f;


                        final = final.normalized * factor;
                        //rb.AddForce(final, ForceMode.VelocityChange);
                        rb.velocity += (((final * movingSpeed) - rb.velocity) * (0.5f));
                        //LOOK AT ME if not going fast!
                        if (rb.velocity.magnitude < 5)
                        {
                            Quaternion q = Quaternion.LookRotation(playerObject.transform.position - rb.transform.position);
                            rb.MoveRotation(Quaternion.RotateTowards(rb.transform.rotation, q, rotateTowardsSpeed * Time.deltaTime));
                            //Debug.Log("turning");
                        }
                        else
                        {
                            Quaternion q = Quaternion.LookRotation(final);
                            rb.MoveRotation(Quaternion.RotateTowards(rb.transform.rotation, q, rotateTowardsSpeed * Time.deltaTime));
                        }

                        //count down to attack. Don't do if waiting.
                        if (type != FishType.Wait)
                            countdown = countdown - 1;
                    }
                    else
                    {
                        //attack, when countdown is done, or you're not a dumb fish.
                        if(!attackSounding)
                        {
                            AudioSource audio = this.gameObject.GetComponent<AudioSource>();
                            if (audio == null)
                            {
                                audio = this.gameObject.AddComponent<AudioSource>();
                            }
                            audio.Stop();
                            //audio.PlayOneShot(this.warningSound);
                            attackSounding = true;
                            DrawLine(rb.transform.position, playerObject.transform.position - playerObject.transform.up/2 + (playerObject.transform.position - rb.transform.position).normalized * 4, 0.4f);
                        }
                        if(countdown > 0)
                        {
                            //when the countdown is between 15 and 0, the fish is tracking the player's position. only relevant to vr port
                            playerTrackedPos = playerObject.transform.position;
                            playerTrackedFor = playerObject.transform.forward;
                            playerTrackedUp = playerObject.transform.up;
                            playerTrackedRight = playerObject.transform.right;
                            countdown = countdown - 1;
                        }

                        final = (playerTrackedPos - rb.transform.position);

                        //if the fish hits or get close enough to it's target, it's done. Find a position forward that is the distance away, then set your countdown to halfish.
                        if (final.magnitude <= 1)
                        {
                            random = false;
                            waitPoint = rb.transform.forward * distance;
                            countdown = attackTimer;
                            attackSounding = false;
                        }

                        //check if fish has passed the threshold to attack head on, if it has AI.
                        threshold.SetNormalAndPosition(playerObject.transform.forward.normalized, playerObject.transform.position + playerObject.transform.forward.normalized * turningPoint);
                        

                        //if you are any direction, go half way to the player in the direction said. otherwise, go straight.
                        if (final.magnitude < ((distance) * 1 / 2) || AI == FishAI.Straight)
                        {
                            Quaternion q = Quaternion.LookRotation(playerTrackedPos - rb.transform.position);
                            rb.MoveRotation(Quaternion.RotateTowards(rb.transform.rotation, q, 100 * Time.deltaTime));
                            Vector3 attackdir = (playerTrackedPos - rb.transform.position).normalized;
                            rb.velocity += (((attackdir * attackingSpeed) - rb.velocity) * (0.5f));
                        }
                        else
                        {
                            Vector3 attackdir = new Vector3();
                            switch (AI)
                            {
                                case FishAI.Up:
                                    attackdir = (playerTrackedPos + (playerTrackedUp * aiDistance) - rb.transform.position).normalized;
                                    rb.velocity += (((attackdir * attackingSpeed) - rb.velocity) * (0.5f));
                                    break;
                                case FishAI.Down:
                                    attackdir = (playerTrackedPos - (playerTrackedUp * aiDistance) - rb.transform.position).normalized;
                                    rb.velocity += (((attackdir * attackingSpeed) - rb.velocity) * (0.5f));
                                    break;
                                case FishAI.Right:
                                    attackdir = (playerTrackedPos + (playerTrackedRight * aiDistance) - rb.transform.position).normalized;
                                    rb.velocity += (((attackdir * attackingSpeed) - rb.velocity) * (0.5f));
                                    break;
                                case FishAI.Left:
                                    attackdir = (playerTrackedPos - (playerTrackedRight * aiDistance) - rb.transform.position).normalized;
                                    rb.velocity += (((attackdir * attackingSpeed) - rb.velocity) * (0.5f));
                                    break;
                            }
                        }
                    }
                    //add drag?
                    rb.velocity *= Mathf.Clamp01(1f - 5 * Time.fixedDeltaTime);
                    break;
            }

           
        }
    }

}
