﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish_Body_Sway : MonoBehaviour {
    public float sway_force = 1;
    public float sway_time = 1;
    public float sway_freq = 1;
    private float curr_time = 0;
    private Vector3 sway = new Vector3(1, 0, 0);
    private float curr_pingpong = 0;
    private FishSegment segment;
    private float time_rand;
    private float tare_time = 0;
	// Use this for initialization
	void Start () {
        segment = gameObject.GetComponent<FishSegment>();
        time_rand = Random.Range(0, sway_freq)/10;
        //sway_freq = sway_freq + time_rand;
        tare_time = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        curr_time += Time.deltaTime;
        /*
        if (curr_time >= sway_time)
        {
            Debug.Log("swaying");
            gameObject.GetComponent<Rigidbody>().AddForce(sway * sway_force);
            sway = sway * -1;
            curr_time = 0;
        }
        */
        curr_pingpong = Mathf.PingPong((Time.time - tare_time) * sway_freq, sway_time * 2) - sway_time;
        gameObject.GetComponent<Rigidbody>().AddRelativeForce(sway * sway_force * curr_pingpong);
        if (segment.dead)
        {
            this.enabled = false;
        }
	}
}
