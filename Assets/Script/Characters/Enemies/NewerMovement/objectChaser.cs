﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using VacuumBreather;

public class objectChaser : MonoBehaviour {
	public GameObject target;
	public float acceleration;
	public float maxSpeed;
	public float Kp;
	public float Ki;
	public float Kd;

	private readonly PidQuaternionController _pidController = new PidQuaternionController(8.0f, 0.0f, 0.05f);
	// Use this for initialization
	void Start () {
	this._pidController.Kp = this.Kp;
	this._pidController.Ki = this.Ki;
	this._pidController.Kd = this.Kd;
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody> ().AddForce (transform.forward * acceleration, ForceMode.Acceleration);

		Quaternion DesiredOrientation = Quaternion.LookRotation(target.transform.position + target.transform.right *3f - transform.position, Vector3.up);

		Vector3 requiredAngularAcceleration = this._pidController.ComputeRequiredAngularAcceleration(
			transform.rotation,
			DesiredOrientation,
			GetComponent<Rigidbody> ().angularVelocity,
			Time.fixedDeltaTime);
		if (transform.position.z <= target.transform.position.z) {
			GetComponent<Rigidbody> ().AddTorque (requiredAngularAcceleration, ForceMode.Acceleration);
		} else {
			GetComponent<Rigidbody> ().angularVelocity = Vector3.Lerp(GetComponent<Rigidbody> ().angularVelocity, Vector3.zero, Time.deltaTime *2);
		}
	}
}
