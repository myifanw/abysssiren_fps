﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent (typeof(AudioSource))]
public class Character : MonoBehaviour {

	public float maxHealth = 100f;
	public float currHealth;
	//recover if you don't take damage. Hurt and invincible when you do. Transition to stop from hurt, then to recover after a bit.
	public enum HealthState
	{
		Recover, //HP recovery
		Stop, //HP recovery stop for a while after being damaged
		Hurt //got damaged
	}
	public HealthState state;
	//damage invincibilty time
	public float hurtInvinTime = 2;
	//how long from stop to recover
	public float recoverTime = 5;
	public float recoverRate = 1;

	public float timer = 0;

	public AudioSource audioSrc;
	public AudioClip deathSound;
	public AudioClip hurtSound;
	// Use this for initialization

	public void Reset () {
		state = HealthState.Recover;
		this.currHealth = this.maxHealth;
		audioSrc = GetComponent<AudioSource> ();
	}

	public void playDamagedSound()
	{
		if (currHealth <= 0f) {
			audioSrc.PlayOneShot(deathSound);
		} else {
			audioSrc.PlayOneShot(hurtSound);
		}
	}

    public virtual void deltaHealth(float deltaAmt){
		deltaHealth (deltaAmt, false);
	}

	public void deltaHealth(float deltaAmt, bool ignoreIFrame){
		if (deltaAmt < 0) {
			//damage
			if (state != HealthState.Hurt || ignoreIFrame) {
				state = (hurtInvinTime >0)? HealthState.Hurt : HealthState.Stop;
				timer = 0;

                float was_alive = currHealth;
                //only apply damage when is not in invincibility frame
                currHealth = Mathf.Min(Mathf.Max(currHealth + deltaAmt, 0f), maxHealth);
				playDamagedSound ();
                if (currHealth <= 0f && was_alive > 0)
                {
                    processDeath();
                }
			}
		} else {
			//heal
			currHealth = Mathf.Min(Mathf.Max(currHealth + deltaAmt, 0f), maxHealth);
		}
	}


	protected virtual void processDeath(){
		Debug.LogError ("NOT SUPPOSED TO CALL VIRTUAL METHOD");
	}
	// Update is called once per frame
	protected virtual void FixedUpdate ()
	{
		timer += Time.deltaTime;
        
		switch (state)
		{
		case HealthState.Recover:
			deltaHealth(recoverRate);
			break;

		case HealthState.Stop:
			if(timer >= recoverTime)
			{
				state = HealthState.Recover;
				timer = 0;
			}
			break;

		case HealthState.Hurt:
			if(timer >= hurtInvinTime)
			{
				state = HealthState.Stop;
				timer = 0;
			}
			break;
		}

	}

	public virtual void applyStun(float stunDuration, float max_stun_scale)
    {
        Debug.LogError ("NOT SUPPOSED TO CALL VIRTUAL METHOD");
	}


	public virtual void applyHeavyStagger(Vector3 contactPosition){
		Debug.LogError ("NOT SUPPOSED TO CALL VIRTUAL METHOD");
	}
}

