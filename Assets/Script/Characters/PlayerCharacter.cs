﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

public class PlayerCharacter : Character {
	public static PlayerCharacter player;
	public float superMeter;
	public float superMeterMax = 3f;
	public float superMeterRate = 0.15f;
    public float superMeterRateMax = 1f;
    private float currMeterRate;
    private bool wasSilent = false;
    private float silentTime = 0;
    private Explosive_Silence boom;
    public Image dmgindicator;
    public DamagedScreen DamagedScreen;

    // Use this for initialization
    void Start () {
        if (this.gameObject.GetComponent<Explosive_Silence>())
            boom = this.gameObject.GetComponent<Explosive_Silence>();
        
        Reset();
	}
	public void Reset(){
		if (player != null) {
			player = null;
			Destroy (player.gameObject);
		}
		player = this;
		superMeter = superMeterMax;
		base.Reset ();
	}
		
	void FixedUpdate ()
	{
		if (GameController.controller.gameState == GameState.Preparation || GameController.controller.gameState == GameState.Tutorial_Finalize) {
			recoverTime = 0f;
			recoverRate = 100f;
		} else {
			recoverTime = 5f;
			recoverRate = 0f;
		}
        if (!ProjectileShooter.projectileShoorter.firing) {
            if (!wasSilent)
            {
                wasSilent = true;
                currMeterRate = superMeterRate;
                silentTime = 0;
            }
            else
            {
                currMeterRate = Mathf.Lerp(superMeterRate, superMeterRateMax, silentTime);
                silentTime += Time.fixedDeltaTime/2;
            }
            deltaSuperMeter(currMeterRate * TimescaleManager.manager.getCurTimeScale() * Time.fixedDeltaTime);
        }
        else
            wasSilent = false;
		base.FixedUpdate ();

	}
    public override void deltaHealth(float deltaAmt)
    {
        base.deltaHealth(deltaAmt);
        if (deltaAmt < 0 && boom && boom.active)
        {
            Debug.Log(deltaAmt);
            boom.explode();
        }
        if(deltaAmt < 0 )
        {
            //show that blood effect
            DamagedScreen.hurt();
        }
    }
    protected override void processDeath(){
        //SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
        TimescaleManager.manager.setCurTimeScale(0);
        gameObject.GetComponentInParent<Grayscale>().enabled = true;
		GameController.controller.processGameOver ();
    }

	public void deltaSuperMeter(float amt ){
		superMeter = Mathf.Max (Mathf.Min (superMeter +  amt, superMeterMax), 0f);

	}
}
