﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
	public static AudioManager manager;
	public AudioSource[] BGM_Sources;
	public AudioSource[] SfxSources;
	private int curBGM_Idx;
	private int curSFX_Idx;
	private float CrossFadeTimer = 0f;
	private float CrossFadeTimer_Base = 1f;
	public AudioClip testBGM1;
//	public AudioClip testBGM2;
//	public AudioClip testSFX1;
//	public AudioClip testSFX2;
	// Use this for initialization
	void Awake () {
		reset ();
	}

	public void reset () {
		if (manager != null && manager != this) {
			Destroy (manager.gameObject);
		}
		manager = this;
		//playSFXOneShot (testSFX1, 5f, 6f);
		curBGM_Idx = 0;
		curSFX_Idx = 0;
	}

	// Update is called once per frame
	void Update () {
        //Debug.LogError(BGM_Sources[1].isPlaying);
		CrossFadeTimer = Mathf.Max(0f, CrossFadeTimer - Time.deltaTime);
		ProcessBGMCrossFade ();
		for (int i = 0; i < SfxSources.Length; i++) {
			SfxSources[i].volume = settingVariables.getVolume_SFX ();
		}

		if (Input.GetKeyDown (KeyCode.F1)) playSFXOneShot (testBGM1,0f, testBGM1.length, 5f);
//		if (Input.GetKeyDown (KeyCode.F2)) playNewBgm (testBGM2);
//		if (Input.GetKeyDown (KeyCode.F3)) playSFXOneShot (testSFX1);
//		if (Input.GetKeyDown (KeyCode.F4)) playSFXOneShot (testSFX2);
	}

	private void ProcessBGMCrossFade(){
		for (int i = 0; i < BGM_Sources.Length; i++) {
			if (i == curBGM_Idx) {
				BGM_Sources [i].volume = settingVariables.getVolume_BGM() * (CrossFadeTimer_Base - CrossFadeTimer) / CrossFadeTimer_Base;
			} else {
				BGM_Sources [i].volume = settingVariables.getVolume_BGM() * (CrossFadeTimer) / CrossFadeTimer_Base;
			}
		}
	}

	public void playNewBgm(AudioClip clip){
		playNewBgm (clip, 1f);
	}
	public void playNewBgm(AudioClip clip, float crossFadeDuration){
		playNewBgm (clip, crossFadeDuration, 1f);
	}
	public void playNewBgm(AudioClip clip, float crossFadeDuration, float pitchLevel){
		playNewBgm (clip, crossFadeDuration, pitchLevel, 1f);
	}
	public void playNewBgm(AudioClip clip, float crossFadeDuration, float pitchLevel, float volumeMultiplier){
        //Debug.Log(clip.name + "playing BGM");
		if (BGM_Sources [curBGM_Idx].clip != clip) {
			curBGM_Idx++;
			if (curBGM_Idx >= BGM_Sources.Length)
				curBGM_Idx = 0;
			if (crossFadeDuration <= 0f)
				crossFadeDuration = 0.001f;
			CrossFadeTimer_Base = crossFadeDuration;
			CrossFadeTimer = CrossFadeTimer_Base;
            //BGM_Sources [curBGM_Idx].clip = MakeSubclip(clip, 0f, clip.length, volumeMultiplier);
            BGM_Sources[curBGM_Idx].clip = clip;
            BGM_Sources [curBGM_Idx].Play ();
		}
		BGM_Sources [curBGM_Idx].pitch = pitchLevel;
	}

	public void playSFXOneShot(string clipName){
		playSFXOneShot (getClipFromAddr(clipName));
	}
	public void playSFXOneShot(string clipName, float start, float stop){
		playSFXOneShot (getClipFromAddr(clipName), start, stop);
	}
	public void playSFXOneShot(string clipName, float start, float stop, float pitchLevel){
		playSFXOneShot (getClipFromAddr(clipName), start, stop, pitchLevel);
	}

	public void playSFXOneShot(AudioClip clip){

		Debug.LogError (clip.name);
		playSFXOneShot (clip, 0f, clip.length);
	}

	public void playSFXOneShot(AudioClip clip, float start, float stop ){
		playSFXOneShot (clip, start, stop, 1f );
	}
	public void playSFXOneShot(AudioClip clip, float start, float stop, float pitchLevel ){
		playSFXOneShot (clip, start, stop, pitchLevel, 1f);
	}
	public void playSFXOneShot(AudioClip clip, float start, float stop, float pitchLevel, float clipVolumeMultiplier){
		playSFXOneShot (clip, start, stop, pitchLevel, clipVolumeMultiplier, PlayerCharacter.player.gameObject);
	}
	public void playSFXOneShot(AudioClip clip, float start, float stop, float pitchLevel, float clipVolumeMultiplier, GameObject targetObj ){
		SfxSources [curSFX_Idx].gameObject.transform.position = targetObj.transform.position;
		SfxSources[curSFX_Idx].pitch = pitchLevel;
		SfxSources[curSFX_Idx].PlayOneShot (MakeSubclip(clip,start, stop, clipVolumeMultiplier));
		curSFX_Idx++;
		if (curSFX_Idx >= SfxSources.Length)
			curSFX_Idx = 0;
        
	}

	public AudioClip getClipFromAddr(string name){
		return Resources.Load<AudioClip> (name);
	}

	private AudioClip MakeSubclip(AudioClip clip, float start, float stop){	
		return MakeSubclip (clip, start, stop, 1f);
	}
	private AudioClip MakeSubclip(AudioClip clip, float start, float stop, float volumeMUltiplier)
	{
        /* Create a new audio clip */
        int frequency = clip.frequency;
        float timeLength = stop - start;
        int samplesLength = (int)(frequency * timeLength) *clip.channels;
        AudioClip newClip = AudioClip.Create(clip.name + "-sub", samplesLength, clip.channels, frequency, false);
        /* Create a temporary buffer for the samples */
        float[] data = new float[samplesLength * clip.channels];
        /* Get the data from the original clip */
        clip.GetData(data, (int)(frequency * start));
        //adjust clip volume
        for (int i = 0; i < data.Length; i++) {
        	data [i] = data [i] * volumeMUltiplier;
        }
        /* Transfer the data to the new clip */
        //newClip.SetData(data, 0);
        newClip.SetData(data, 0);
        /* Return the sub clip */
        return newClip;
        //return clip;
    }
}
