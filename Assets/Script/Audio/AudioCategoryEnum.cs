﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioCategoryEnum {
	BackgroundMusic,
	SoundEffect,
}
