﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinishedBuildVolumeDisplay : MonoBehaviour {
	Image img;
    public ProjectileShooter shooter;
    public bool display_buffered_vol = false;
	// Use this for initialization
	void Start () {
		img = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
        if (shooter && display_buffered_vol)
        {
            img.fillAmount = shooter.buffered_Volume;
        }
        else
		    img.fillAmount = MicTestFinishedBuild_Input.volumeVal;
	}
}
