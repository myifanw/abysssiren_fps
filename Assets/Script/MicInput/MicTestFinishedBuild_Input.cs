﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MicTestFinishedBuild_Input : MonoBehaviour {
	public static MicTestFinishedBuild_Input micInput;

	//input,microphone var
	private static int MicSampleTime_s = 1;
	private static int MicSampleFrequency_perS = 44100;
	private bool _isInitialized;

	//sampling, audio clip var
	public static AudioSource _ac;
	//private static AudioClip _clipRecord;
	private static int _VolumeSampleWindow = 500; //1000 is 2-ish centi-secnond of sample window
	private bool beginningBuffer;

	//boundingVar
	public static float maxVolLevel = float.PositiveInfinity;
	public static float minVolLevel = float.NegativeInfinity;

	//volume var
	public static float rawVolumeVal;
	public static float volumeVal;

	//check settingVariable script for sensitivity
	//public static float Volume_Sensitivity = 8f;


    public static float shootVolumeThreshold = 0.2f;
    public static bool fire = false;

    void InitMic()
    {
        _ac = GetComponent<AudioSource>();
        _ac.clip = Microphone.Start(null, true, MicSampleTime_s, MicSampleFrequency_perS);
        //Debug.Log(Microphone.devices[0] + Microphone.devices[1]);
		_ac.Stop ();
		beginningBuffer = true;
		//Debug.Log ("MIC INIT FINISHED");
	}

	void StopMicrophone()
	{
		Microphone.End (null);
		//Debug.Log ("MIC STOPPED");
	}


	public float getRawVolume(){
		float[] sampleWindow = new float[_VolumeSampleWindow];
		int samplePosition = (int)(Microphone.GetPosition (null) - _VolumeSampleWindow - 1);
		if (samplePosition < 0) {
			if (beginningBuffer) {
                beginningBuffer = false;
				return 0f;
			} else {
                samplePosition = samplePosition + (MicSampleTime_s * MicSampleFrequency_perS);

            }
		}

		_ac.clip.GetData (sampleWindow, samplePosition);
		float rawVolume = 0;
		for (int i = 0; i < _VolumeSampleWindow; i++) {
			rawVolume = rawVolume + Mathf.Abs( sampleWindow [i]);
		}
		rawVolume = rawVolume / _VolumeSampleWindow;
		return rawVolume;
	}

	public void setVolumeVal(){
		rawVolumeVal = getRawVolume ();
		if (Input.GetKey (KeyCode.LeftControl)) {
			volumeVal = 0.5f;
		}
		else if (Input.GetKey (KeyCode.S)) {
			volumeVal = 0.9f;
		} else {
			volumeVal = Mathf.Min(getRawVolume () * settingVariables.getVolume_Sensitivity(), 1.0f);
		}
	}

	void Update()
	{
		setVolumeVal ();
		if(volumeVal > shootVolumeThreshold)
        {
            fire = true;
        }
        else
        {
            fire = false;
        }

	}

	void OnEnable()
	{
		InitMic ();
		_isInitialized = true;

		if (micInput != null && micInput != this) {
			Destroy (micInput);
		}
		micInput = this;
	}

	void OnDisable()
	{
		StopMicrophone ();
	}

	void OnDestory()
	{
		StopMicrophone ();
	}

	void OnApplicationFocus(bool focus)
	{
		if (focus) {
			if (!_isInitialized) {
				InitMic ();
				_isInitialized = true;
			}
		}

		if (!focus) {
			StopMicrophone ();
			_isInitialized = false;
		}
	}

}
