﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Material_Fog : MonoBehaviour {
    public Color color;
    public Color far_color = Color.black;
    public float far_distance = 500;
    private GameObject player;
    private Renderer rend;
    private MaterialPropertyBlock _propBlock;
    // Use this for initialization
    void Start () {
        player = GameObject.Find("Player");
        rend = GetComponent<Renderer>();
        float distance = Vector3.Distance(player.transform.position, this.transform.position);
        _propBlock = new MaterialPropertyBlock();
        _propBlock.SetColor("_Color", Color.Lerp(color, far_color, distance / far_distance));
        rend.SetPropertyBlock(_propBlock);
    }
}
