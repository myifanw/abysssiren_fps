﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenResolutionSetter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        float width = Screen.width;
        float height = Screen.height;
        float ratio = width / height;
        if (width > 1200)
        {
            width = 1200;
            height = height * (width / (height * ratio));
        }
        Screen.SetResolution(Screen.width, Screen.height, true);
    }
	
}
