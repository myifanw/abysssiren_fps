﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class debugSceneLoader : MonoBehaviour {
	public KeyCode Key = KeyCode.L;
	public string sceneName;
		
	// Use this for initialization
	void Start () {
		
	}

	void OnGUI() {
		GUI.TextArea (new Rect (10, 100, 100, 110), "Press " + Key.ToString() + " to load " + sceneName);

	}
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (Key)) {
			SceneManager.LoadScene (sceneName);
		}
	}
}
