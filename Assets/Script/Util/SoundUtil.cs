﻿using UnityEngine;
using System.Collections;

public class SoundUtil : MonoBehaviour {

	public static void playClip(GameObject obj, string clipFilePath){
		playClip(obj ,Resources.Load<AudioClip> (clipFilePath));
	}

	public static void playClip(GameObject obj, AudioClip clip){
		AudioSource audioSource;
		if (obj.GetComponents<AudioSource> ().Length > 0) {
			audioSource = obj.GetComponent<AudioSource> ();
		} else {
			audioSource = obj.AddComponent<AudioSource> ();
		}
		audioSource.playOnAwake = false;
		audioSource.clip = clip;
		audioSource.Play ();
	}
}
