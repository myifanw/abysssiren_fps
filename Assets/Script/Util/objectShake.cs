﻿using UnityEngine;
using System.Collections;

/// http://www.mikedoesweb.com/2012/camera-shake-in-unity/

public class objectShake : MonoBehaviour {

	private Vector3 originPosition;
	private Quaternion originRotation;
	public float shake_time = 2f;
	public float shake_intensity = .3f;
	public float rotationMult = 0.2f;

	private float temp_shake_intensity = 0;


	void Update (){
		if (temp_shake_intensity > 0){
			transform.position = originPosition + Random.insideUnitSphere * temp_shake_intensity;
			transform.rotation = new Quaternion(
				originRotation.x + Random.Range (-temp_shake_intensity,temp_shake_intensity) * rotationMult,
				originRotation.y + Random.Range (-temp_shake_intensity,temp_shake_intensity) * rotationMult,
				originRotation.z + Random.Range (-temp_shake_intensity,temp_shake_intensity) * rotationMult,
				originRotation.w + Random.Range (-temp_shake_intensity,temp_shake_intensity) * rotationMult);
			temp_shake_intensity -= (shake_intensity/shake_time) * TimescaleManager.manager.getRealTimeDeltaTime();
		}
	}

	void Shake(){
		originPosition = transform.position;
		originRotation = transform.rotation;
		temp_shake_intensity = shake_intensity;

	}
}
