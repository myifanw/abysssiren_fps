﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooterBtnBase : MonoBehaviour {
	public delegate void BreakAction();
	public event BreakAction OnBreak;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void processBtn(){
		processBtnSpecifics ();
		if(OnBreak != null)
			OnBreak();
	}
	protected virtual void processBtnSpecifics(){
		
	}
}
