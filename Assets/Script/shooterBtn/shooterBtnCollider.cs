﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooterBtnCollider : MonoBehaviour {
	public shooterBtnBase btnReference;
	private Rigidbody rb;
	// Use this for initialization
	void Start () {
		btnReference.OnBreak += processBtnColliderBreak;
		rb = GetComponent<Rigidbody> ();
	}

	void OnCollisionEnter(Collision collision)
	{
        if(collision.gameObject.tag == "Projectile")
            btnReference.processBtn ();
	}

	//if the button confirmed register a break, each collider will enable gravity
	public void processBtnColliderBreak(){
		//rb.useGravity = true;
        this.gameObject.layer = 0;
	}
}
