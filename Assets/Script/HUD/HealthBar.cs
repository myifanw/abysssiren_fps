﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	private Image img;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (img == null) {
			img = GetComponent<Image> ();
		}
		img.fillAmount = PlayerCharacter.player.currHealth/ PlayerCharacter.player.maxHealth;
	}
}
