﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followerUI : MonoBehaviour {
	public GameObject target;
	public GameObject player;
	float radius;
	float targetMaxDistance = 0.6f;
	// Use this for initialization
	void Start () {
		radius = Vector3.Distance (transform.position, player.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp (transform.position, target.transform.position, Time.deltaTime * 15f);
		//if(Vector3.Distance( transform.position, target.transform.position)>targetMaxDistance)transform.position = (transform.position - target.transform.position).normalized * targetMaxDistance + target.transform.position;
		//if(Vector3.Distance( transform.position, target.transform.position)>targetMaxDistance)transform.position = Vector3.Lerp (transform.position, target.transform.position, Time.deltaTime * 10f);

		//transform.position = (transform.position - player.transform.position).normalized * radius + player.transform.position;
		transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, 0.3f);
	}
}
