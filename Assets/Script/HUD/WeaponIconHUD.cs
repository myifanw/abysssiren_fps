﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WeaponIconHUD : MonoBehaviour {
	public RectTransform[] dividerArray;
	public RectTransform[] iconArray;
	private float height = 0f;
	public ProjectileBaseInfoDictionary baseInfoDict;


	void OnEnable(){
		//register lsitener
		ProjectileShooter.OnCurLoadOutChange += updateIcon;
		WeaponTypeButton.OnLoadOutWeaponChange += updateIcon;
		settingVariables.OnSettingChange += updateIcon;
	}
	void OnDisable(){
		//remove listener
		ProjectileShooter.OnCurLoadOutChange -= updateIcon;
		WeaponTypeButton.OnLoadOutWeaponChange -= updateIcon;
		settingVariables.OnSettingChange -= updateIcon;
	}

	// Use this for initialization
	void Awake () {
		height = GetComponent<RectTransform> ().rect.height;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void updateIcon(){
		//set threshold divider
		dividerArray [0].gameObject.SetActive(true);
		dividerArray [0].localPosition = new Vector2 (0f, height*settingVariables.getShooter_minShootThreshold());

		//threshold for power shot
		dividerArray [1].gameObject.SetActive(true);
		dividerArray [1].localPosition = new Vector2 (0f, height*settingVariables.getShooter_powerShotThreshold());

		iconArray [0].gameObject.SetActive(true);
		iconArray [0].GetComponent<Image> ().sprite = baseInfoDict.getProjectileInfo( ProjectileLoadOutList.projectileLoadOutList.LoadOutList [ProjectileShooter.projectileShoorter.curLoadOutIndex].projectileTypeList [0]).icon;
		iconArray [0].localPosition = new Vector2 (0f, height* ((settingVariables.getShooter_powerShotThreshold() + settingVariables.getShooter_minShootThreshold()) /2));



		/*
		//index for icon
		int icon_idx = 0;
		//index for divider
		int divider_idx = 1;

		int effWeaponAmt = ProjectileLoadOutList.projectileLoadOutList.LoadOutList [ProjectileShooter.projectileShoorter.curLoadOutIndex].effectiveWeaponAmount;
		for (int i = 0; i < effWeaponAmt; i++) {
			float temp_divider = ((float)i+1f)/effWeaponAmt; //height ratio of divider for this icon
			float temp_icon = ((float)i+0.5f)/effWeaponAmt; //height ratio for the icon
			//set icon
			iconArray [icon_idx].gameObject.SetActive(true);
			iconArray [icon_idx].GetComponent<Image> ().sprite = baseInfoDict.getProjectileInfo( ProjectileLoadOutList.projectileLoadOutList.LoadOutList [ProjectileShooter.projectileShoorter.curLoadOutIndex].projectileTypeList [i]).icon;
			iconArray [icon_idx++].localPosition = new Vector2 (0f, height* (ProjectileShooter.shootVolumeThreshold + ((1-ProjectileShooter.shootVolumeThreshold) * temp_icon)));
			//set divider above
			dividerArray [divider_idx].gameObject.SetActive(true);
			dividerArray [divider_idx++].localPosition = new Vector2 (0f, height* (ProjectileShooter.shootVolumeThreshold + ((1-ProjectileShooter.shootVolumeThreshold) * temp_divider)));
		}

		for (int i = icon_idx; i < iconArray.Length; i++) {
			iconArray [i].gameObject.SetActive(false);
		}
		for (int i = divider_idx; i < dividerArray.Length; i++) {
			dividerArray [i].gameObject.SetActive(false);
		}
		*/

	}
}
