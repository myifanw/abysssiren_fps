﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SuperBarDisplay : MonoBehaviour {
	private Image img;
	public Color usableColor;
	public Color unusableColor;
    public Color emptyColor;
    public Color emptyColor2;

    public Image bgImage;

    public Color bgBaseColor;
    public Color bgColor;
    public Color bgColor2;

    private bool emptying = false;
    private bool filling = false;
    private float progress;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (img == null) {
			img = GetComponent<Image> ();
		}
		img.fillAmount = PlayerCharacter.player.superMeter / PlayerCharacter.player.superMeterMax;
		if (PlayerCharacter.player.superMeter >= ProjectileShooter.projectileShoorter.getCurProjectileInfo ().superCost) {
			img.color = usableColor;
            bgImage.color = bgBaseColor;
        }
        else if (PlayerCharacter.player.superMeter >= ProjectileShooter.projectileShoorter.getCurProjectileInfo().normalCost)
        {
            //img.color = unusableColor;
            //bgImage.color = bgBaseColor;
            if( emptying)
            {
                filling = true;
                emptying = false;
                progress = 0;
            }
            else
            {
                img.color = unusableColor;
            }
        }
        else
        {
            if(filling)
                progress = 0;
            emptying = true;
            filling = false;
            //img.color = emptyColor;
            //bgImage.color = bgColor;
        }
        if (emptying)
        {
            img.color = Color.Lerp(emptyColor2, emptyColor, Mathf.PingPong(progress, 1));
            bgImage.color = Color.Lerp(bgColor2, bgColor, Mathf.PingPong(progress, 1));
            progress += Time.deltaTime * 5;
        }
        else if (filling)
        {
            img.color = Color.Lerp(emptyColor, unusableColor, progress);
            bgImage.color = Color.Lerp(bgColor, bgBaseColor, progress);
            progress += Time.deltaTime * 4;
        }
        
    }
}
