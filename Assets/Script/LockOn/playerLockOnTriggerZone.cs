﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Greyman;
public class playerLockOnTriggerZone : MonoBehaviour {
	public static playerLockOnTriggerZone lockOnZone;
	public Transform playerTransform;
	//must match the cone, radius of the circle
	public float lockOnZoneRadius = 20f;
	//must match the cone, length of the cone
	public float lockOnZoneDistance = 25f;

	public List<FishSegment> segmentInZone;


	public static FishSegment lockedOnSegment = null;
	 
	// Use this for initialization
	void Awake () {
		if (lockOnZone != null) {
			Destroy (lockOnZone.gameObject);
		}
		lockOnZone = this;
	}

//	void OnGUI()
//	{
//		string s = "";
//		s = Time.time+"";
//		if (lockedOnSegment == null) {
//			s = s +  " NULLL \n\n\n";
//		} else {
//			s = s + lockedOnSegment.attachedAI.gameObject.name + "\n\n\n";
//		}
//
//		foreach (FishSegment fs in segmentInZone) {
//			s = s + fs.attachedAI.gameObject.name +"\n";
//		}
//		Debug.LogError ("HERE");
//		GUI.Label(new Rect(10, 10, 300, 200), s);
//	}

	// Update is called once per frame
	void Update () {
		for (int i = segmentInZone.Count - 1; i >= 0; i--) {
			if (segmentInZone [i].isSegmentDead ()) {
				if (lockedOnSegment == segmentInZone [i]) {
					lockedOnSegment = null;
				}
				segmentInZone.Remove (segmentInZone [i]);
			}
		}
		float minDistance = float.PositiveInfinity;
		float distance = float.PositiveInfinity;
		foreach(FishSegment fs in segmentInZone){
			if (!fs.gameObject.activeSelf) {
				if (lockedOnSegment == fs)
					lockedOnSegment = null;
				segmentInZone.Remove (fs);
				continue;
			}

			distance = Vector3.Cross (playerTransform.forward * lockOnZoneDistance, fs.transform.position - playerTransform.transform.position).magnitude;
			if (distance < minDistance) {
				lockedOnSegment = fs;
				minDistance = distance;
			}
		}

        //Debug.Log(lockedOnSegment.name + "Lock");
    }
	void OnTriggerEnter(Collider other) {
		checkIfCanAdd(other.transform);
	}
//	void OnTriggerStay(Collider other) {
//		checkIfCanAdd(other.transform);
//	}
	void OnTriggerExit(Collider other) {
		if (segmentInZone.Contains (other.GetComponent<FishSegment> ())) {
			if (lockedOnSegment == other.GetComponent<FishSegment> ())
				lockedOnSegment = null;
			segmentInZone.Remove (other.GetComponent<FishSegment> ());
		}
	}
	//when enemy first spawn, they will also call this if the head is inside trigger zone. This is because there is a race condition on enemy AI reset() that put its head in the offscreen indicator and the on trigger enter from this script that prevent a enemy that spawn into the lockon zone from being able to lock on
	public void checkIfCanAdd(Transform trans){
		if (trans.GetComponents<FishSegment> ().Length > 0) {
			//if (((trans.CompareTag ("Enemy") && trans.GetComponent<FishSegment> ().head) || trans.CompareTag ("EnemyProjetileLayer")) && !trans.GetComponent<FishSegment> ().dead && OffScreenIndicator.offscreenIndic.getManager ().ExistsIndicator (trans) && !segmentInZone.Contains (trans.GetComponent<FishSegment> ())) {
			if (((trans.CompareTag ("Enemy") && trans.GetComponent<FishSegment> ().head) || trans.gameObject.layer==LayerMask.NameToLayer ("EnemyProjetileLayer")) && !trans.GetComponent<FishSegment> ().dead && !segmentInZone.Contains (trans.GetComponent<FishSegment> ())) {
					segmentInZone.Add (trans.GetComponent<FishSegment> ());
			}
		}
	}
}
