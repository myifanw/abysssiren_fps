﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : Bullet {
    public float e_force = 100;
    public float e_radius = 10;
    [Range(0,5)]
    public float e_damage_duration = .1f;
    private float e_damage_countdown;
    [Range(0, 5)]
    public float e_visual_duration = 2;
    public bool head_only = false;
    private bool exploded = false;
    public 

    // Use this for initialization
    void Start () {
	}

    void OnTriggerEnter(Collider col)
    {
        //Debug.Log("Explosion?");
        if (col.gameObject.tag == "Enemy" && (!head_only || col.gameObject.GetComponent<FishSegment>().head))
        {
            float dist = Vector3.Distance(col.transform.position, this.transform.position);
            col.attachedRigidbody.AddExplosionForce(e_force, this.transform.position, e_radius);
            //col.attachedRigidbody.AddForce((col.transform.position - this.transform.position).normalized * (e_force - e_force * dist/(e_radius * 2)), ForceMode.Impulse);
            col.gameObject.GetComponent<FishSegment>().processBulletContact(this, validStunTravelDistance > Vector3.Magnitude(gameObject.transform.position - PlayerCharacter.player.transform.position));
        }
     
    }

    // Update is called once per frame
    void Update () {
		if(!exploded)
        {
            Debug.Log("exploede??");
            exploded = true;
            e_damage_countdown = e_damage_duration;
        }
        else
        {
            if (e_damage_countdown <= 0)
            {
                exploded = false;
                this.gameObject.SetActive(false);
            }
            e_damage_countdown -= Time.deltaTime;
        }
	}
}
