﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpin : MonoBehaviour {
    public float scrollSpeedx, scrollSpeedy;
    Renderer rend;
    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        float offy = Time.time * scrollSpeedy;
        float offx = Time.time * scrollSpeedx;
        rend.material.mainTextureOffset = new Vector2(offx, offy);
    }
}
