﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawn : MonoBehaviour {

    //teleport enemy in effect

    public GameObject implosion;
    public static Queue<GameObject> implosion_queue;
    private int implosion_pool_amount = 20;
    public float imp_scale = 1;
    public GameObject enemy;
    public GameObject playerObject;
    public float imp_delay = 0;
    public float spawn_delay = 1;
    public AudioClip spawn_sound;
    // Use this for initialization
    void Start () {
        if (implosion_queue == null)
        {
            //Debug.Log("create sparks");
            implosion_queue = new Queue<GameObject>();
            for (int i = 0; i < implosion_pool_amount; i++)
            {
                GameObject imp = Instantiate(implosion, new Vector3(999, -999, 9999), new Quaternion(0, 0, 0, 0)) as GameObject;
                DontDestroyOnLoad(imp);
                implosion_queue.Enqueue(imp);
                //Debug.Log("creating each spark");
            }
        }
        if(!playerObject)
        {
            playerObject = GameObject.Find("Player");
        }
        if (implosion)
        {
            StartCoroutine(Implode(imp_delay));
        }
        StartCoroutine(Spawn(spawn_delay));
    }

    IEnumerator Implode(float time)
    {
        //disable this for a sec
        yield return new WaitForSeconds(time);
        GameObject curr_imp = implosion_queue.Dequeue();
        curr_imp.transform.position = transform.position;
        curr_imp.transform.rotation = new Quaternion(0, 0, 0, 0);
        curr_imp.transform.localScale = new Vector3(imp_scale, imp_scale, imp_scale);
        curr_imp.GetComponent<ParticleSystem>().time = 1;
        curr_imp.GetComponent<ParticleSystem>().Play();
        implosion_queue.Enqueue(curr_imp);
        if(spawn_sound)
        {
            AudioManager.manager.playSFXOneShot(spawn_sound, 0, spawn_sound.length, 1 + UnityEngine.Random.Range(-0.1f, 0.1f));
        }
    }

    IEnumerator Spawn(float time)
    {
        yield return new WaitForSeconds(time);
        enemy.SetActive(true);
        if (enemy.GetComponentInChildren<fish_AI_Base>())
        {
            fish_AI_Base cont = enemy.GetComponent<fish_AI_Base>();
            cont.playerObject = playerObject;
        }
    }

}
