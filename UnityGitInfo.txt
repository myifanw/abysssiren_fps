Abyss Siren FPS Git related info

Unity version 5.5.4f1

***Gamasutra coming in clutch with them up to date Uity Git guide
http://www.gamasutra.com/blogs/TimPettersen/20161206/286981/The_complete_guide_to_Unity__Git.php

***Unity Git configurations
Navigate to Edit > Project Settings > Editor and set:
-Version Control > Mode to Visible Meta Files (This causes Unity to write asset .meta files as normal, non-hidden files in your Assets directory.) 
-Asset Serialization > Mode to Force Text (This give Git a fighting chance to sucessfully merge changes to binary files such as scenes and prefab)

***Meta and git (.meta files need to be tracked)
http://answers.unity3d.com/questions/932348/visible-or-hidden-meta-files-with-git.html